package com.foodmario.customer;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.GPSTracker;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.utils.AppLog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * This class is used for location selection, the selected location name along with lat lng returns to calling class
 */
public class ChooseLocationActivity extends AppCompatActivity {

    SharedPreference sharedPreference;

    CustomProgressDialog pd;
    Alerts alerts;
    static
    ImageView ivTick;

    public static TextView tvCurrentLocation;

    public static TextView tvChooseFromMap;
    public static TextView tvChooseFromMapTitle;

    public static String delivery_location;
    public static String current_location;
    private Geocoder geocoder;

    private static final int REQUEST_PERMISSIONS = 11;
    private boolean boolean_permission = false;
    private boolean boolean_location_loaded = false;
    private GPSTracker gpsTracker;
    public static String delivery_lat;
    public static String delivery_lng;
    private String current_lat;
    private String current_lng;
    private GoogleApiClient mGoogleApiClient;
    boolean isLocationLoaded = false;
    private Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_choose_location);

        ButterKnife.bind(this);
        pd = new CustomProgressDialog(this);
        tvChooseFromMap = (TextView) findViewById(R.id.tv_choose_from_map);
        tvCurrentLocation = (TextView) findViewById(R.id.tv_current_location);
        tvChooseFromMapTitle = (TextView) findViewById(R.id.tv_choose_title);
        ivTick = (ImageView) findViewById(R.id.tick);

//        tvCurrentLocation.setText(getArguments().getString("address"));
        alerts = new Alerts(this);
        sharedPreference = new SharedPreference(this);
        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FILTERED_ID, 0);
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
        gpsTracker = new GPSTracker(this);
        if (!gpsTracker.canGetLocation())
            showGpsSettingDialog();

        geocoder = new Geocoder(this, Locale.getDefault());

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//        }
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (!pd.isLoading() && !isLocationLoaded)
//                    fn_permission();
//            }
//        }, 2000);

        subscription = Observable.interval(5000, 1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    public void call(Long aLong) {
                        // here is the task that should repeat
                        if (!pd.isLoading() && !isLocationLoaded && boolean_permission)
                            fn_permission();
                    }
                });


    }

    private void fn_permission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

//            if ((ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION))) {
//
//
//            } else {
                ActivityCompat.requestPermissions(ChooseLocationActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION

                        },
                        REQUEST_PERMISSIONS);

//            }
            } else {
                boolean_permission = true;
                getLatLng();
            }
        } else
            getLatLng();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission = true;
                    getLatLng();

                } else {
//                    Toast.makeText(getApplicationContext(), "Please allow the permission", Toast.LENGTH_LONG).show();
                    tvCurrentLocation.setText("Location permission not granted. Go to settings and give permission");
                    boolean_permission = false;
                }
            }

        }
    }

    private void getLatLng() {
        if (!boolean_location_loaded) {
            pd.showpd("Fetching your location. Please wait...");
            gpsTracker = new GPSTracker(this);
            if (gpsTracker.canGetLocation()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (gpsTracker.latitude != 0.0) {
                            delivery_lat = String.valueOf(gpsTracker.latitude);
                            delivery_lng = String.valueOf(gpsTracker.longitude);

                            current_lat = String.valueOf(gpsTracker.latitude);
                            current_lng = String.valueOf(gpsTracker.longitude);
                            LatLng latLng = new LatLng(gpsTracker.latitude, gpsTracker.longitude);
                            getUserAddress(latLng);
                            boolean_location_loaded = true;
//                        Toast.makeText(ChooseLocationActivity.this, "delivery_lat:  " + delivery_lat, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(ChooseLocationActivity.this, "lon:  " + delivery_lng, Toast.LENGTH_SHORT).show();
                        } else
                            getLatLng();
                    }
                }, 1000);
            } else {
//            showGpsSettingDialog();
//            boolean_location_loaded = false;
                pd.hidepd();
            }
        }
    }

    private void getUserAddress(LatLng point) {
        List<Address> addresses = new ArrayList<>();
        try {
            addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1);
        } catch (IOException e) {
            pd.hidepd();
            e.printStackTrace();
        }
        Address address = null;
        try {
            address = addresses.get(0);

        } catch (Exception exx) {
        }

        if (address != null) {
            StringBuilder sb = new StringBuilder();
            try {
                sb.append(address.getAddressLine(0) + "");
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    if (address.getAddressLine(i).contains("Unnamed")) {
                        continue;
                    } else {
                        sb = new StringBuilder();
                        sb.append(address.getAddressLine(i) + "");
                    }
                    break;
                }
            } catch (Exception exx) {

            }
//            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//            }
            sharedPreference.setKeyValues(CommonDef.SharedPrefrences.DELIVERY_LOCATION, sb.toString());
            delivery_location = sb.toString();
            current_location = sb.toString();

            tvCurrentLocation.setText(current_location);
            isLocationLoaded = true;
            subscription.unsubscribe();
        } else {
            tvCurrentLocation.setText("Could not track your device location. Please choose from map");
            isLocationLoaded = true;
        }
        pd.hidepd();


        //remove previously placed Marker
    }

    private void showGpsSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        //Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);

        //5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        //this is the key ingredient to show dialog always when GPS is off
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult
                    (LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied.
                        // The client can initialize location //
                        // requests here.
                        fn_permission();
                        getLatLng();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ChooseLocationActivity.this, CommonDef.REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        fn_permission();
        super.onResume();
    }

    @OnClick(R.id.btn_proceed)
    void onProceedClicked() {
        if (delivery_location != null && !delivery_location.equalsIgnoreCase("")) {
            sharedPreference.setKeyValues(CommonDef.DELIVERY_LOCATION, delivery_location);
            sharedPreference.setKeyValues(CommonDef.DELIVERY_LAT, delivery_lat);
            sharedPreference.setKeyValues(CommonDef.DELIVERY_LON, delivery_lng);
            Opener.openHomeActivity(this);
            finish();
        } else
            alerts.showToastMsg("Please set delivery location");
    }

    @OnClick(R.id.tv_choose_from_map)
    void doChooseFromMap() {
        Opener.openChooseFromMap(this, false);
    }

    @OnClick(R.id.tv_choose_title)
    void doChooseMap() {
        Opener.openChooseFromMap(this, false);
    }

    /**
     * Set location selected from map
     *
     * @param location
     */
    public static void setLocationFromMap(String location) {
        tvChooseFromMap.setText(location);
        tvChooseFromMap.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick_green, 0);
        ivTick.setVisibility(View.GONE);
        delivery_location = location;
//
    }

    @OnClick(R.id.tv_current_location)
    void onCurrentLocationClicked() {
        tvChooseFromMap.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        ivTick.setVisibility(View.VISIBLE);
        delivery_location = current_location;
        delivery_lat = current_lat;
        delivery_lng = current_lng;
    }

    @OnClick(R.id.tv_current_location_title)
    void onCurrentCLicked() {
        onCurrentLocationClicked();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case CommonDef.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        AppLog.e("Settings", "Subscription OK");
                        AppLog.e("GPS", "GPS is Enabled in your device");

                        //fetch ongoing job
//                        ongoingJobPresenter.getOngoingJob();
//                        onSetProgressBarVisibility(View.VISIBLE);
                        fn_permission();
                        getLatLng();
                        break;
                    case RESULT_CANCELED:
                        AppLog.e("Settings", "Subscription Cancel");
                        AppLog.e("GPS", " gps is Disabled in your device");
//                        finish();
                        break;
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        subscription.unsubscribe();
    }
}
