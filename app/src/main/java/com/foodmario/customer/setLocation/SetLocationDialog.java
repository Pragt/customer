package com.foodmario.customer.setLocation;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.checkOut.model.OrderDetail;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.view.MainFragment;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/31/18.
 */

public class SetLocationDialog extends DialogFragment {

    SharedPreference sharedPreference;


    CustomProgressDialog pd;
    Alerts alerts;

    public static TextView tvCurrentLocation;

    public static TextView tvChooseFromMap;
    public static TextView tvChooseFromMapTitle;

//    public static AddCouponCodeListner listner;

    public static SetLocationDialog getInstance(String delivery_location) {
        SetLocationDialog dialog = new SetLocationDialog();
        Bundle args = new Bundle();
        args.putString("address", delivery_location);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.full_screen_dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_choose_location, container);
        ButterKnife.bind(this, view);
//        pd = new CustomProgressDialog(getActivity());
//        tvChooseFromMap = (TextView) view.findViewById(R.id.tv_choose_from_map);
//        tvCurrentLocation = (TextView) view.findViewById(R.id.tv_current_location);
//        tvChooseFromMapTitle = (TextView) view.findViewById(R.id.tv_choose_title);
//
//        tvCurrentLocation.setText(getArguments().getString("address"));
//        alerts = new Alerts(getActivity());
//        sharedPreference = new SharedPreference(getActivity());
//        tvCurrentLocation.setText(MainActivity.delivery_location);
//
        return view;
//    }
//
//    public static void setLocationFromMap(String location) {
//        tvChooseFromMap.setText(location);
//        tvChooseFromMap.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick_green, 0);
//        tvCurrentLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//        MainActivity.delivery_location = location;
//
//    }
//
//    @OnClick(R.id.tv_current_location)
//    void onCurrentLocationClicked() {
//        tvChooseFromMap.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//        tvCurrentLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_tick_green, 0);
//        MainActivity.delivery_location = MainActivity.current_location;
//        MainActivity.delivery_lat = MainActivity.current_lat;
//        MainActivity.delivery_lng = MainActivity.current_lng;
//    }
//
//
//    public interface AddCouponCodeListner {
//        void addCouponCodeListner(OrderDetail results);
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
////        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
//        //this line is what you need:
//
//    }
//
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        return super.onCreateDialog(savedInstanceState);
//    }
//
//    @OnClick(R.id.tv_choose_from_map)
//    void doChooseFromMap() {
//        Opener.openChooseFromMap(getActivity());
//    }
//
//    @OnClick(R.id.btn_proceed)
//    void onProceedClicked() {
//        dismiss();
//        MainFragment.reloadHome();
//    }
//
//    @OnClick(R.id.tv_choose_title)
//    void doChooseMap() {
//        Opener.openChooseFromMap(getActivity());
    }
}
