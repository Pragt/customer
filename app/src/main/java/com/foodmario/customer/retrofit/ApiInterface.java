package com.foodmario.customer.retrofit;


import com.foodmario.customer.about_us.AboutUsResponse;
import com.foodmario.customer.checkOut.model.GetCartResponse;
import com.foodmario.customer.chooseFromMap.newAddressResponse.NewAddressToLatLng;
import com.foodmario.customer.comment.model.AddCommentResponse;
import com.foodmario.customer.comment.model.CommentResponse;
import com.foodmario.customer.commonInterface.NormalResponse;
import com.foodmario.customer.faq.model.FaqResponse;
import com.foodmario.customer.foodPreferences.model.FoodPreferencesResponse;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.chef.model.ChefListResponse;
import com.foodmario.customer.home.Fragments.home.model.GeLikeResponse;
import com.foodmario.customer.home.Fragments.home.model.GetFoodResponse;
import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionResponse;
import com.foodmario.customer.loginModule.model.LoginResponse;
import com.foodmario.customer.mySubscriptions.model.MySubscriptionNewResponse;
import com.foodmario.customer.orderHistory.model.MyOrderResponse;
import com.foodmario.customer.privacyPolicy.PrivacyPolicyResponse;
import com.foodmario.customer.register.model.AddressToLatLngResponse;
import com.foodmario.customer.register.model.GoogleAutocompleteResponse;
import com.foodmario.customer.subscriptionDetail.model.UpdateSubscriptionResponse;
import com.foodmario.customer.termsAndconditions.TermsAndConditionsResponse;
import com.foodmario.customer.vendorProfile.model.VendorProfileResponse;
import com.foodmario.customer.viewProfile.model.GetProfileResponse;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;


public interface ApiInterface {
    @FormUrlEncoded
    @POST("auth/login")
    Observable<LoginResponse> doLogin(@Field("email") String email,
                                      @Field("password") String password,
                                      @Field("facebook_token") String fbToken,
                                      @Field("device_id") String device_id,
                                      @Field("push_notification_token") String firenbaseToken,
                                      @Field("device_type") int deviceType,
                                      @Field("login_type") int loginType,
                                      @Field("user_type") int user_type);

    @Multipart
    @POST("auth/register")
    Observable<NormalResponse> doRegister(@Part("name") RequestBody fullName,
                                          @Part("email") RequestBody email,
                                          @Part("mobile_number") RequestBody phoneNo,
                                          @Part("location") RequestBody location,
                                          @Part("password") RequestBody password,
                                          @Part MultipartBody.Part image,
                                          @Part("lat") RequestBody latitude,
                                          @Part("long") RequestBody logitude,
                                          @Part("user_type") RequestBody user_type
    );


    @Multipart
    @POST("auth/completeProfile")
    Observable<LoginResponse> doCompleteYourProfile(@Part("name") RequestBody fullName,
                                                    @Part("email") RequestBody email,
                                                    @Part("mobile_number") RequestBody phoneNo,
                                                    @Part("location") RequestBody location,
                                                    @Part("facebook_token") RequestBody fbToken,
                                                    @Part MultipartBody.Part image,
                                                    @Part("fb_image") RequestBody fbImage,
                                                    @Part("lat") RequestBody latitude,
                                                    @Part("long") RequestBody logitude,
                                                    @Part("user_type") RequestBody user_type,
                                                    @Part("device_id") RequestBody device_id,
                                                    @Part("device_type") RequestBody device_type,
                                                    @Part("push_notification_token") RequestBody firebaseToken);

    @Multipart
    @POST("auth/updateProfile")
    Observable<LoginResponse> doUpdateYourProfile(@Header("Authorization") String auth_key,
                                                  @Part("name") RequestBody fullName,
                                                  @Part("mobile_number") RequestBody phoneNo,
                                                  @Part("location") RequestBody location,
                                                  @Part MultipartBody.Part image,
                                                  @Part("lat") RequestBody latitude,
                                                  @Part("long") RequestBody longitude,
                                                  @Part("about_me") RequestBody aboutMe,
                                                  @Part("vat_number") RequestBody vatNumber);

    @FormUrlEncoded
    @POST("auth/resendactivationemail")
    Observable<NormalResponse> resendVerificationCode(
            @Field("email") String email);

    @GET("auth/checkUser")
    Observable<NormalResponse> checkUser(
            @Header("Authorization") String auth_key);

    @FormUrlEncoded
    @POST("auth/logout")
    Observable<NormalResponse> logOut(
            @Header("Authorization") String auth_key, @Field("device_id") String deviceId);

    @GET(UrlHelpers.googleAutocomplete)
    Observable<GoogleAutocompleteResponse> getplaces(@Query("key") String apiKey,
                                                     @Query("input") String input,
                                                     @Query("components") String zone);

    @GET(UrlHelpers.googleAddressToLatLng)
    Call<AddressToLatLngResponse> googleAddressToLatlng(@Query("address") String address);

    @GET(UrlHelpers.googleAddressToLatLng)
    Call<NewAddressToLatLng> googleAddressToLatlngNew(@Query("address") String address);


    @POST("addFood")
    Observable<NormalResponse> addFood(@Header("Authorization") String auth_key,
                                       @Body JsonObject servingHourlist);

    @FormUrlEncoded
    @POST("customer/viewComment")
    Observable<CommentResponse> getComments(@Header("Authorization") String auth_key,
                                            @Field("food_id") int food_id);

    @FormUrlEncoded
    @POST("customer/comment")
    Observable<AddCommentResponse> addComment(@Header("Authorization") String auth_key,
                                              @Field("food_id") int food_id,
                                              @Field("comments") String comment,
                                              @Field("parent_id") String parentId);

    @GET("customer/getFoodPreference")
    Observable<FoodPreferencesResponse> getFoodPreferences(@Header("Authorization") String auth_key);

    @FormUrlEncoded
    @POST("customer/getCart")
    Observable<GetCartResponse> getMyOrders(@Header("Authorization") String auth_key,
                                            @Field("cart_id") int intValues);

    @GET("customer/getSubscription")
    Observable<SubscriptionResponse> getSubscription(@Header("Authorization") String auth_key);

    @FormUrlEncoded
    @POST("customer/vendorList")
    Observable<ChefListResponse> getChefList(@Header("Authorization") String auth_key,
                                             @Field("latitude") String latitude,
                                             @Field("longitude") String longitude,
                                             @Field("substring") String substring);

    @GET("customer/orderHistory")
    Observable<MyOrderResponse> getMyOrders(@Header("Authorization") String auth_key);

    @GET("customer/getMySubscription")
    Observable<MySubscriptionNewResponse> getMySubscription(@Header("Authorization") String auth_key);

    @POST("customer/setfoodpreference")
    Observable<NormalResponse> setFoodPreferences(@Header("Authorization") String auth_key,
                                                  @Body JsonObject foodPreferences);

    @POST("customer/addFeedback")
    Observable<NormalResponse> addFeedback(@Header("Authorization") String auth_key,
                                           @Body JsonObject foodPreferences);

    @FormUrlEncoded
    @POST("customer/deleteFromCart")
    Observable<GetCartResponse> deleteFromCart(@Header("Authorization") String auth_key,
                                               @Field("cart_id") int cart_id,
                                               @Field("food_id") int food_id);

    @POST("customer/updateCart")
    Observable<GetCartResponse> updateCart(@Header("Authorization") String auth_key,
                                           @Body JsonObject foodPreferences);

    @FormUrlEncoded
    @POST("customer/removeCouponCode")
    Observable<GetCartResponse> deleteCouponCode(@Header("Authorization") String auth_key,
                                                 @Field("cart_id") int cartId);

    @POST("customer/confirmOrder")
    Observable<GetCartResponse> confirmOrder(@Header("Authorization") String auth_key,
                                             @Body JsonObject foodPreferences);

    @POST("customer/deleteUnavailableFromCart")
    Observable<GetCartResponse> deleteUnavailable(@Header("Authorization") String auth_key,
                                                  @Body JsonObject foodPreferences);

    @FormUrlEncoded
    @POST("customer/getFood")
    Observable<GetFoodResponse> getFood(@Header("Authorization") String auth_key,
                                        @Field("latitude") String latitude,
                                        @Field("longitude") String logitude,
                                        @Field("value") String value,
                                        @Field("type") int type,
                                        @Field("substring") String substring,
                                        @Field("offset") int offset,
                                        @Field("filter_type") int filterId);

    @FormUrlEncoded
    @POST("customer/react")
    Observable<GeLikeResponse> toggleLike(@Header("Authorization") String auth_key,
                                          @Field("food_id") int food_id);

    @FormUrlEncoded
    @POST("customer/getVendorDetails")
    Observable<VendorProfileResponse> getVendorProfile(@Header("Authorization") String auth_key,
                                                       @Field("vendor_id") int vendor_id);

    @POST("customer/addToCart")
    Observable<GetCartResponse> addToCart(@Header("Authorization") String auth_key,
                                          @Body JsonObject foodDetail);

    @FormUrlEncoded
    @POST("customer/useCouponCode")
    Observable<GetCartResponse> useCouponCode(@Header("Authorization") String auth_key,
                                              @Field("cart_id") int cart_id,
                                              @Field("coupon_code") String couponCode);

    @FormUrlEncoded
    @POST("auth/updatePassword")
    Observable<NormalResponse> changePassword(@Header("Authorization") String auth_key,
                                              @Field("current_password") String currentPassword,
                                              @Field("new_password") String newPassword);

    @FormUrlEncoded
    @POST("auth/forgetpassword")
    Observable<NormalResponse> forgetPassword(@Field("email") String email);

    @GET("customer/privacyPolicy")
    Observable<PrivacyPolicyResponse> getPrivacyPolicy(@Header("Authorization") String auth_key);

    @GET("customer/termsConditions")
    Observable<TermsAndConditionsResponse> getTermsAndConditions(@Header("Authorization") String auth_key);

    @GET("customer/aboutUs")
    Observable<AboutUsResponse> getAboutUs(@Header("Authorization") String auth_key);

    @GET("customer/faq")
    Observable<FaqResponse> getFaqs(@Header("Authorization") String auth_key);

    @GET("auth/profile")
    Observable<GetProfileResponse> loadProfile(@Header("Authorization") String auth_key);

    @POST("customer/updateSubscription")
    Observable<UpdateSubscriptionResponse> updateSubscription(@Header("Authorization") String auth_key,
                                                              @Body JsonObject foodPreferences);

    @POST("customer/confirmSubscription")
    Observable<NormalResponse> subscribe(@Header("Authorization") String auth_key,
                                         @Body JsonObject foodPreferences);

    @POST("customer/resumeSubscription")
    Observable<NormalResponse> resume(@Header("Authorization") String auth_key,
                                      @Body JsonObject foodPreferences);
}
