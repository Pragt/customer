package com.foodmario.customer.register.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AddressToLatLngResponse {

    @SerializedName("results")
    @Expose
    public List<Result> results = new ArrayList<Result>();
    @SerializedName("status")
    @Expose
    public String status;

    public class Result {

        @SerializedName("address_components")
        @Expose
        public List<AddressComponent> addressComponents = new ArrayList<AddressComponent>();
        @SerializedName("formatted_address")
        @Expose
        public String formattedAddress;
        @SerializedName("geometry")
        @Expose
        public Geometry geometry;
        @SerializedName("place_id")
        @Expose
        public String placeId;
        @SerializedName("types")
        @Expose
        public List<String> types = new ArrayList<String>();

        public class AddressComponent {

            @SerializedName("long_name")
            @Expose
            public String longName;
            @SerializedName("short_name")
            @Expose
            public String shortName;
            @SerializedName("types")
            @Expose
            public List<String> types = new ArrayList<String>();

        }

        public class Geometry {

            @SerializedName("bounds")
            @Expose
            public Bounds bounds;
            @SerializedName("location")
            @Expose
            public Location location;
            @SerializedName("location_type")
            @Expose
            public String locationType;
            @SerializedName("viewport")
            @Expose
            public Viewport viewport;

            public class Location {
                @SerializedName("delivery_lat")
                @Expose
                public Float lat;
                @SerializedName("delivery_lng")
                @Expose
                public Float lng;
            }

            public class Bounds {

                @SerializedName("northeast")
                @Expose
                public Northeast northeast;
                @SerializedName("southwest")
                @Expose
                public Southwest southwest;

                public class Northeast {

                    @SerializedName("delivery_lat")
                    @Expose
                    public Float lat;
                    @SerializedName("delivery_lng")
                    @Expose
                    public Float lng;

                }

                public class Southwest {

                    @SerializedName("delivery_lat")
                    @Expose
                    public Float lat;
                    @SerializedName("delivery_lng")
                    @Expose
                    public Float lng;

                }

            }

            public class Viewport {

                @SerializedName("northeast")
                @Expose
                public Northeast_ northeast;
                @SerializedName("southwest")
                @Expose
                public Southwest_ southwest;

                public class Northeast_ {

                    @SerializedName("delivery_lat")
                    @Expose
                    public Float lat;
                    @SerializedName("delivery_lng")
                    @Expose
                    public Float lng;

                }

                public class Southwest_ {

                    @SerializedName("delivery_lat")
                    @Expose
                    public Float lat;
                    @SerializedName("delivery_lng")
                    @Expose
                    public Float lng;

                }

            }

        }

    }

}