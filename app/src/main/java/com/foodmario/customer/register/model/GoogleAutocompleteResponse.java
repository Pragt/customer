package com.foodmario.customer.register.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GoogleAutocompleteResponse {

    @SerializedName("predictions")
    @Expose
    public List<Prediction> predictions = new ArrayList<Prediction>();
    @SerializedName("status")
    @Expose
    public String status;


    public class Prediction {

        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("matched_substrings")
        @Expose
        public List<MatchedSubstring> matchedSubstrings = new ArrayList<MatchedSubstring>();
        @SerializedName("place_id")
        @Expose
        public String placeId;
        @SerializedName("reference")
        @Expose
        public String reference;
        @SerializedName("terms")
        @Expose
        public List<Term> terms = new ArrayList<Term>();
        @SerializedName("types")
        @Expose
        public List<String> types = new ArrayList<String>();

        public class MatchedSubstring {

            @SerializedName("length")
            @Expose
            public Integer length;
            @SerializedName("offset")
            @Expose
            public Integer offset;

        }

        public class Term {

            @SerializedName("offset")
            @Expose
            public Integer offset;
            @SerializedName("value")
            @Expose
            public String value;

        }
    }

    public static ArrayList<String> getList(GoogleAutocompleteResponse response) {
        ArrayList<String> autoCompleteList = new ArrayList<String>();

        for (int i = 0; i < response.predictions.size(); i++) {
            autoCompleteList.add(response.predictions.get(i).description);
        }
        return autoCompleteList;
    }

    public static String[] getStringList(GoogleAutocompleteResponse response) {
        ArrayList<String> autoCompleteList = new ArrayList<String>();

        for (int i = 0; i < response.predictions.size(); i++) {
            autoCompleteList.add(response.predictions.get(i).description);
        }
        String[] stringArr = new String[autoCompleteList.size()];
        stringArr = autoCompleteList.toArray(stringArr);
        return stringArr;
    }
}