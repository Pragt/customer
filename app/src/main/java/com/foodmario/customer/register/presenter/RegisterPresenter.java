package com.foodmario.customer.register.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.register.model.RegisterObject;
import com.foodmario.customer.register.view.IRegisterView;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 12/23/17.
 */

public class RegisterPresenter {
    public IRegisterView view;
    SharedPreference sharedPreference;

    public RegisterPresenter(IRegisterView registerView, SharedPreference preference) {
        this.view = registerView;
        this.sharedPreference = preference;
        this.view.init();
    }

    public void doRegister(RegisterObject object) {
        if (isValid(object)) {
            view.onShowProgressDialog("Signing Up... Please wait");

            RequestBody fullName = RequestBody.create(MediaType.parse("multipart/form-data"), object.fullName);
            RequestBody email = RequestBody.create(MediaType.parse("multipart/form-data"), object.email);
            RequestBody location = RequestBody.create(MediaType.parse("multipart/form-data"), object.location);
            RequestBody mobile_number = RequestBody.create(MediaType.parse("multipart/form-data"), object.phoneNo);
            RequestBody password = RequestBody.create(MediaType.parse("multipart/form-data"), object.password);
            RequestBody latitude = RequestBody.create(MediaType.parse("multipart/form-data"), object.latitude);
            RequestBody longitude = RequestBody.create(MediaType.parse("multipart/form-data"), object.longitude);
            RequestBody user_type = RequestBody.create(MediaType.parse("multipart/form-data"), object.userType);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            MultipartBody.Part bodyProfilePic = null;
            if (!object.image.equalsIgnoreCase("")) {
                File profile_pic = new File(object.image);

                RequestBody requestProfileFile = RequestBody.create(MediaType.parse("image/jpeg"), profile_pic);

                bodyProfilePic = MultipartBody.Part.createFormData("profile_pic", profile_pic.getName(), requestProfileFile);

            }

            ApiClient.getClient().create(ApiInterface.class).doRegister(fullName, email, mobile_number, location, password, bodyProfilePic, latitude, longitude, user_type)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                                view.onHideProgressDialog();
                                if (s.statusCode == CommonDef.Response.SUCCESS) {
                                    view.onSuccess(s.message);

                                } else {
                                    view.onError(s.message);

                                }
                            },
                            e -> {
                                view.onError(CommonMethods.getErrorTye(e));
                                view.onHideProgressDialog();
                                e.printStackTrace();
                            },
                            () -> System.out.println("supervisor list"));
        }
    }

    private boolean isValid(RegisterObject object) {
        if (object.fullName.isEmpty()) {
            view.onValidationError("Please Enter Full Name");
            return false;
        } else if (object.email.isEmpty()) {
            view.onValidationError("Please Enter Your Email");
            return false;
        } else if (!CommonMethods.isValidEmail(object.email)) {
            view.onValidationError("Please Enter Valid Email");
            return false;
        } else if (object.phoneNo.isEmpty()) {
            view.onValidationError("Please Enter Your Mobile Number");
            return false;
        } else if (!CommonMethods.isValidPhoneNumber(object.phoneNo)) {
            view.onValidationError("Please Enter Valid Mobile Number");
            return false;
        } else if (object.password.isEmpty()) {
            view.onValidationError("Please Enter Password");
            return false;
        } else if (object.rePassword.isEmpty()) {
            view.onValidationError("Please Re-Enter Password");
            return false;
        } else if (!object.password.equalsIgnoreCase(object.rePassword)) {
            view.onValidationError("Password and Re-type password must match");
            return false;
        }
        return true;
    }
}
