package com.foodmario.customer.register.model;

/**
 * Created by prajit on 12/23/17.
 */

public class RegisterObject {
    public String fullName;
    public String email;
    public String phoneNo;
    public String location;
    public String password;
    public String rePassword;
    public String image = "";
    public String latitude;
    public String longitude;
    public String userType;
    public String accessToken;
    public String aboutMe;
    public String fbImage;
    public String firebaseToken;
    public String vatNumber;
}
