package com.foodmario.customer.helpers;

/**
 * Created by prajit on 12/17/17.
 */

public class UrlHelpers {

    public static final String BASE_URL = "https://www.foodmario.com/api/";
    public static final String googleAutocomplete = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
//    public static final String googleAddressToLatLng = "https://maps.googleapis.com/maps/api/geocode/json";
    public static final String googleAddressToLatLng = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyARMDTkE4BNrnBwZj-TkCIpvj3mnfd8Q8s";
    public static final String SUBSCRIPTION_BASE_URL = "https://www.foodmario.com/images/subscription/";
    public static String FOOD_BASE_URL = "https://www.foodmario.com/images/food/";
    public static String USER_BASE_URL = "https://www.foodmario.com/images/user/";
    public static String CATEGORY_FOOD_URL = "https://www.foodmario.com/images/food-category/";
}
