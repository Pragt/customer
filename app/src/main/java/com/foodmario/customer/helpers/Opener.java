package com.foodmario.customer.helpers;

import android.app.Activity;
import android.content.Intent;

import com.foodmario.customer.ChooseLocationActivity;
import com.foodmario.customer.MainActivity;
import com.foodmario.customer.checkOut.model.OrderDetail;
import com.foodmario.customer.checkOut.view.MyCheckoutActivity;
import com.foodmario.customer.chooseFromMap.ChooseFromMapActivity;
import com.foodmario.customer.comment.view.CommentActivity;
import com.foodmario.customer.completeProfile.view.CompleteYourProfileActivity;
import com.foodmario.customer.foodDetail.view.FoodDetailActivity;
import com.foodmario.customer.foodPreferences.view.ChooseFoodPreferencesActivity;
import com.foodmario.customer.forget_password.ForgetPasswordActivity;
import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionPackage;
import com.foodmario.customer.loginModule.view.LoginActivity;
import com.foodmario.customer.mySubscriptions.model.Subscription;
import com.foodmario.customer.mySubscriptions.mySubscriptionDetail.MySubscriptionDetailActivity;
import com.foodmario.customer.orderDetail.OrderDetailActivity;
import com.foodmario.customer.orderHistory.model.RecentOrder;
import com.foodmario.customer.orderHistory.view.OrderHistoryFragmentNew;
import com.foodmario.customer.placeSubscription.PlaceSubscriptionActivity;
import com.foodmario.customer.register.view.RegisterActivity;
import com.foodmario.customer.subscriptionDetail.SubscriptionDetailActivity;
import com.foodmario.customer.thankyouscreen.ThankyouActivity;
import com.foodmario.customer.thankyouscreen.ThankyouSubscriptionActivity;
import com.foodmario.customer.vendorProfile.model.Food;
import com.foodmario.customer.vendorProfile.model.Vendor;
import com.foodmario.customer.vendorProfile.view.VendorProfile;
import com.foodmario.customer.viewProfile.view.ViewProfileActivity;

/**
 * Created by prajit on 12/17/17.
 */

public class Opener {
    public static void openCompleteProfileActivity(Activity activity, String name, String email, String fb_image, String firebase_token, String accessToken) {
        Intent intent = new Intent(activity, CompleteYourProfileActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("email", email);
        intent.putExtra("fb_token", accessToken);
        intent.putExtra("fb_image", fb_image);
        intent.putExtra("firebase_token", firebase_token);
        activity.startActivity(intent);
    }

    public static void openSignUpActivity(Activity activity) {
        Intent intent = new Intent(activity, RegisterActivity.class);
        activity.startActivity(intent);
    }

    public static void openHomeActivity(Activity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        activity.finishAffinity();
    }

    public static void openCommentActivity(Activity activity, Integer food_id, int newPos) {
        Intent intent = new Intent(activity, CommentActivity.class);
        intent.putExtra(CommonDef.FOOD_ID, food_id);
        intent.putExtra("pos", newPos);
        activity.startActivityForResult(intent, CommonDef.RESPONSE_COMMENT);
    }

    public static void LoginActivity(Activity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }

    public static void back(Activity activity) {
        activity.finish();
    }

    public static void openChooseFoodPreference(Activity activity, boolean isFirstTime) {
        Intent intent = new Intent(activity, ChooseFoodPreferencesActivity.class);
        activity.startActivity(intent);
    }

    public static void openVendorProfile(Activity activity, Integer id) {
        Intent intent = new Intent(activity, VendorProfile.class);
        intent.putExtra(CommonDef.VENDOR_ID, id);
        activity.startActivity(intent);
    }

    public static void openFoodDetail(Activity activity, Food food, Vendor vendor) {
        Intent intent = new Intent(activity, FoodDetailActivity.class);
        intent.putExtra(CommonDef.FOOD_DETAIL, food);
        intent.putExtra(CommonDef.VENDOR_PROFILE, vendor);
        activity.startActivityForResult(intent, 13);
    }

    public static void openCheckoutActivity(Activity activity, OrderDetail orderResponse) {
        Intent intent = new Intent(activity, MyCheckoutActivity.class);
        intent.putExtra(CommonDef.ORDER_DETAIL, orderResponse);
        activity.startActivityForResult(intent, 11);
    }

    public static void openOrderHistory(Activity activity) {
        Intent intent = new Intent(activity, OrderHistoryFragmentNew.class);
        activity.startActivity(intent);
    }

    public static void openProfileActivity(Activity activity) {
        Intent intent = new Intent(activity, ViewProfileActivity.class);
        activity.startActivity(intent);
    }

    public static void openThankyouScreen(Activity activity) {
        Intent intent = new Intent(activity, ThankyouActivity.class);
        activity.startActivity(intent);
    }

    public static void openThankyouSubscriptionScreen(Activity activity) {
        Intent intent = new Intent(activity, ThankyouSubscriptionActivity.class);
        activity.startActivity(intent);
    }

    public static void openForgetPass(Activity activity) {
        Intent intent = new Intent(activity, ForgetPasswordActivity.class);
        activity.startActivity(intent);
    }

    public static void openOrderDetailActivity(Activity activity, RecentOrder delivered) {
        Intent intent = new Intent(activity, OrderDetailActivity.class);
        intent.putExtra(CommonDef.ORDER_DETAIL, delivered);
        activity.startActivity(intent);
    }

    public static void openSubscrtionDetail(Activity activity, SubscriptionPackage result) {
        Intent intent = new Intent(activity, SubscriptionDetailActivity.class);
        intent.putExtra("subscription", result);
        activity.startActivity(intent);
    }

    public static void openPlaceSubscriptionActivity(Activity activity, SubscriptionPackage subscriptions) {
        Intent intent = new Intent(activity, PlaceSubscriptionActivity.class);
        if (subscriptions != null) {
            intent.putExtra("is_resume", false);
            intent.putExtra("subscriptions", subscriptions);
        }
        activity.startActivity(intent);
    }

    public static void openResumeSubscriptionActivity(Activity activity, int id, String type) {
        Intent intent = new Intent(activity, PlaceSubscriptionActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("is_resume", true);
        intent.putExtra("type", type);
        activity.startActivity(intent);
    }

    public static void openChooseFromMap(Activity activity, boolean isFromHome) {
        Intent intent = new Intent(activity, ChooseFromMapActivity.class);
        intent.putExtra(CommonDef.IS_FROM_HOME, isFromHome);
        activity.startActivity(intent);
    }

    public static void openChooseDeliveryLocation(Activity activity) {
        Intent intent = new Intent(activity, ChooseLocationActivity.class);
        activity.startActivity(intent);
    }

    public static void openSubscriptionDetail(Activity activity, Subscription routine) {
        Intent intent = new Intent(activity, MySubscriptionDetailActivity.class);
        intent.putExtra("subscription", routine);
        activity.startActivity(intent);
    }
}
