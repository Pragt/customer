package com.foodmario.customer.helpers;

/**
 * Created by prajit on 10/18/16.
 */

import android.app.Activity;
import android.app.ProgressDialog;

import retrofit2.http.Body;


public class CustomProgressDialog {

    ProgressDialog mProgress;

    public CustomProgressDialog(Activity activity) {
        mProgress = new ProgressDialog(activity);
        mProgress.setIndeterminate(false);
    }

    public void showpd(String message) {
        mProgress.setMessage(message);
        mProgress.setCancelable(false);
        if (!mProgress.isShowing()) {
            mProgress.show();
        }
    }

    public void hidepd() {
        if (mProgress.isShowing()) {
            mProgress.dismiss();
        }
    }

    public Boolean isLoading(){
        return mProgress.isShowing();
    }
}
