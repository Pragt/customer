package com.foodmario.customer.helpers;

/**
 * Created by prajit on 4/28/18.
 */

public interface LoginType {
    int FACEBOOK = 1;
    int NORMAL = 2;
}
