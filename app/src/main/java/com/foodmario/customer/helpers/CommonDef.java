package com.foodmario.customer.helpers;

/**
 * Created by prajit on 12/17/17.
 */

public class CommonDef {
    public static final int REQUEST_STORAGE_CAMERA = 100;
    public static final String ZONE = "country:np";
    public static final String FOOD_DETAIL = "Food_Detail";
    public static final String FOOD_ID = "food_id";
    public static final String VENDOR_ID = "Vendor_ID";
    public static final String VENDOR_PROFILE = "Vendor_profile";
    public static final String NETWORK_ERROR = "Something went wrong. Please try again later";
    public static final String ORDER_DETAIL = "Order Detail";
    public static final int REQUEST_CHECK_SETTINGS = 11;
    public static final int RESPONSE_COMMENT = 101;
    public static final String DELIVERY_LOCATION = "DeliveryLocation";
    public static final String DELIVERY_LAT = "lat";
    public static final String DELIVERY_LON = "lng";
    public static final int GO_BACK = 12;
    public static final String GO_TO_SUBSCRIPTION = "GoToSubscription";
    public static final String IS_FB_LOGIN = "Is_Fb_Login";
    public static final String LOGIN_TYPE = "Login Type";
    public static final String IS_FROM_HOME = "Is_From_Home";
    public static String TAG= "FoodMario" ;
    public static String No_Connection = "No connection. Please check your internet connectivity and try again.";
    public static final String GO_TO_MY_ORDER="Go_to_my_order";

    public static class Response {
        public static int SUCCESS = 200;
    }

    public class SharedPrefrences {
        public static final String AUTH_KEY = "Auth_key";
        public static final String IS_LOGGED_IN= "Is_logged_in";
        public static final String HAS_CHOOSED_FOOD_PREFERENCES = "Has_choosed_food_pref";
        public static final String FULL_NAME = "FullName";
        public static final String CART_ID = "Cart_Id";
        public static final String PROFILE_PIC = "User_image";
        public static final String EMAIL = "Email";
        public static final String MOBILE = "Mobile";
        public static final String ADDRESS = "Address";
        public static final String User_desc= "description";
        public static final String LATITUDE = "Latitude";
        public static final String LONGITUDE = "Longitude";
        public static final String DELIVERY_LOCATION = "DeliveryLocation";
        public static final String FILTERED_ID = "FILTERED_ID";
        public static final String VAT_NUMBER = "Vat_number";
    }
}
