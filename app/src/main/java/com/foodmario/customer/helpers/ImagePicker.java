package com.foodmario.customer.helpers;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.system.ErrnoException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prajit on 3/22/17.
 * <p>
 * *******cALL THIS FUNCTION ON BUTTON CLICK*******
 * private void choosePictures() {
 * if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
 * if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
 * ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
 * ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
 * return;
 * }
 * }
 * startActivityForResult(ImagePicker.getPickImageChooserIntent(getActivity()), 200);
 * }
 * Add this provider in manifest file
 * <provider
 * android:name="android.support.v4.content.FileProvider"
 * android:authorities="${applicationId}.provider"
 * android:exported="false"
 * android:grantUriPermissions="true">
 * <meta-data
 * android:name="android.support.FILE_PROVIDER_PATHS"
 * android:resource="@xml/provider_paths" />
 * </provider>
 */

/**
 * *******cALL THIS FUNCTION ON BUTTON CLICK*******
 * private void choosePictures() {
 * if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
 * if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
 * ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
 * ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
 * return;
 * }
 * }
 * startActivityForResult(ImagePicker.getPickImageChooserIntent(getActivity()), 200);
 * }
 */

/** Add this provider in manifest file
 *  <provider
 android:name="android.support.v4.content.FileProvider"
 android:authorities="${applicationId}.provider"
 android:exported="false"
 android:grantUriPermissions="true">
 <meta-data
 android:name="android.support.FILE_PROVIDER_PATHS"
 android:resource="@xml/provider_paths" />
 </provider>
 */

/** provider_paths (xml)
 *  <provider
 android:name="android.support.v4.content.FileProvider"
 android:authorities="${applicationId}.provider"
 android:exported="false"
 android:grantUriPermissions="true">
 <meta-data
 android:name="android.support.FILE_PROVIDER_PATHS"
 android:resource="@xml/provider_paths" />
 </provider>
 */

public class ImagePicker {

    private static File getImage;
    private static Uri outputFileUri;
    private static String outputImagePath;

    public static Intent getPickImageChooserIntent(Activity activity) {

// Determine Uri of camera image to  save.
//        getImage = activity.getExternalCacheDir();
        getImage = new File(FileUtils.getImageDirectory(activity), FileUtils.getUniqueImageName());
//        outputFileUri = getCaptureImageOutputUri(activity);
        outputImagePath = getImage.getPath();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = activity.getPackageManager();

// collect all camera intents
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                outputFileUri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", getImage);
                captureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            } else
                outputFileUri = getCaptureImageOutputUri(activity);

            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }

            allIntents.add(intent);
        }

// collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

// the main intent is the last in the  list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    public static Intent getGallaryImageChooserIntent(Activity activity) {

// Determine Uri of camera image to  save.
//        getImage = activity.getExternalCacheDir();
        getImage = new File(FileUtils.getImageDirectory(activity), FileUtils.getUniqueImageName());
//        outputFileUri = getCaptureImageOutputUri(activity);
        outputImagePath = getImage.getPath();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = activity.getPackageManager();

// collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

// the main intent is the last in the  list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture  by camera.
     */
    private static Uri getCaptureImageOutputUri(Activity activity) {
        outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpg"));
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from  {@link #//getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera  and gallery image.
     *
     * @param - returned data of the  activity result
     */
    public static Uri getPickImageResultUri() {
        return outputFileUri;
    }

    /**
     * Test if we can open the given Android URI to test if permission required error is thrown.<br>
     */
    public static boolean isUriRequiresPermissions(Activity activity, Uri uri) {
        try {
            ContentResolver resolver = activity.getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }
}
