package com.foodmario.customer.helpers;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.Toast;

import com.foodmario.customer.R;

/**
 * Created by pragt on 5/14/17.
 */

public class Alerts {
    private final LayoutInflater li;
    Activity activity;

    public Alerts(Activity activity) {
        this.activity = activity;
        li = LayoutInflater.from(activity);
    }

    public void showToastMsg(String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }

    public void showSuccessAlert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        alertDialog.setMessage(msg);
        alertDialog.setTitle("Foodmario");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void showSuccessAlerWithoutFinish(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        alertDialog.setMessage(msg);
        alertDialog.setTitle("Foodmario");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void showErrorAlert(String msg) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(activity, R.color.red_3));
        snackbar.show();
    }

    public void showErrorAlertWithFinish(String msg) {
//        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
//                .setTitleText("Oops...")
//                .setContentText(msg)
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sweetAlertDialog) {
//                        activity.finish();
//                    }
//                })
//                .show();

        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        alertDialog.setMessage(msg);
        alertDialog.setTitle("Foodmario");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        activity.finish();
                    }
                });

        Button buttonbackground = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        buttonbackground.setBackgroundColor(Color.BLUE);

        Button buttonbackground1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.BLUE);
        alertDialog.show();
    }

    public void showWarningAlert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        alertDialog.setTitle("Foodmario");
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void showLogOutConfirmationDialog(String message, final OnConfirmationClickListener listener, int requestCode) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        alertDialog.setTitle("Foodmario");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Log out",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        listener.onYesClicked();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        listener.onNoClicked();
                    }
                });
        alertDialog.show();
    }

    public void showDeleteConfirmationDialog(String message, final OnConfirmationClickListener listener, int requestCode) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Foodmario");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Remove",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        listener.onYesClicked();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        listener.onNoClicked();
                    }
                });
        alertDialog.show();
    }

    public void showWarningDialog(String message, final OnConfirmationClickListener listener, int requestCode) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        alertDialog.setMessage(message);
        alertDialog.setTitle("Foodmario");
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onYesClicked();
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void showInactiveAccountDialog(final OnConfirmationClickListener listener, int requestCode) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.alert_dialog).create();
        alertDialog.setMessage("Account inactive. A verification email has been sent to your email address");
        alertDialog.setTitle("Foodmario");
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Resend verification code",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onYesClicked();
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }




    public interface OnConfirmationClickListener {
        void onYesClicked();

        void onNoClicked();
    }
}
