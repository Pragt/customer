package com.foodmario.customer.comment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("results")
    @Expose
    public List<Comments> comments = null;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;

}