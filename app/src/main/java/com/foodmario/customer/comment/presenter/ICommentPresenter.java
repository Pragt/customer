package com.foodmario.customer.comment.presenter;

/**
 * Created by prajit on 1/20/18.
 */

public interface ICommentPresenter {
    void getComments(int food_id);

    void addComment(String comment, int food_id, int parentId, int commentPos);
}
