package com.foodmario.customer.comment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddCommentResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public Comments comments;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}