package com.foodmario.customer.comment.presenter;

import com.foodmario.customer.comment.view.ICommentView;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/20/18.
 */

public class CommentPresenter implements ICommentPresenter {
    SharedPreference sharedPrefrences;
    ICommentView view;

    public CommentPresenter(SharedPreference sharedPreference, ICommentView view) {
        this.sharedPrefrences = sharedPreference;
        this.view = view;
        this.view.init();
    }

    @Override
    public void getComments(int food_id) {
        ApiClient.getClient().create(ApiInterface.class).getComments(sharedPrefrences.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), food_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (view != null) {
                                view.onHideProgressDialog();
                                if (s.statusCode == CommonDef.Response.SUCCESS) {
                                    view.onGetList(s.comments);
                                } else {
                                    view.onError(s.message);
                                }
                            }
                        },
                        e -> {
                            if (view != null) {
                               view.onError(CommonMethods.getErrorTye(e));
                                view.onHideProgressDialog();
                            }
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    @Override
    public void addComment(String comment, int food_id, int parentId, int commentPos) {
        ApiClient.getClient().create(ApiInterface.class).addComment(sharedPrefrences.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), food_id, comment, String.valueOf(parentId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (view != null) {
                                view.onHideProgressDialog();
                                if (s.statusCode == CommonDef.Response.SUCCESS) {
                                    view.onCommentSuccess(s.comments, commentPos);
                                } else {
                                    view.onError(s.message);
                                }
                            }
                        },
                        e -> {
                            if (view != null) {
                               view.onError(CommonMethods.getErrorTye(e));
                                view.onHideProgressDialog();
                            }
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    public void unbindView() {
        this.view = null;
    }
}
