package com.foodmario.customer.comment.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.comment.model.Comments;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.utils.LoadImage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class MyCommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    ReplyClickListner mListner;
    public List<Comments> comments;

    public MyCommentAdapter(Context mContext, List<Comments> comments) {
        this.mContext = mContext;
        this.comments = comments;

    }

    public void setListner(ReplyClickListner listner) {
        this.mListner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_layout_comments, null);
        return new myCategoriesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            holder1.tvName.setText(comments.get(position).userName);
            holder1.tvComment.setText(comments.get(position).comments);
            holder1.tvDate.setText(getDate(comments.get(position).date));
            holder1.llReply.removeAllViews();

            LoadImage.loadCircularUserImage((Activity) mContext, holder1.ivProfileImage, UrlHelpers.USER_BASE_URL + comments.get(position).userImage, R.drawable.ic_man_1, R.drawable.ic_man_1);

            for (int i = 0; i < comments.get(position).childComment.size(); i++) {
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_layout_reply, null);
                ImageView ivUserImage = (ImageView) view.findViewById(R.id.iv_profile_img);
                TextView tvUserName = (TextView) view.findViewById(R.id.tv_name);
                TextView tvComment = (TextView) view.findViewById(R.id.tv_comment);

                LoadImage.loadCircularUserImage((Activity) mContext, ivUserImage, UrlHelpers.USER_BASE_URL + comments.get(position).childComment.get(i).userImage, R.drawable.ic_man_1, R.drawable.ic_man_1);
                tvUserName.setText(comments.get(position).childComment.get(i).userName);
                tvComment.setText(comments.get(position).childComment.get(i).comments);

                holder1.llReply.addView(view);
            }

            holder1.tvReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListner.doReply(position);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void addComment(Comments commentObj) {
        comments.add(commentObj);
        notifyItemInserted(comments.size() - 1);
    }

    public void addReply(Comments comment, int commentPos) {
        comments.set(commentPos, comment);
        notifyItemChanged(commentPos);
    }

    public void addComment(int pos) {

    }

    public void addAdd(List<Comments> comments) {
        this.comments = comments;
        notifyDataSetChanged();
    }

    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_reply)
        TextView tvReply;

        @BindView(R.id.tv_date)
        TextView tvDate;

        @BindView(R.id.tv_comment)
        TextView tvComment;

        @BindView(R.id.iv_profile_img)
        ImageView ivProfileImage;

        @BindView(R.id.ll_reply)
        LinearLayout llReply;

        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface ReplyClickListner {

        void doReply(int position);
    }

    public static String getDate(String dateTime) {
        if (dateTime == null)
            return "";
        if (dateTime.isEmpty() || dateTime.equalsIgnoreCase("0"))
            return "";

        SimpleDateFormat formattor = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = formattor.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdfs = new SimpleDateFormat("dd MMM yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return date == null ? "" : sdfs.format(date);

    }


}
