package com.foodmario.customer.comment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Comments {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("food_id")
    @Expose
    public Integer foodId;
    @SerializedName("comments")
    @Expose
    public String comments;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("parent_id")
    @Expose
    public Integer parentId;
    @SerializedName("child_comment")
    @Expose
    public List<ChildComment> childComment = null;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("user_image")
    @Expose
    public String userImage;

}