package com.foodmario.customer.comment.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.comment.adapter.MyCommentAdapter;
import com.foodmario.customer.comment.model.Comments;
import com.foodmario.customer.comment.presenter.CommentPresenter;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.utils.LoadImage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CommentActivity extends AppCompatActivity implements ICommentView, MyCommentAdapter.ReplyClickListner {

    @BindView(R.id.rv_comments)
    RecyclerView rvComments;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.pb_comment)
    ProgressBar pbComment;

    @BindView(R.id.iv_tick)
    ImageView ivTick;

    @BindView(R.id.rl_send)
    RelativeLayout rlSend;

    @BindView(R.id.tv_loading)
    TextView tvLoading;

    @BindView(R.id.iv_profile_pic)
    ImageView ivProfilePic;


    @BindView(R.id.et_comment)
    EditText edtComment;
    private MyCommentAdapter mAdapter;

    CommentPresenter presenter;
    SharedPreference sharedPreference;
    Alerts alerts;
    int food_id;
    private List<Comments> comments;
    private Integer parent_id = 0;
    int commentPos = -1;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        CommonMethods.setupUI(findViewById(R.id.rl_comment_activity), this);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        presenter = new CommentPresenter(sharedPreference, this);
        food_id = getIntent().getIntExtra(CommonDef.FOOD_ID, 0);
        pos = getIntent().getIntExtra("pos", 0);

        presenter.getComments(food_id);
    }

    @Override
    public void init() {
        alerts = new Alerts(this);
        tvTitle.setText("Comments");
        rvComments.setLayoutManager(new LinearLayoutManager(this));
        comments = new ArrayList<>();
        mAdapter = new MyCommentAdapter(this, comments);
        mAdapter.setListner(this);
        rvComments.setAdapter(mAdapter);

        LoadImage.loadCircularUserImage(this, ivProfilePic, UrlHelpers.USER_BASE_URL + sharedPreference.getStringValues(CommonDef.SharedPrefrences.PROFILE_PIC), R.drawable.ic_man_1, R.drawable.ic_man_1);

        edtComment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (edtComment.getText().toString().isEmpty()) {
                        commentPos = -1;
                        parent_id = 0;
                    }
                }
                else
                {

                }
            }
        });
    }


    @Override
    public void onShowProgressDialog(String msg) {

    }

    @Override
    public void onHideProgressDialog() {

    }

    @Override
    public void onSuccess(String msg) {

    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void onValidationError(String msg) {

    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("pos", pos);
        int count = 0;
        for (int i = 0; i < comments.size(); i++)
        {
            count ++;
            for (int j = 0; j < comments.get(i).childComment.size(); j++)
            {
                count++;
            }
        }
        intent.putExtra("count", count);
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.rl_send)
    void onSendClicked() {
        if (!edtComment.getText().toString().isEmpty()) {
            presenter.addComment(edtComment.getText().toString(), food_id, parent_id, commentPos);
            ivTick.setVisibility(View.GONE);
            pbComment.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetList(List<Comments> comments) {
        this.comments = comments;
        if (this.comments.size() == 0) {
            tvLoading.setText("No comments available.");
        } else {
            tvLoading.setVisibility(View.GONE);
            mAdapter = new MyCommentAdapter(this, comments);
            mAdapter.setListner(this);
            rvComments.setAdapter(mAdapter);
        }
    }

    @Override
    public void onCommentSuccess(Comments comment, int commentPos) {
        tvLoading.setVisibility(View.GONE);
        if (commentPos != -1) {
//            mAdapter.addReply(comment, commentPos);
            this.comments.set(commentPos, comment);
            mAdapter.notifyItemChanged(commentPos);
            rvComments.scrollToPosition(commentPos);
        } else {
            this.comments.add(comment);
            mAdapter.notifyItemInserted(comments.size() - 1);
            if (comments.size() == 1)
                mAdapter.addAdd(comments);
            rvComments.scrollToPosition(mAdapter.comments.size() - 1);
//            mAdapter.addComment(comment);
        }

        ivTick.setVisibility(View.VISIBLE);
        pbComment.setVisibility(View.GONE);
        edtComment.setFocusable(true);
        edtComment.setClickable(true);
        edtComment.setText("");
        parent_id = 0;
        this.commentPos = -1;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unbindView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void doReply(int position) {
        this.parent_id = this.comments.get(position).id;

        commentPos = position;
        edtComment.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtComment, InputMethodManager.SHOW_IMPLICIT);
    }
}
