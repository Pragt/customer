package com.foodmario.customer.comment.view;


import com.foodmario.customer.comment.model.Comments;
import com.foodmario.customer.commonInterface.IcommonView;

import java.util.List;

/**
 * Created by prajit on 1/20/18.
 */

public interface ICommentView extends IcommonView {
    void onGetList(List<Comments> comments);

    void onCommentSuccess(Comments comment, int commentPos);
}
