package com.foodmario.customer.faq;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.faq.model.Faqs;
import com.foodmario.customer.helpers.CommonMethods;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class FaqAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    int openedPos = -1;

    List<Faqs> data;

    public FaqAdapter(Context mContext, List<Faqs> data) {
        this.mContext = mContext;
        this.data = data;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_faq, null);
        return new HelpCenterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof HelpCenterViewHolder) {
            final HelpCenterViewHolder holder1 = (HelpCenterViewHolder) holder;
            holder1.tvQuestion.setText(data.get(position).helpTitle);
            holder1.tvAnswer.setText(data.get(position).helpDescription);

            holder1.llQuestion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    notifyDataSetChanged()

                    if (holder1.tvAnswer.isShown()) {
                        CommonMethods.collapse(holder1.tvAnswer);
                        holder1.ivArrow.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_expand));
                        holder1.llQuestion.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_faq_inactive));

                    } else {
                        CommonMethods.expand(holder1.tvAnswer);
                        holder1.ivArrow.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_collapse));
                        holder1.llQuestion.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_faq_active));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class HelpCenterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rl_question)
        RelativeLayout llQuestion;
        @BindView(R.id.iv_arrow)
        ImageView ivArrow;
        @BindView(R.id.tv_answer)
        TextView tvAnswer;
        @BindView(R.id.tv_question)
        TextView tvQuestion;

        public HelpCenterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}