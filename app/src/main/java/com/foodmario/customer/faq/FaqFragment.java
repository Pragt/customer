package com.foodmario.customer.faq;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.foodmario.customer.R;
import com.foodmario.customer.MainActivity;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.faq.model.Faqs;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/31/18.
 */

public class FaqFragment extends Fragment{

    SharedPreference sharedPreference;

    @BindView(R.id.rv_faq)
    RecyclerView rvFaq;
    private FaqAdapter mAdadpter;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    Alerts alerts;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        ButterKnife.bind(this, view);
        init();
        getFaqs();
        return view;
    }

    private void getFaqs() {
        progressBar.setVisibility(View.VISIBLE);
        ApiClient.getClient().create(ApiInterface.class).getFaqs(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            progressBar.setVisibility(View.GONE);
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                setAdapter(s.data);
                            } else {
                                alerts.showErrorAlert(s.message);
                            }
                        },
                        e -> {
                            alerts.showErrorAlert(CommonMethods.getErrorTye(e));
                            progressBar.setVisibility(View.GONE);
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    private void setAdapter(List<Faqs> data) {
        mAdadpter = new FaqAdapter(getActivity(), data);
        rvFaq.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvFaq.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(getActivity(), 10), false));
        rvFaq.setAdapter(mAdadpter);
    }

    private void init() {
        sharedPreference = new SharedPreference(getActivity());
        alerts = new Alerts(getActivity());
    }

    @OnClick(R.id.ic_menu)
    void openMenu() {
        MainActivity.drawerLayout.openDrawer(GravityCompat.START);
    }
}
