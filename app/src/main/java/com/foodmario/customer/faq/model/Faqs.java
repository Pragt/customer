package com.foodmario.customer.faq.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Faqs {

@SerializedName("help_title")
@Expose
public String helpTitle;
@SerializedName("help_description")
@Expose
public String helpDescription;

}