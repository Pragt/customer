package com.foodmario.customer.faq.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FaqResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("data")
@Expose
public List<Faqs> data = null;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}