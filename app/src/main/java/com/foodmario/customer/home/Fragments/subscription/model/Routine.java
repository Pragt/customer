package com.foodmario.customer.home.Fragments.subscription.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Routine implements Serializable {

@SerializedName("week_id")
@Expose
public Integer weekId;
@SerializedName("line_total")
@Expose
public Integer lineTotal;
@SerializedName("qty")
@Expose
public Integer qty;
@SerializedName("foods")
@Expose
public List<Food> foods = null;

    public boolean isShown = true;
}