package com.foodmario.customer.home.Fragments.orderDetail.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.checkOut.model.Food;
import com.foodmario.customer.checkOut.model.OrderDetail;
import com.foodmario.customer.checkOut.view.MyCheckoutActivity;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.customViews.SwipeUtil.SwipeUtil;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.orderDetail.adapter.OrderDetailAdapter;
import com.foodmario.customer.home.Fragments.orderDetail.presenter.OrderDetailPresenter;
import com.foodmario.customer.home.view.MainFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class MyCartFragment extends Fragment implements IOrderDetailView {

    public static
    RecyclerView rvOrderDetail;

    @BindView(R.id.tv_sub_total)
    TextView tvSubTotal;

    @BindView(R.id.tv_promo_discount)
    TextView tvPromoDiscount;

    @BindView(R.id.tv_delivery_charge)
    TextView tvDeliveryCharge;

    @BindView(R.id.tv_grand_total)
    TextView tvGrandTotal;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.ll_empty)
    LinearLayout llEmptyCart;

    @BindView(R.id.rlPromoDiscount)
    RelativeLayout rlPromoDisocunt;

    @BindView(R.id.rl_cart)
    RelativeLayout rlCart;

    OrderDetail orderDetail;
    private OrderDetailAdapter mAdapter;
    SharedPreference sharedPreference;
    OrderDetailPresenter presenter;
    Alerts alerts;
    CustomProgressDialog pd;
    List<Food> foodList;
    private boolean isFromUnavailable = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_order_detail, null);
        ButterKnife.bind(this, view);
        rvOrderDetail = (RecyclerView) view.findViewById(R.id.rv_order_detail);
        sharedPreference = new SharedPreference(getActivity());
        presenter = new OrderDetailPresenter(sharedPreference, this);
        return view;

    }

    @Override
    public void init() {
        alerts = new Alerts(getActivity());
        pd = new CustomProgressDialog(getActivity());
        rvOrderDetail.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvOrderDetail.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(getActivity(), 5), false));

//        swipeRefreshLayout.setEnabled(false);
//        setSwipeForRecyclerView();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getMyOrders();
            }
        });

        rvOrderDetail.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // or get a reference to the ViewPager and cast it to ViewParent

                MainFragment.viewPager.requestDisallowInterceptTouchEvent(true);

                // let this view deal with the event or
                return false;
            }
        });
    }

    private void loadData() {
        tvSubTotal.setText("Rs." + orderDetail.finalPrice);
        tvPromoDiscount.setText("Rs. " + orderDetail.promotionDiscount);
        tvDeliveryCharge.setText("Rs. " + orderDetail.deliveryCharge);
        if (orderDetail.promotionDiscount == 0) {
            rlPromoDisocunt.setVisibility(View.GONE);
        } else
            rlPromoDisocunt.setVisibility(View.VISIBLE);
        tvGrandTotal.setText("Rs. " + (int) (orderDetail.totalPrice));

        MainFragment.setBadgeCount(orderDetail.foods.size());

        if (orderDetail.foods.size() > 0) {
            rlCart.setVisibility(View.VISIBLE);
            llEmptyCart.setVisibility(View.GONE);
        } else {
            rlCart.setVisibility(View.GONE);
            llEmptyCart.setVisibility(View.VISIBLE);
            sharedPreference.setKeyValues(CommonDef.SharedPrefrences.CART_ID, 0);
        }
    }

    @Override
    public void onShowProgressDialog(String msg) {
        pd.showpd(msg);
    }

    @Override
    public void onHideProgressDialog() {
        pd.hidepd();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onSuccess(String msg) {
        alerts.showSuccessAlert(msg);

    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }


//    @OnClick(R.id.btn_update)
//    void onUpdateClicked() {
//        presenter.updateCart(getCartObject());
//    }

    public JsonObject getCartObject() {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        jsonObject.addProperty("cart_id", sharedPreference.getIntValues(CommonDef.SharedPrefrences.CART_ID));
        for (int i = 0; i < mAdapter.foods.size(); i++) {
            JsonObject jsonObject1 = new JsonObject();
            jsonObject1.addProperty("food_id", mAdapter.foods.get(i).foodId);
            jsonObject1.addProperty("quantity", mAdapter.foods.get(i).qty);
            jsonArray.add(jsonObject1);
        }
        jsonObject.add("foods", jsonArray);
        return jsonObject;
    }

    private void setSwipeForRecyclerView() {

        SwipeUtil swipeHelper = new SwipeUtil(0, ItemTouchHelper.LEFT, getActivity()) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                mAdapter.pendingRemoval(swipedPosition);
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
//                if (mAdapter.isPendingRemoval(position)) {
//                    return 0;
//                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(swipeHelper);
        mItemTouchHelper.attachToRecyclerView(rvOrderDetail);

        //set swipe label
//        swipeHelper.setLeftSwipeLable("Archive");
        //set swipeF background-Color
        swipeHelper.setLeftcolorCode(ContextCompat.getColor(getActivity(), R.color.red));

    }

    @Override
    public void deleteFood(int position) {
        presenter.deleteFood(position, foodList.get(position).foodId);
//        alerts.showDeleteConfirmationDialog("Are you sure to remove this food from cart?", new Alerts.OnConfirmationClickListener() {
//            @Override
//            public void onYesClicked() {
//
//            }
//
//            @Override
//            public void onNoClicked() {
//                mAdapter.notifyItemChanged(position);
//            }
//        }, 100);
    }

    @Override
    public void onDeleteSuccess(String message, Integer position, OrderDetail results) {
        foodList.remove(position);
        mAdapter.deleteFood(position);
        orderDetail = results;
        foodList = orderDetail.foods;
        mAdapter.notifyDataSetChanged();
        loadData();
        if (foodList.size() == 0) {
            sharedPreference.setKeyValues(CommonDef.SharedPrefrences.CART_ID, 0);
        }
    }

    @Override
    public void onDeleteError(String message) {
        mAdapter.notifyDataSetChanged();
        alerts.showErrorAlert(message);
    }

    @Override
    public void onUpdateSuccess(String message, OrderDetail results) {
//        alerts.showSuccessAlerWithoutFinish(message);
        orderDetail = results;
        loadData();
        foodList = results.foods;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onGetCart(OrderDetail results) {
        orderDetail = results;
        foodList = orderDetail.foods;
        mAdapter = new OrderDetailAdapter(getActivity(), foodList);
        mAdapter.setListner(this);
        rvOrderDetail.setAdapter(mAdapter);
        loadData();
    }

    @Override
    public void doUpdate(int position, Integer count) {
        presenter.updateCart(getCartObject(), count, position);
    }

    @Override
    public void onUpdateError(String message, Integer oldCount, int position) {
        alerts.showErrorAlert(message);
        foodList.get(position).qty = oldCount;
        mAdapter = new OrderDetailAdapter(getActivity(), foodList);
        rvOrderDetail.setAdapter(mAdapter);
        mAdapter.setListner(this);
    }


    @OnClick(R.id.btn_proceed)
    void onProceedClicked() {
        Intent intent = new Intent(getActivity(), MyCheckoutActivity.class);
        intent.putExtra(CommonDef.ORDER_DETAIL, orderDetail);
        startActivityForResult(intent, 11);
//        onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sharedPreference.getIntValues(CommonDef.SharedPrefrences.CART_ID) != 0)
            if (!isFromUnavailable)
                presenter.getMyOrders();

        isFromUnavailable = false;
    }

    public void clearCart() {
        llEmptyCart.setVisibility(View.VISIBLE);
        rlCart.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            isFromUnavailable = true;
            if (requestCode == 11) {
                orderDetail = (OrderDetail) data.getSerializableExtra("data");
                foodList = orderDetail.foods;
                alerts.showSuccessAlert(data.getStringExtra("message"));
                mAdapter = new OrderDetailAdapter(getActivity(), foodList);
                rvOrderDetail.setAdapter(mAdapter);
                mAdapter.setListner(this);
            }
        }
    }
}

