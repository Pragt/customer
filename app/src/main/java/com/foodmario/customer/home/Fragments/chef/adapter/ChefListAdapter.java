package com.foodmario.customer.home.Fragments.chef.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.chef.model.Chef;
import com.foodmario.customer.home.Fragments.chef.view.IChefView;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class ChefListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int TYPE_BE_A_COOK = 1;
    private static final int TYPE_CHEF_LIST = 2;
    private Context mContext;
    public List<Chef> list;

    IChefView listner;

    public ChefListAdapter(Context mContext, List<Chef> results) {
        this.mContext = mContext;
        this.list = results;

    }

    public void setListner(IChefView listner)
    {
        this.listner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_CHEF_LIST) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_chef, parent, false);
            return new myCategoriesViewHolder(view);
        }else
        {
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_be_a_cook, parent, false);
            return new myBeACookViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            holder1.tvVendorName.setText(list.get(position).name);
            holder1.tvFoodCount.setText(list.get(position).foodCount + " food");
            holder1.tvDistance.setText(list.get(position).distance + " KM away");

            LoadImage.loadCircularUserImage((Activity) mContext, holder1.ivProfileImage, UrlHelpers.USER_BASE_URL + list.get(position).userImage, R.drawable.ic_man_1, R.drawable.ic_man_1);
            holder1.itemView.setOnClickListener(v -> listner.onVendorClicked(position));
        }else
        {
            holder.itemView.setOnClickListener(v -> listner.onBeACookClicked());
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addAll(List<Chef> results) {
        this.list = results;
        notifyDataSetChanged();
    }

    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_profile_img)
        ImageView ivProfileImage;

        @BindView(R.id.tv_food_count)
        TextView tvFoodCount;

        @BindView(R.id.tv_distance)
        TextView tvDistance;

        @BindView(R.id.tv_vendor_name)
        TextView tvVendorName;

        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


    public class myBeACookViewHolder extends RecyclerView.ViewHolder {


        public myBeACookViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == list.size())
            return TYPE_BE_A_COOK;
        else
            return TYPE_CHEF_LIST;
    }
}
