package com.foodmario.customer.home.Fragments.home.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.addToCartDialog.MyBottomSheetDialogFragment;
import com.foodmario.customer.comment.view.CommentActivity;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.customViews.LinearLayoutManagerWithSmoothScroller;
import com.foodmario.customer.customViews.NestedScroll;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.home.adapter.CategoriesAdapter;
import com.foodmario.customer.home.Fragments.home.adapter.FoodAdapter;
import com.foodmario.customer.home.Fragments.home.model.Category;
import com.foodmario.customer.home.Fragments.home.model.NearestFood;
import com.foodmario.customer.home.Fragments.home.model.Results;
import com.foodmario.customer.home.Fragments.home.presenter.HomeFragmentPresenter;
import com.foodmario.customer.vendorProfile.model.Food;
import com.foodmario.customer.vendorProfile.model.Vendor;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

/**
 * Created by prajit on 1/13/18.
 */

public class HomeFragment extends Fragment implements IHomeFragmentView, FoodAdapter.FoodListner, CategoriesAdapter.onCategoryClickListner {

    @BindView(R.id.rv_food)
    RecyclerView rvFood;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    private FoodAdapter mAdapter;
    Alerts alerts;
    SharedPreference sharedPreference;
    CustomProgressDialog pd;

    String delivery_lat;
    String delivery_lng;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.divider)
    View divider;


    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    public HomeFragmentPresenter presenter;
    String value = "";
    public int type = 0;
    private List<NearestFood> foodList;
    private List<Category> categoryList;
    int offset = 0;
    public boolean isLastPage = false;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    private CategoriesAdapter mCategoryAdapter;
    private LinearLayoutManagerWithSmoothScroller layoutManager;
    public boolean isLoading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_menu, container, false);
        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());
        presenter = new HomeFragmentPresenter(sharedPreference, this);
        delivery_lat = sharedPreference.getStringValues(CommonDef.DELIVERY_LAT);
        delivery_lng = sharedPreference.getStringValues(CommonDef.DELIVERY_LON);
        presenter.getFoodListing(delivery_lat, delivery_lng, value, type, "", 0);
        return view;
    }

    @Override
    public void onProfileImageClicked(int pos) {
        Opener.openVendorProfile(getActivity(), foodList.get(pos).vendorDetails.id);
    }

    @Override
    public void onLikeClicked(int newPos) {
        boolean oldLiked = foodList.get(newPos).isLiked;
        int oldLikeCount = foodList.get(newPos).likeCount;

        if (foodList.get(newPos).isLiked) {
            foodList.get(newPos).isLiked = false;
            foodList.get(newPos).likeCount--;
        } else {
            foodList.get(newPos).isLiked = true;
            foodList.get(newPos).likeCount++;
        }
        mAdapter.setList(foodList);
        mAdapter.onLikeClicked(newPos);
        presenter.setLiked(foodList.get(newPos).id, newPos, oldLiked, oldLikeCount);
    }

    @Override
    public void onCommentClicked(int newPos) {
//        Opener.openCommentActivity(getActivity(), foodList.get(newPos).id, newPos);
        Intent intent = new Intent(getActivity(), CommentActivity.class);
        intent.putExtra(CommonDef.FOOD_ID, foodList.get(newPos).id);
        intent.putExtra("pos", newPos);
        startActivityForResult(intent, CommonDef.RESPONSE_COMMENT);
    }

    @Override
    public void onShareClicked(int newPos) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Foodmario");
        share.putExtra(Intent.EXTRA_TEXT, foodList.get(newPos).shareLink);

        startActivity(Intent.createChooser(share, "Foodmario"));
    }

    @Override
    public void onAddToCartClicked(int position) {
        String image = "";
        if (foodList.get(position).dishImages != null && foodList.get(position).dishImages.size() > 0)
            image = foodList.get(position).dishImages.get(0).imagePath;

        MyBottomSheetDialogFragment.newInstance(foodList.get(position).id, foodList.get(position).vendorDetails.name, image, foodList.get(position).foodName, foodList.get(position).price, foodList.get(position).finalPrice, foodList.get(position).maxUnitPerDay).show(getFragmentManager(), "");
    }

    @Override
    public void onFoodImageClicked(NearestFood food) {
        Vendor vendor = new Vendor();
        vendor.name = food.vendorDetails.name;
        vendor.userAddress = food.vendorDetails.userAddress;
        vendor.userImage = food.vendorDetails.userImage;
        vendor.id = food.vendorDetails.id;

        Food food1 = new Food();
        food1.commentCount = food.commentCount;
        food1.id = food.id;
        food1.createdAt = food.createdAt;
        food1.description = food.description;
        food1.discount = food.discount;
        food1.discountAmount = food.discountAmount;
        food1.dishImages = food.dishImages;
        food1.foodName = food.foodName;
        food1.finalPrice = food.finalPrice;
        food1.isFeatured = food.isFeatured;
        food1.discountStatus = food.discountStatus;
        food1.price = food.price;
        food1.isLiked = food.isLiked;
        food1.likeCount = food.likeCount;
        food1.maxPreTime = food.estimatedDeliveryTime.toString();
        food1.maxUnitPerDay = food.maxUnitPerDay;
        food1.userId = food.userId;
        Opener.openFoodDetail(getActivity(), food1, vendor);
    }

    @Override
    public void init() {
        alerts = new Alerts(getActivity());
        pd = new CustomProgressDialog(getActivity());
        layoutManager = new LinearLayoutManagerWithSmoothScroller(getActivity());
        rvFood.setLayoutManager(layoutManager);
        rvFood.setNestedScrollingEnabled(false);
//        rvFood.setHasFixedSize(true);
        foodList = new ArrayList<>();
        categoryList = new ArrayList<>();
        mAdapter = new FoodAdapter(getActivity(), foodList);
        mAdapter.setListner(this);
        ((DefaultItemAnimator) rvFood.getItemAnimator()).setSupportsChangeAnimations(false);
        rvFood.setItemAnimator(null);

        rvFood.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(getActivity(), 8), false));
        rvFood.setAdapter(mAdapter);

        showProgressBar();
        rvCategories.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvCategories.setAdapter(new CategoriesAdapter(getActivity(), categoryList));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getFoodListing(delivery_lat, delivery_lng, value, type, etSearch.getText().toString(), 0);
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    if (MainFragment.pos == 0) {
                    progressBar.setVisibility(View.VISIBLE);
                    presenter.getFoodListing(delivery_lat, delivery_lng, value, type, etSearch.getText().toString(), 0);
                    CommonMethods.hideSoftKeyboard(getActivity());
                }
//                }
                return false;
            }
        });

//        rvFood.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
//            @Override
//            public void onLoadMore(int current_page) {
//                if (!isLastPage && !presenter.isLoading) {
//                    mAdapter.addLoader();
////                    nestedScrollView.smoothScrollTo(0, 0);
//                    nestedScrollView.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            nestedScrollView.fullScroll(View.FOCUS_DOWN);
//                        }
//                    });
//                    presenter.getFoodListing(delivery_lat, delivery_lng, value, type, etSearch.getText().toString(), foodList.size());
//                }
//
//            }
//        });


        nestedScrollView.setOnScrollChangeListener(new NestedScroll() {
            @Override
            public void onScroll() {
                if (!isLastPage && !presenter.isLoading) {
                    mAdapter.addLoader();
//                    nestedScrollView.smoothScrollTo(0, 0);
                    nestedScrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            nestedScrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                    presenter.getFoodListing(delivery_lat, delivery_lng, value, type, etSearch.getText().toString(), foodList.size() - 1);
                }

            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        searchFood(delivery_lat, delivery_lng, value, type, etSearch.getText().toString());

                    }
                }, 500);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onShowProgressDialog(String msg) {
        pd.showpd(msg);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onHideProgressDialog() {
        pd.hidepd();
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
//        if (!MainFragment.etSearch.getText().toString().isEmpty())
//            MainFragment.etSearch.setText("");

    }

    @Override
    public void onSuccess(String msg) {
        alerts.showWarningAlert(msg);
    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
        if (foodList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
            if (etSearch.getText().toString().isEmpty())
                tvEmpty.setText("No food available near your location.");
            else
                tvEmpty.setText("No food found for this search keyword.");
        }
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGetFood(Results results, boolean isLastPage) {
        this.foodList = results.nearestFood;
        this.isLastPage = isLastPage;
        mAdapter = new FoodAdapter(getActivity(), foodList);
        mAdapter.setListner(this);
        mAdapter.setList(this.foodList);
        rvFood.setAdapter(mAdapter);
        divider.setVisibility(View.VISIBLE);
        if (mCategoryAdapter == null) {
            this.categoryList = results.categories;
            rvCategories.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            Category category = new Category();
            category.name = "All";
            category.type = 0;
            category.id = 0;
            category.isSelected = true;
            categoryList.add(0, category);
            mCategoryAdapter = new CategoriesAdapter(getActivity(), categoryList);
            mCategoryAdapter.setListner(this);
            rvCategories.setAdapter(mCategoryAdapter);
        }

        if (foodList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
            if (etSearch.getText().toString().isEmpty())
                tvEmpty.setText("No food available near your location.");
            else
                tvEmpty.setText("No food found for this search keyword.");
        } else {
            tvEmpty.setVisibility(View.GONE);
        }


    }

    @Override
    public void onFoodReactError(String message, boolean isLiked, int oldLikeCount, int newPos) {
        foodList.get(newPos).isLiked = isLiked;
        foodList.get(newPos).likeCount = oldLikeCount;
////        mAdapter.addAll(foodList);
        mAdapter.setList(foodList);
        alerts.showToastMsg(message);
    }

    @Override
    public void onFoodReactSuccess(Boolean isLike, int likeCount, int newPos) {
        foodList.get(newPos).isLiked = isLike;
        foodList.get(newPos).likeCount = likeCount;
        mAdapter.setList(foodList);
//        mAdapter.addAll(foodList);
    }

    @Override
    public void addFood(Results results, boolean isLastPage) {
        this.isLastPage = isLastPage;
        this.foodList.remove(foodList.size() - 1);
        this.foodList.addAll(results.nearestFood);
//        mAdapter.appendAll(results.nearestFood);
//        this.foodList.addAll(foodList);
        for (int i = 0; i < foodList.size(); i++) {
            mAdapter.notifyItemInserted(this.foodList.size() - results.nearestFood.size() + i);
        }
    }

    @Override
    public void hideLoadMoreProgress() {
        mAdapter.hideLoader();
    }

    @Override
    public void doLogOut() {
        sharedPreference.clearData();
        Opener.LoginActivity(getActivity());
        getActivity().finishAffinity();
    }

    @Override
    public void onResume() {
        super.onResume();
//        presenter.getFoodListing(delivery_lat, delivery_lng, value, type, etSearch.getText().toString());
    }

    @Override
    public void onCategoryItemClicked(int pos) {
        if (CommonMethods.isConnectingToInternet(getActivity())) {
            type = categoryList.get(pos).type;
            for (int i = 0; i < categoryList.size(); i++) {
                categoryList.get(i).isSelected = false;
            }
            categoryList.get(pos).isSelected = true;
            mCategoryAdapter.addAll(categoryList);

            if (categoryList.get(pos).type == 1) {
                value = String.valueOf(categoryList.get(pos).id);
            } else {
                value = String.valueOf(categoryList.get(pos).name);
            }
            showProgressBar();
            presenter.getFoodListing(delivery_lat, delivery_lng, value, type, etSearch.getText().toString(), 0);
        } else
            alerts.showErrorAlert(CommonDef.No_Connection);
    }

    @Override
    public void onLoadAll() {
        progressBar.setVisibility(View.VISIBLE);
        scrollToTop();
        presenter.getFoodListing(delivery_lat, delivery_lng, value, type, etSearch.getText().toString(), 0);
    }

    public void searchFood(String lat, String lng, String value, int type, String substring) {
        presenter.getFoodListing(lat, lng, value, type, substring, 0);
    }

    public void scrollToTop() {
        nestedScrollView.fullScroll(View.FOCUS_UP);
        etSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CommonDef.RESPONSE_COMMENT) {
                int pos = data.getIntExtra("pos", 0);
                int commentCount = data.getIntExtra("count", 0);
                foodList.get(pos).commentCount = commentCount;
                mAdapter.setList(foodList);
                mAdapter.onLikeClicked(pos);
            }
        }
    }
}
