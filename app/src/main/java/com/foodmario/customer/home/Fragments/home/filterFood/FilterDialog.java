package com.foodmario.customer.home.Fragments.home.filterFood;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by prajit on 6/12/18.
 */

public class FilterDialog extends DialogFragment implements CompoundButton.OnCheckedChangeListener {


    private static FilterFoodListner listner;
    @BindView(R.id.ivCross)
    ImageView ivCross;

    @BindView(R.id.cb_price_high_to_low)
    CheckBox cbPriceHighToLow;

    @BindView(R.id.cb_price_low_to_high)
    CheckBox cbPriceLowToHigh;

    @BindView(R.id.cb_popularity_high_to_low)
    CheckBox cbPopularityHighToLow;

    @BindView(R.id.cb_popilarity_low_to_high)
    CheckBox cbPopularityLowToHigh;

    @BindView(R.id.cb_delivery_high_to_low)
    CheckBox cbDeliveryHighToLow;

    @BindView(R.id.cb_delivery_low_to_high)
    CheckBox cbDeliveryLowToHigh;

    SharedPreference sharedPreference;
    int filterId;


    public static FilterDialog getInstance(FilterFoodListner list) {
        FilterDialog dialog = new FilterDialog();
        listner = list;
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_filter, null);
        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());

        cbDeliveryHighToLow.setOnCheckedChangeListener(this);
        cbDeliveryLowToHigh.setOnCheckedChangeListener(this);
        cbPriceLowToHigh.setOnCheckedChangeListener(this);
        cbPriceHighToLow.setOnCheckedChangeListener(this);
        cbPopularityLowToHigh.setOnCheckedChangeListener(this);
        cbPopularityHighToLow.setOnCheckedChangeListener(this);

        filterId = sharedPreference.getIntValues(CommonDef.SharedPrefrences.FILTERED_ID);

        switch (sharedPreference.getIntValues(CommonDef.SharedPrefrences.FILTERED_ID)) {
            case 1:
                cbPriceHighToLow.setChecked(true);
                break;
            case 2:
                cbPriceLowToHigh.setChecked(true);
                break;
            case 3:
                cbPopularityHighToLow.setChecked(true);
                break;
            case 4:
                cbPopularityLowToHigh.setChecked(true);
                break;
            case 5:
                cbDeliveryHighToLow.setChecked(true);
                break;
            case 6:
                cbDeliveryLowToHigh.setChecked(true);
                break;


        }


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            cbPriceHighToLow.setChecked(false);
            cbPriceHighToLow.setClickable(true);
            cbPriceLowToHigh.setChecked(false);
            cbPriceLowToHigh.setClickable(true);
            cbDeliveryHighToLow.setChecked(false);
            cbDeliveryHighToLow.setClickable(true);
            cbDeliveryLowToHigh.setChecked(false);
            cbDeliveryLowToHigh.setClickable(true);
            cbPopularityHighToLow.setChecked(false);
            cbPopularityHighToLow.setClickable(true);
            cbPopularityLowToHigh.setChecked(false);
            cbPopularityLowToHigh.setClickable(true);

            compoundButton.setChecked(true);
            compoundButton.setClickable(false);
            switch (compoundButton.getId()) {
                case R.id.cb_price_high_to_low:
                    filterId = 1;
                    break;
                case R.id.cb_price_low_to_high:
                    filterId = 2;
                    break;
                case R.id.cb_popularity_high_to_low:
                    filterId = 3;
                    break;
                case R.id.cb_popilarity_low_to_high:
                    filterId = 4;
                    break;
                case R.id.cb_delivery_high_to_low:
                    filterId = 5;
                    break;
                case R.id.cb_delivery_low_to_high:
                    filterId = 6;
                    break;
            }
        }
    }

    @OnClick(R.id.tv_reset)
    void onResetClicked() {
        filterId = 0;
        cbPriceHighToLow.setChecked(false);
        cbPriceHighToLow.setClickable(true);
        cbPriceLowToHigh.setChecked(false);
        cbPriceLowToHigh.setClickable(true);
        cbDeliveryHighToLow.setChecked(false);
        cbDeliveryHighToLow.setClickable(true);
        cbDeliveryLowToHigh.setChecked(false);
        cbDeliveryLowToHigh.setClickable(true);
        cbPopularityHighToLow.setChecked(false);
        cbPopularityHighToLow.setClickable(true);
        cbPopularityLowToHigh.setChecked(false);
        cbPopularityLowToHigh.setClickable(true);

        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FILTERED_ID, filterId);
        dismiss();
        listner.onFoodFilter(0);
    }

    public interface FilterFoodListner {
        void onFoodFilter(int id);
    }

    @OnClick(R.id.ivCross)
    void onCrossClicked() {
        dismiss();
    }

    @OnClick(R.id.btn_apply)
    void onApplyClicked() {
        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FILTERED_ID, filterId);
        dismiss();
        listner.onFoodFilter(0);
    }

}
