package com.foodmario.customer.home.Fragments.home.presenter;

/**
 * Created by prajit on 1/27/18.
 */

public interface IHomePresenter {
    void getFoodListing(String lat, String lng, String value, int type, String substring, int offset);

    void setLiked(Integer food_id, int newPos, Boolean isLiked, int oldLikeCount);
}


