package com.foodmario.customer.home.Fragments.home.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.home.model.Category;
import com.foodmario.customer.home.Fragments.home.model.NearestFood;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by pragt on 3/5/17.
 */

public class FoodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_FOOD = 2;
    private static final int TYPE_PROGRESS = 1;
    private Context mContext;
    FoodListner listner;
    List<NearestFood> foodList;
    List<Category> categoryList;
    private int MAX_LINES = 4;
    private boolean isLikedClicked = false;

    public FoodAdapter(Context mContext, List<NearestFood> foodList) {
        this.mContext = mContext;
        this.foodList = foodList;
    }

    public void setListner(FoodListner listner) {
        this.listner = listner;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_FOOD) {
            view = LayoutInflater.from(mContext).inflate(R.layout.layout_food_item, null);
            return new myFoodViewHolder(view);
        } else
            view = LayoutInflater.from(mContext).inflate(R.layout.item_progress_dialog, parent, false);
        return new myProgressViewHolder(view);

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myFoodViewHolder) {
            int newPos = position;
            myFoodViewHolder holder1 = (myFoodViewHolder) holder;
            final ImageAdapter mAdapter = new ImageAdapter((Activity) mContext, foodList.get(newPos).dishImages);
            holder1.viewPager.setAdapter(mAdapter);

            holder1.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onProfileImageClicked(newPos);
                }
            });

            holder1.tvName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onProfileImageClicked(newPos);
                }
            });

            holder1.tvAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onProfileImageClicked(newPos);
                }
            });

            final int dotscount;
            final ImageView[] dots;

            dotscount = mAdapter.getCount();
            dots = new ImageView[dotscount];
            holder1.llDots.removeAllViews();

            for (int i = 0; i < dotscount; i++) {
                dots[i] = new ImageView(mContext);
                dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_unselected_dot));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                params.setMargins(8, 0, 8, 0);

                holder1.llDots.addView(dots[i], params);

            }

            if (dots.length > 0)
                dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_selected_dot));
            if (dots.length <= 1) {
                holder1.llDots.setVisibility(View.GONE);
            } else
                holder1.llDots.setVisibility(View.VISIBLE);


            holder1.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    for (int i = 0; i < dotscount; i++) {
                        dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_unselected_dot));
                    }

                    try {
                        dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_selected_dot));
                    } catch (Exception exx) {
                    }

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            holder1.viewPager.setOnTouchListener(new View.OnTouchListener() {
                float oldX = 0, newX = 0, sens = 5;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            oldX = event.getX();
                            break;

                        case MotionEvent.ACTION_UP:
                            newX = event.getX();
                            if (Math.abs(oldX - newX) < sens) {
                                listner.onFoodImageClicked(foodList.get(position));
                                return true;
                            }
                            oldX = 0;
                            newX = 0;
                            break;
                    }

                    return false;
                }
            });

//            holder1.tvDescription.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    holder1.tvDescription.setText(foodList.get(newPos).description);
//                    holder1.tvDescription.setMaxLines(100);
//                }
//            });
//                holder1.tvDescription.setMaxLines(100);

            LoadImage.loadCircularUserImage((Activity) mContext, holder1.ivImage, UrlHelpers.USER_BASE_URL + foodList.get(newPos).vendorDetails.userImage, R.drawable.ic_man_1, R.drawable.ic_man_1);
            holder1.tvName.setText(foodList.get(newPos).vendorDetails.name);
            holder1.tvAddress.setText(foodList.get(newPos).vendorDetails.userAddress);
            holder1.tvLikeCount.setText(foodList.get(newPos).likeCount + "");
            holder1.tvCommentCount.setText(foodList.get(newPos).commentCount + "");
            holder1.tvFoodName.setText(foodList.get(newPos).foodName);
            holder1.tvDistance.setText(CommonMethods.calculateMinutesAndHour(foodList.get(newPos).estimatedDeliveryTime) + ", " + foodList.get(newPos).distance + " KM away");
            holder1.tvFaKe.setText(foodList.get(newPos).description);

            holder1.tvFinalPrice.setText("Rs. " + foodList.get(newPos).finalPrice);
            holder1.tvPrice.setText("Rs. " + foodList.get(newPos).price);
            holder1.tvPrice.setPaintFlags(holder1.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            if (String.valueOf(foodList.get(newPos).finalPrice).equalsIgnoreCase(String.valueOf(foodList.get(newPos).price))) {
                holder1.tvPrice.setVisibility(View.GONE);
            } else
                holder1.tvPrice.setVisibility(View.VISIBLE);

            holder1.llLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onLikeClicked(newPos);
                }
            });

            holder1.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onCommentClicked(newPos);
                }
            });

            holder1.llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onShareClicked(newPos);
                }
            });

            holder1.ivCart.setOnClickListener(v -> listner.onAddToCartClicked(position));

            if (foodList.get(newPos).isLiked)
                holder1.ivLikes.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_heart_red));
            else
                holder1.ivLikes.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_heart));

            if (!isLikedClicked) {
                holder1.tvDescription.setText(foodList.get(newPos).description);
                ViewTreeObserver vto = holder1.tvDescription.getViewTreeObserver();
                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        ViewTreeObserver obs = holder1.tvDescription.getViewTreeObserver();
                        obs.removeGlobalOnLayoutListener(this);
                        if (holder1.tvDescription.getLineCount() > 4) {
                            Log.d("", "Line[" + holder1.tvDescription.getLineCount() + "]" + holder1.tvDescription.getText());
//                        int lineEndIndex = holder1.tvDescription.getLayout().getLineEnd(4);
//                        String text = holder1.tvDescription.getText().subSequence(0, lineEndIndex-3)+"...";
//                        holder1.tvDescription.setText(text);
//                        Log.d("","NewText:"+text);
                            holder1.tvDescription.setMaxLines(4);
                            holder1.btnShowMore.setVisibility(View.VISIBLE);
                        } else
                            holder1.btnShowMore.setVisibility(View.GONE);

                    }
                });
            }

            holder1.btnShowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder1.btnShowMore.getText().toString().equalsIgnoreCase("Show More...")) {
                        holder1.tvDescription.setMaxLines(100);//your TextView
                        holder1.btnShowMore.setText("Show Less");
                    } else {
                        holder1.tvDescription.setMaxLines(4);//your TextView
                        holder1.btnShowMore.setText("Show More...");
                    }
                }
            });

        }
    }


    @Override
    public int getItemCount() {
        return foodList.size();
    }


    public void addAll(List<NearestFood> foodList) {
        isLikedClicked = false;
        this.foodList = foodList;
        notifyDataSetChanged();
    }

    public void setList(List<NearestFood> foodList) {
        this.foodList = foodList;
    }

    public void onLikeClicked(int newPos) {
        isLikedClicked = true;
        notifyItemChanged(newPos);
    }

    public void appendAll(List<NearestFood> foodList) {
        this.foodList.addAll(foodList);
        for (int i = 0; i < foodList.size(); i++) {
            notifyItemInserted(this.foodList.size() - foodList.size() - i);
        }
    }

    public void addLoader() {
        this.foodList.add(null);
        notifyItemInserted(foodList.size());
    }

    @Override
    public int getItemViewType(int position) {
        if (foodList.get(position) == null)
            return TYPE_PROGRESS;
        else
            return TYPE_FOOD;
    }

    public void hideLoader() {
        if (foodList != null && foodList.size() > 4 && foodList.get(foodList.size() - 1) == null) {
            foodList.remove(foodList.size() - 1);
            notifyItemRemoved(foodList.size() - 1);
        }
    }

    public class myFoodViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_address)
        TextView tvAddress;

        @BindView(R.id.tv_final_price)
        TextView tvFinalPrice;

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.tv_distance)
        TextView tvDistance;

        @BindView(R.id.tv_like_count)
        TextView tvLikeCount;

        @BindView(R.id.tv_comment_count)
        TextView tvCommentCount;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        @BindView(R.id.tv_description)
        TextView tvDescription;

        @BindView(R.id.flViewPager)
        FrameLayout flViewPager;

        @BindView(R.id.tv_fake_description)
        TextView tvFaKe;


        @BindView(R.id.iv_profile_img)
        ImageView ivImage;

        @BindView(R.id.iv_cart)
        Button ivCart;


        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.viewPager)
        ViewPager viewPager;

        @BindView(R.id.slider_dots)
        LinearLayout llDots;

        @BindView(R.id.ll_like)
        LinearLayout llLike;

        @BindView(R.id.ll_comment)
        LinearLayout llComment;

        @BindView(R.id.ll_share)
        LinearLayout llShare;

        @BindView(R.id.btnShowmore)
        TextView btnShowMore;

        @BindView(R.id.iv_likes)
        ImageView ivLikes;

        public myFoodViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


    public interface FoodListner {
        void onProfileImageClicked(int pos);

        void onLikeClicked(int newPos);

        void onCommentClicked(int newPos);

        void onShareClicked(int newPos);

        void onAddToCartClicked(int position);

        void onFoodImageClicked(NearestFood food);
    }


    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(
                        addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                viewMore), TextView.BufferType.SPANNABLE);
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        makeTextViewResizable(tv, 4, "View More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    private class myProgressViewHolder extends RecyclerView.ViewHolder {
        public myProgressViewHolder(View view) {
            super(view);
        }
    }
}
