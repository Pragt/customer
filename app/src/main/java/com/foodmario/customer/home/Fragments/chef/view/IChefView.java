package com.foodmario.customer.home.Fragments.chef.view;

import com.foodmario.customer.commonInterface.IcommonView;
import com.foodmario.customer.home.Fragments.chef.model.Chef;

import java.util.List;

/**
 * Created by prajit on 1/28/18.
 */

public interface IChefView extends IcommonView {
    void onGetChefList(List<Chef> results);

    void onVendorClicked(int position);

    void onBeACookClicked();
}
