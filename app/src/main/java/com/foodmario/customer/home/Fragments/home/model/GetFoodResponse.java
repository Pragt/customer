package com.foodmario.customer.home.Fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetFoodResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public Results results;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}