package com.foodmario.customer.home.Fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeLikeResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("results")
    @Expose
    public Results results;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;

    public class Results {

        @SerializedName("is_like")
        @Expose
        public Boolean isLike;

        @SerializedName("like_count")
        @Expose
        public int likeCount;

    }

}