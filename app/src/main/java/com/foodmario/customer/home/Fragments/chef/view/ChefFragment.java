package com.foodmario.customer.home.Fragments.chef.view;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.chef.adapter.ChefListAdapter;
import com.foodmario.customer.home.Fragments.chef.model.Chef;
import com.foodmario.customer.home.Fragments.chef.presenter.ChefPresenter;
import com.foodmario.customer.home.view.MainFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by prajit on 1/28/18.
 */

public class ChefFragment extends Fragment implements IChefView {


    String delivery_lat;
    String delivery_lng;
    private SharedPreference sharedPreference;
    @BindView(R.id.rv_chef)
    RecyclerView rvChef;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.et_search)
    EditText etSearch;

    public ChefPresenter presenter;
    List<Chef> chefList;
    private ChefListAdapter mAdadpter;

    String substring = "";

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_chef, container, false);
        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());
        delivery_lat = sharedPreference.getStringValues(CommonDef.DELIVERY_LAT);
        delivery_lng = sharedPreference.getStringValues(CommonDef.DELIVERY_LON);
        presenter = new ChefPresenter(sharedPreference, this);
//        presenter.getChefList(latitude, longitude, substring);
        presenter.getChefList(delivery_lat, delivery_lng, substring);

        return view;
    }

    @Override
    public void init() {
        rvChef.setLayoutManager(new LinearLayoutManager(getActivity()));
        chefList = new ArrayList<>();
        mAdadpter = new ChefListAdapter(getActivity(), chefList);
        mAdadpter.setListner(this);
        rvChef.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(getActivity(), 5), false));
        rvChef.setAdapter(mAdadpter);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getChefList(delivery_lat, delivery_lng, substring);
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (MainFragment.pos == 1) {
                        progressBar.setVisibility(View.VISIBLE);

                        presenter.getChefList(delivery_lat, delivery_lng, etSearch.getText().toString());
                    }

                    CommonMethods.hideSoftKeyboard(getActivity());
                }
                return false;
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        presenter.getChefList(delivery_lat, delivery_lng, etSearch.getText().toString());

                    }

                }, 500);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onShowProgressDialog(String msg) {

    }

    @Override
    public void onHideProgressDialog() {
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);

        if (chefList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
        }
//        MainFragment.etSearch.setText("");
    }

    @Override
    public void onSuccess(String msg) {

    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void onValidationError(String msg) {

    }

    @Override
    public void onGetChefList(List<Chef> results) {
        chefList = results;
        mAdadpter.addAll(chefList);

        if (chefList.size() > 0)
            tvEmpty.setVisibility(View.GONE);
    }

    @Override
    public void onVendorClicked(int position) {
        Opener.openVendorProfile(getActivity(), chefList.get(position).id);
    }

    @Override
    public void onBeACookClicked() {
        CommonMethods.gotoVendorApp(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @OnClick(R.id.llBeACook)
    void beACookClicked(){
        onBeACookClicked();
    }
}
