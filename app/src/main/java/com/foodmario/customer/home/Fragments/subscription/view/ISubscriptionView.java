package com.foodmario.customer.home.Fragments.subscription.view;

import com.foodmario.customer.commonInterface.IcommonView;
import com.foodmario.customer.home.Fragments.subscription.model.Routine;
import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionPackage;

import java.util.List;

/**
 * Created by prajit on 2/25/18.
 */

public interface ISubscriptionView extends IcommonView {
    void onGetSubscription(List<SubscriptionPackage> results);
}
