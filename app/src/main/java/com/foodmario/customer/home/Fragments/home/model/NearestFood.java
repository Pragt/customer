package com.foodmario.customer.home.Fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NearestFood {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("user_id")
@Expose
public Integer userId;
@SerializedName("food_name")
@Expose
public String foodName;
@SerializedName("slug")
@Expose
public String slug;
@SerializedName("price")
@Expose
public Integer price;
@SerializedName("description")
@Expose
public String description;
@SerializedName("max_unit_per_day")
@Expose
public Integer maxUnitPerDay;
@SerializedName("max_pre_time")
@Expose
public String maxPreTime;
@SerializedName("status")
@Expose
public String status;
@SerializedName("discount_status")
@Expose
public String discountStatus;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;
@SerializedName("is_featured")
@Expose
public String isFeatured;
@SerializedName("is_liked")
@Expose
public Boolean isLiked;
@SerializedName("discount")
@Expose
public Discount discount;
@SerializedName("dish_images")
@Expose
public List<DishImage> dishImages = null;
@SerializedName("like_count")
@Expose
public Integer likeCount;
@SerializedName("comment_count")
@Expose
public Integer commentCount;
@SerializedName("discount_amount")
@Expose
public Float discountAmount;
@SerializedName("final_price")
@Expose
public Integer finalPrice;

@SerializedName("share_link")
@Expose
public String shareLink;
@SerializedName("estimated_delivery_time")
@Expose
public Integer estimatedDeliveryTime;
@SerializedName("distance")
@Expose
public Integer distance;
@SerializedName("vendor_details")
@Expose
public VendorDetails vendorDetails;

}