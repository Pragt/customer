package com.foodmario.customer.home.Fragments.chef.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Chef {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("user_image")
@Expose
public String userImage;
@SerializedName("name")
@Expose
public String name;
@SerializedName("email")
@Expose
public String email;
@SerializedName("user_lat")
@Expose
public String userLat;
@SerializedName("user_long")
@Expose
public String userLong;
@SerializedName("user_address")
@Expose
public String userAddress;
@SerializedName("address_2")
@Expose
public String address2;
@SerializedName("address_3")
@Expose
public String address3;
@SerializedName("user_phone")
@Expose
public String userPhone;
@SerializedName("user_category_id")
@Expose
public Integer userCategoryId;
@SerializedName("user_type")
@Expose
public String userType;
@SerializedName("sell_status")
@Expose
public String sellStatus;
@SerializedName("status")
@Expose
public String status;
@SerializedName("user_description")
@Expose
public Object userDescription;
@SerializedName("has_set_serving_hours")
@Expose
public String hasSetServingHours;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;
@SerializedName("role_id")
@Expose
public Integer roleId;
@SerializedName("is_active")
@Expose
public Integer isActive;
@SerializedName("is_approved")
@Expose
public Integer isApproved;
@SerializedName("food_count")
@Expose
public Integer foodCount;

@SerializedName("distance")
@Expose
public Integer distance;

}