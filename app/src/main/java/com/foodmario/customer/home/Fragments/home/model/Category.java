package com.foodmario.customer.home.Fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("name")
@Expose
public String name;
@SerializedName("type")
@Expose
public Integer type;
@SerializedName("image")
@Expose
public String image;

public boolean isSelected = false;

}