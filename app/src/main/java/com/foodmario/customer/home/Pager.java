package com.foodmario.customer.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.foodmario.customer.home.Fragments.subscription.view.SubscriptionFragment;
import com.foodmario.customer.home.Fragments.chef.view.ChefFragment;
import com.foodmario.customer.home.Fragments.home.view.HomeFragment;
import com.foodmario.customer.home.Fragments.orderDetail.view.MyCartFragment;

/**
 * Created by Belal on 2/3/2016.
 */
//Extending FragmentStatePagerAdapter
public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs 
        switch (position) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                ChefFragment chefFragment = new ChefFragment();
                return chefFragment;
//            case 2:
//                MySubscriptionFragment subscriptionFragment = new MySubscriptionFragment();
//                return subscriptionFragment;
            case 3:
                MyCartFragment cartFragment = new MyCartFragment();
                return cartFragment;

            default:
                SubscriptionFragment tab2 = new SubscriptionFragment();
                return tab2;
        }
    }

    //Overriden method getCount to get the number of tabs 
    @Override
    public int getCount() {
        return tabCount;
    }
}