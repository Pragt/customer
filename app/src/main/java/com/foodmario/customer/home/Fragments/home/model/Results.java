package com.foodmario.customer.home.Fragments.home.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Results {

    @SerializedName("categories")
    @Expose
    public List<Category> categories = null;
    @SerializedName("nearest_food")
    @Expose
    public List<NearestFood> nearestFood = null;
    @SerializedName("is_last_page")
    @Expose
    public boolean isLastPage = false;
}