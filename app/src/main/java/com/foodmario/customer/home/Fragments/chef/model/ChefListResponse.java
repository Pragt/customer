package com.foodmario.customer.home.Fragments.chef.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChefListResponse {

@SerializedName("status")
@Expose
public String status;
@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public List<Chef> results = null;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}