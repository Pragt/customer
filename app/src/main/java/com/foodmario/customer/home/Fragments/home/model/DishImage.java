package com.foodmario.customer.home.Fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DishImage implements Serializable{

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("image_path")
@Expose
public String imagePath;

}