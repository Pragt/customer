package com.foodmario.customer.home.Fragments.subscription.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Discount implements Serializable {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("subscription_id")
@Expose
public Integer subscriptionId;
@SerializedName("discount_type")
@Expose
public String discountType;
@SerializedName("value")
@Expose
public String value;
@SerializedName("min_food_total")
@Expose
public int minFoodTotal;
@SerializedName("min_order_amount")
@Expose
public int minOrderAmount;

}