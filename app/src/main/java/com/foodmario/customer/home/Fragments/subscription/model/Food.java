package com.foodmario.customer.home.Fragments.subscription.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Food implements Serializable {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("food_id")
@Expose
public Integer foodId;
@SerializedName("subscription_id")
@Expose
public Integer subscriptionId;
@SerializedName("week_id")
@Expose
public Integer weekId;
@SerializedName("qty")
@Expose
public Integer qty;
@SerializedName("line_total")
@Expose
public String lineTotal;
@SerializedName("delivery_time")
@Expose
public String deliveryTime;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;
@SerializedName("is_enabled")
@Expose
public Boolean isEnabled;
@SerializedName("details")
@Expose
public Details details;

}