package com.foodmario.customer.home.Fragments.subscription.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.checkOut.model.Food;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.chef.model.Chef;
import com.foodmario.customer.home.Fragments.orderDetail.view.IOrderDetailView;
import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionPackage;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class MySubscriptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    IOrderDetailView listner;
    public List<Food> foods;
    List<SubscriptionPackage> list;

    public MySubscriptionAdapter(Context mContext, List<SubscriptionPackage> results) {
        this.mContext = mContext;
        this.list = results;
//        this.foods = foods;
    }

    public void setListner(IOrderDetailView listner) {
        this.listner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_subscription_new, null);
        return new myCategoriesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    listner.onResumeClicked(position);
                        Opener.openSubscrtionDetail((Activity) mContext, list.get(position));
                }
            });

            holder1.tvName.setText(list.get(position).name);
            if (list.get(position).discountStatus.equalsIgnoreCase("1")) {
                holder1.tvPrice.setText("Rs. " + list.get(position).subTotal);
                int finalPrice = list.get(position).subTotal - list.get(position).totalDiscount;
                int discount = list.get(position).totalDiscount / list.get(position).subTotal * 100;
                holder1.tvFinalPrice.setText("Rs. " + finalPrice);
                if (list.get(position).discountType.equalsIgnoreCase("1"))
                    holder1.tvDiscount.setText("Rs. " + list.get(position).totalDiscount + " off");
                else
                    holder1.tvDiscount.setText(list.get(position).percentageDiscount.intValue() + " % off");
                holder1.tvPrice.setPaintFlags(holder1.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                holder1.tvFinalPrice.setText("Rs. " + list.get(position).subTotal);
                holder1.tvDiscount.setVisibility(View.GONE);
                holder1.tvPrice.setVisibility(View.GONE);
            }
            String subscriptionType = "";
            if (list.get(position).type.equalsIgnoreCase("monthly"))
                subscriptionType = "28 Days ";
            else if (list.get(position).type.equalsIgnoreCase("biweekly"))
                subscriptionType = "14 Days ";
            else if (list.get(position).type.equalsIgnoreCase("weekly"))
                subscriptionType = "7 Days ";
            else
                subscriptionType = list.get(position).type;
            holder1.tvSubscriptionType.setText(subscriptionType + " Subscription");

            LoadImage.loadBigImage((Activity) mContext, holder1.ivImage, holder1.progressBar, UrlHelpers.SUBSCRIPTION_BASE_URL + list.get(position).imagePath, R.drawable.placeholder_1440_480_4x, R.drawable.placeholder_1440_480_4x);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addAll(List<Chef> results) {
//        this.list = results;
//        notifyDataSetChanged();
    }

    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.iv_image)
        ImageView ivImage;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_final_price)
        TextView tvFinalPrice;

        @BindView(R.id.tv_discount)
        TextView tvDiscount;

        @BindView(R.id.tv_subscription_type)
        TextView tvSubscriptionType;

        @BindView(R.id.progress_bar)
        ProgressBar progressBar;

        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
