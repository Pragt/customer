package com.foodmario.customer.home.Fragments.subscription.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.subscription.adapter.MySubscriptionAdapter;
import com.foodmario.customer.home.Fragments.subscription.model.Routine;
import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionPackage;
import com.foodmario.customer.home.Fragments.subscription.presenter.SubscriptionPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Belal on 2/3/2016.
 */

//Our class extending fragment
public class SubscriptionFragment extends Fragment implements ISubscriptionView {

    @BindView(R.id.rv_subscription)
    RecyclerView rvSubscriptions;

    @BindView(R.id.ll_no_subscription)
    LinearLayout llNoSubscription;

    SubscriptionPresenter presenter;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    private List<SubscriptionPackage> results;


    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating 
        //Change R.layout.tab1 in you classes
        View view = inflater.inflate(R.layout.fragment_subscription, container, false);
        ButterKnife.bind(this, view);
        init();
        presenter = new SubscriptionPresenter(this, new SharedPreference(getActivity()));
        progressBar.setVisibility(View.VISIBLE);
        presenter.getSubscription();
        return view;
    }

    @Override
    public void init() {
        rvSubscriptions.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSubscriptions.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(getActivity(), 5), false));
        llNoSubscription.setVisibility(View.GONE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getSubscription();
            }
        });
    }

    @Override
    public void onShowProgressDialog(String msg) {

    }

    @Override
    public void onHideProgressDialog() {
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        if (results == null || results.size() == 0)
            tvEmpty.setVisibility(View.VISIBLE);
        else
            tvEmpty.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String msg) {

    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void onValidationError(String msg) {

    }


    @Override
    public void onGetSubscription(List<SubscriptionPackage> results) {
        this.results = results;
        rvSubscriptions.setAdapter(new MySubscriptionAdapter(getActivity(), results));

        llNoSubscription.setVisibility(View.GONE);

    }
}
