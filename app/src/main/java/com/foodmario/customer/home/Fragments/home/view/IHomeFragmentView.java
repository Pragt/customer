package com.foodmario.customer.home.Fragments.home.view;

import com.foodmario.customer.commonInterface.IcommonView;
import com.foodmario.customer.home.Fragments.home.model.Results;

/**
 * Created by prajit on 1/27/18.
 */

public interface IHomeFragmentView extends IcommonView {

    void showProgressBar();

    void onGetFood(Results results, boolean isLastPage);

    void onFoodReactError(String message, boolean isLiked, int oldLikeCount, int newPos);

    void onFoodReactSuccess(Boolean isLike, int likeCount, int newPos);

    void addFood(Results results, boolean isLastPage);

    void hideLoadMoreProgress();

    void doLogOut();
}
