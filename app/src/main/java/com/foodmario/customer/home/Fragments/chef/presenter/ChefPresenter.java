package com.foodmario.customer.home.Fragments.chef.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.chef.view.IChefView;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/28/18.
 */

public class ChefPresenter {


    private final SharedPreference sharedPreferences;
    private final IChefView view;

    public ChefPresenter(SharedPreference sharedPreference, IChefView view) {
        this.sharedPreferences = sharedPreference;
        this.view = view;
        this.view.init();
    }

    public void getChefList(String latitude, String longitude, String substring) {
        ApiClient.getClient().create(ApiInterface.class).getChefList(sharedPreferences.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), latitude, longitude, substring)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onGetChefList(s.results);

                            } else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                           view.onError(CommonMethods.getErrorTye(e));
                            view.onHideProgressDialog();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
