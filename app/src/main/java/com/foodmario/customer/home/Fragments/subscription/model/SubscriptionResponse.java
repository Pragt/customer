package com.foodmario.customer.home.Fragments.subscription.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("results")
    @Expose
    public List<SubscriptionPackage> results = null;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;

}