package com.foodmario.customer.home.Fragments.home.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.home.view.IHomeFragmentView;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/27/18.
 */

public class HomeFragmentPresenter implements IHomePresenter {
    SharedPreference sharedPreference;
    IHomeFragmentView view;
    public boolean isLoading = false;

    public HomeFragmentPresenter(SharedPreference sharedPreference, IHomeFragmentView view) {
        this.sharedPreference = sharedPreference;
        this.view = view;
        this.view.init();
    }

    @Override
    public void getFoodListing(String lat, String lng, String value, int type, String substring, int offset) {
        isLoading = true;
//        view.showProgressBar();
        ApiClient.getClient().create(ApiInterface.class).getFood(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), lat, lng, value, type, substring, offset, (sharedPreference.getIntValues(CommonDef.SharedPrefrences.FILTERED_ID)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            isLoading = false;
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                if (offset == 0)
                                    view.onGetFood(s.results, s.results.isLastPage);
                                else
                                    view.addFood(s.results, s.results.isLastPage);

                            } else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                            view.onError(CommonMethods.getErrorTye(e));
//                            if (CommonMethods.getErrorTye(e).contains("Something went")){
//                                view.doLogOut();
//                            }
                            view.hideLoadMoreProgress();
                            view.onHideProgressDialog();
                            isLoading = false;
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    @Override
    public void setLiked(Integer food_id, int newPos, Boolean isLiked, int oldLikeCount) {
        ApiClient.getClient().create(ApiInterface.class).toggleLike(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), food_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onFoodReactSuccess(s.results.isLike, s.results.likeCount, newPos);
                            } else {
//                                view.onError(s.message);
                                view.onFoodReactError(s.message, isLiked, oldLikeCount, newPos);
                            }
                        },
                        e -> {
//                           view.onError(CommonMethods.getErrorTye(e));
                            view.onFoodReactError(CommonDef.No_Connection, !isLiked, oldLikeCount, newPos);
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}

