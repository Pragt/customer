package com.foodmario.customer.home.Fragments.home.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.home.model.Category;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    List<Category> categoryList;
    private onCategoryClickListner listner;

    public CategoriesAdapter(Context mContext, List<Category> categoryList) {
        this.mContext = mContext;
        this.categoryList = categoryList;

    }

    public void setListner(onCategoryClickListner listner)
    {
        this.listner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_category_images, null);
        return new myCategoriesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
                holder1.tvFoodName.setText(categoryList.get(position).name);

            if (categoryList.get(position).type == 1 || categoryList.get(position).type == 2)
                LoadImage.loadCircular((Activity) mContext, holder1.ivFood, UrlHelpers.CATEGORY_FOOD_URL + categoryList.get(position).image, R.drawable.food, R.drawable.food);
            else if (categoryList.get(position).type == 3 )
                LoadImage.loadCircular((Activity) mContext, holder1.ivFood, UrlHelpers.FOOD_BASE_URL + categoryList.get(position).image, R.drawable.food, R.drawable.food);
            else
                LoadImage.loadCircular((Activity) mContext, holder1.ivFood, UrlHelpers.FOOD_BASE_URL + categoryList.get(position).image, R.drawable.food, R.drawable.food);

                holder1.ivFood.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listner.onCategoryItemClicked(position);
//                        holder1.ivFood.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_food_selected));
                    }
                });

                if (categoryList.get(position).isSelected)
                {
                    holder1.ivFood.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_food_selected));
                }else
                    holder1.ivFood.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_food_unselected));
        }
    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void addAll(List<Category> categoryList) {
        this.categoryList = categoryList;
        notifyDataSetChanged();
    }

    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        //        @BindView(R.id.fl_food)
//        FrameLayout flFood;
//
//        @BindView(R.id.iv_tick)
//        ImageView ivTick;
//
        @BindView(R.id.iv_food)
        ImageView ivFood;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface onCategoryClickListner{
        void onCategoryItemClicked(int pos);

        void onLoadAll();
    }


}
