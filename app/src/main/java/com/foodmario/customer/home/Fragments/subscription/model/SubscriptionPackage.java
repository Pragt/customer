package com.foodmario.customer.home.Fragments.subscription.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionPackage implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image_path")
    @Expose
    public String imagePath;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("combo_type")
    @Expose
    public String comboType;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("min_food_in_pack")
    @Expose
    public Integer minFoodInPack;
    @SerializedName("discount_status")
    @Expose
    public String discountStatus;
    @SerializedName("total_discount")
    @Expose
    public Integer totalDiscount;
    @SerializedName("price")
    @Expose
    public Integer subTotal;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("item_count")
    @Expose
    public Integer itemCount;
//    @SerializedName("price")
//    @Expose
//    public Integer price;
    @SerializedName("final_price")
    @Expose
    public Integer finalPrice;
    @SerializedName("food_count")
    @Expose
    public Integer foodCount;
    @SerializedName("amount_discount")
    @Expose
    public Integer amountDiscount;
    @SerializedName("percentage_discount")
    @Expose
    public Double percentageDiscount;
    @SerializedName("discount_type")
    @Expose
    public String discountType;
    @SerializedName("routine")
    @Expose
    public List<Routine> routine = null;
    @SerializedName("discount")
    @Expose
    public Discount discount;

    public String start_date = "";
}