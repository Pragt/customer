package com.foodmario.customer.home.Fragments.orderDetail.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.checkOut.model.Food;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.chef.model.Chef;
import com.foodmario.customer.home.Fragments.orderDetail.view.IOrderDetailView;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class OrderDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    IOrderDetailView listner;
    public List<Food> foods;

    public OrderDetailAdapter(Context mContext, List<Food> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }

    public void setListner(IOrderDetailView listner) {
        this.listner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_order_detail, null);
        return new myCategoriesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            if (foods.get(position).details.dishImages != null && foods.get(position).details.dishImages.size() > 0)
                LoadImage.loadCircular((Activity) mContext, holder1.ivFood, UrlHelpers.FOOD_BASE_URL + foods.get(position).details.dishImages.get(0).imagePath, R.drawable.ic_man_1, R.drawable.ic_man_1);
            holder1.tvFoodName.setText(foods.get(position).details.foodName);
            if (foods.get(position).discountAmount == 0) {
                holder1.tvPrice.setText("Rs. " + foods.get(position).details.price + "");
//                holder1.tvPrice.setPaintFlags(holder1.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder1.tvDiscountedPrice.setVisibility(View.GONE);
                holder1.tvTotalPrice.setText("Rs. " + foods.get(position).details.price * foods.get(position).qty + "");
            } else {
                holder1.tvPrice.setText("Rs. " + foods.get(position).details.price + "");
                holder1.tvPrice.setPaintFlags(holder1.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                int finalPrice = foods.get(position).details.price - foods.get(position).discountAmount;
                holder1.tvDiscountedPrice.setText("Rs. " + (finalPrice) + "");
                holder1.tvTotalPrice.setText("Rs. " + finalPrice * foods.get(position).qty + "");
            }
            holder1.tvCount.setText(foods.get(position).qty + "");

            if (foods.get(position).details.isUnAvailable == 1) {
                holder1.itemView.setAlpha(0.6f);
                holder1.tv_unavailable.setVisibility(View.VISIBLE);
            } else {
                holder1.tv_unavailable.setVisibility(View.GONE);
                holder1.itemView.setAlpha(1f);
            }

            holder1.ivAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (foods.size() > position && foods.get(position).qty < foods.get(position).maxQuantity) {
                        foods.get(position).qty++;
                        holder1.tvCount.setText(foods.get(position).qty + "");
                        listner.doUpdate(position, foods.get(position).qty - 1);
                    }

                }
            });

            holder1.ivSub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (foods.get(position).qty > 1) {
                        foods.get(position).qty--;
                        holder1.tvCount.setText(foods.get(position).qty + "");
                        listner.doUpdate(position, foods.get(position).qty + 1);
                    }

                }
            });

//            holder1.itemView.setOnTouchListener(new View.OnTouchListener() {
//
//                @Override
//                public boolean onTouch(View view, MotionEvent motionEvent) {
//                    // or get a reference to the ViewPager and cast it to ViewParent
//
//                    MainFragment.viewPager.requestDisallowInterceptTouchEvent(true);
//                    MyCartFragment.rvOrderDetail.requestDisallowInterceptTouchEvent(true);
//
//                    // let this view deal with the event or
//                    return false;
//                }
//            });

            holder1.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    //creating a popup menu
                    Context wrapper = new ContextThemeWrapper(mContext, R.style.Custom_Theme);
                    PopupMenu popup = new PopupMenu(wrapper, holder1.ivSub);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.options_menu);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.delete:
                                    pendingRemoval(position);
                                    return true;

                                default:
                                    return false;
                            }
                        }
                    });
                    //displaying the popup
                    popup.show();

                    return false;
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return foods.size();
    }

    public void addAll(List<Chef> results) {
//        this.list = results;
//        notifyDataSetChanged();
    }

    public void pendingRemoval(int position) {
        listner.deleteFood(position);
    }

    public void deleteFood(int position) {
        foods.remove(position);
        notifyItemRemoved(position);
    }

//    public boolean isPendingRemoval(int position) {
//        if (position < foods.size())
//            return false;
//        else
//            return true;
//    }

    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_sub)
        ImageView ivSub;

        @BindView(R.id.iv_add)
        ImageView ivAdd;

        @BindView(R.id.tv_count)
        TextView tvCount;

        @BindView(R.id.iv_food)
        ImageView ivFood;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.tv_total_price)
        TextView tvTotalPrice;

        @BindView(R.id.tv_unavailable)
        TextView tv_unavailable;

        @BindView(R.id.tv_discounted_price)
        TextView tvDiscountedPrice;

        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
