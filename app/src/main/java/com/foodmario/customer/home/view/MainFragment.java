package com.foodmario.customer.home.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.home.filterFood.FilterDialog;
import com.foodmario.customer.home.Fragments.home.view.HomeFragment;
import com.foodmario.customer.home.Fragments.orderDetail.view.MyCartFragment;
import com.foodmario.customer.home.Pager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainFragment extends Fragment implements TabLayout.OnTabSelectedListener, PopupMenu.OnMenuItemClickListener {

    private SharedPreference sharedPreference;
    private TabLayout tabLayout;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.iv_filter)
    ImageView ivFilter;

    @BindView(R.id.iv_location)
    ImageView ivLocation;

    public static ViewPager viewPager;

    @BindView(R.id.ic_menu)
    ImageView icMenu;


    private boolean doubleBackToExitPressedOnce = false;
    public static int pos = 1;
    private static Pager adapter;
    private static TextView tvBadgeCount;
    private static LinearLayout llBadgeContainer;
    private HomeFragment fragment;
    private PopupMenu popup;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());
        viewPager = (ViewPager) view.findViewById(R.id.pager);

        //Initializing the tablayout
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        View view1 = getActivity().getLayoutInflater().inflate(R.layout.customtabs, null);
        view1.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_home);
        tabLayout.addTab(tabLayout.newTab().setCustomView(view1));


        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment.scrollToTop();
                viewPager.setCurrentItem(0);
            }
        });

        View view2 = getActivity().getLayoutInflater().inflate(R.layout.customtabs, null);
        view2.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_chef);
        tabLayout.addTab(tabLayout.newTab().setCustomView(view2));

        View view3 = getActivity().getLayoutInflater().inflate(R.layout.customtabs, null);
        view3.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_subscription);
        tabLayout.addTab(tabLayout.newTab().setCustomView(view3));


        View view4 = getActivity().getLayoutInflater().inflate(R.layout.tab_cart, null);
        view3.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_subscription);
        tabLayout.addTab(tabLayout.newTab().setCustomView(view4));
        tvBadgeCount = (TextView) view4.findViewById(R.id.badge_count);
        llBadgeContainer = (LinearLayout) view4.findViewById(R.id.badgeCotainer);
        tvBadgeCount.setText("0");

//      tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Creating our pager adapter
        adapter = new Pager(getChildFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);


        //swiping the view
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                TabLayout.Tab tab = tabLayout.getTabAt(position);
                tab.select();
                pos = position;
                switch (position) {
                    case 0:
                        tvTitle.setText("Foodmario");
                        HomeFragment fragment = (HomeFragment) adapter.instantiateItem(viewPager, 0);
                        fragment.scrollToTop();
                        ivFilter.setVisibility(View.VISIBLE);
                        ivLocation.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        tvTitle.setText("All Chefs");
                        ivFilter.setVisibility(View.INVISIBLE);
                        ivLocation.setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        tvTitle.setText("Subscription");
                        ivFilter.setVisibility(View.INVISIBLE);
                        ivLocation.setVisibility(View.INVISIBLE);
                        break;
                    case 3:
                        tvTitle.setText("Cart Detail");
                        ivFilter.setVisibility(View.INVISIBLE);
                        ivLocation.setVisibility(View.INVISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        fragment = (HomeFragment) adapter.instantiateItem(viewPager, 0);
        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);
        return view;

    }


    @OnClick(R.id.ic_menu)
    void openMenu() {
        MainActivity.drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @OnClick(R.id.iv_filter)
    void onMyOrderClicked() {
        FilterDialog.getInstance(new FilterDialog.FilterFoodListner() {
            @Override
            public void onFoodFilter(int id) {
                reloadHome();
            }
        }).show(getActivity().getFragmentManager(), "");

    }

    @OnClick(R.id.iv_location)
    void onLocationClicked() {
        Opener.openChooseFromMap(getActivity(), true);
    }

    public static void reloadCart() {
        MyCartFragment fragment = (MyCartFragment) adapter.instantiateItem(viewPager, 3);
        fragment.onResume();
    }

    public void reloadHome() {
        fragment.onLoadAll();
    }

    public static void setBadgeCount(int count) {
        if (count > 0) {
            llBadgeContainer.setVisibility(View.VISIBLE);
            tvBadgeCount.setText(count + "");
        } else {
            llBadgeContainer.setVisibility(View.GONE);
        }

    }

    public static void gotoMyCart() {
        if (viewPager != null)
            viewPager.setCurrentItem(3);
    }

    public static void resetCart() {
        llBadgeContainer.setVisibility(View.GONE);
        MyCartFragment fragment = (MyCartFragment) adapter.instantiateItem(viewPager, 3);
        fragment.clearCart();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        fragment.scrollToTop();

        popup.getMenu().getItem(0).setChecked(false);
        popup.getMenu().getItem(1).setChecked(false);
        popup.getMenu().getItem(2).setChecked(false);
        popup.getMenu().getItem(3).setChecked(false);

        switch (menuItem.getItemId()) {
            case R.id.high_to_low:
                menuItem.setChecked(true);
//                menuItem.che
                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FILTERED_ID, 0);
//                Toast.makeText(getActivity(), "High to low Clicked", Toast.LENGTH_SHORT).show();
                reloadHome();
                break;

            case R.id.low_to_high:
                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FILTERED_ID, 1);
                menuItem.setChecked(true);
//                Toast.makeText(getActivity(), "Low to high Clicked", Toast.LENGTH_SHORT).show();
                reloadHome();
                break;

            case R.id.most_popular:
                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FILTERED_ID, 2);
                menuItem.setChecked(true);
//                Toast.makeText(getActivity(), "Most popular Clicked", Toast.LENGTH_SHORT).show();
                reloadHome();
                break;

            case R.id.fastest_delivery:
                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FILTERED_ID, 3);
                menuItem.setChecked(true);
//                Toast.makeText(getActivity(), "Fastest Delivery Clicked", Toast.LENGTH_SHORT).show();
                reloadHome();
                break;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
//        MyCartFragment fragment = (MyCartFragment) adapter.instantiateItem(viewPager, 3);
//        fragment.onResume();

    }
}
