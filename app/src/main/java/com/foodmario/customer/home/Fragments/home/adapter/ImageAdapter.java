package com.foodmario.customer.home.Fragments.home.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.home.model.DishImage;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;


public class ImageAdapter extends PagerAdapter {

    private Activity context;
    private LayoutInflater layoutInflater;
    List<DishImage> dishImages;
    private String[] images = {"https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?w=940&h=650&dpr=2&auto=compress&cs=tinysrgb", "https://images.pexels.com/photos/792027/pexels-photo-792027.jpeg?w=940&h=650&dpr=2&auto=compress&cs=tinysrgb", "https://images.pexels.com/photos/262978/pexels-photo-262978.jpeg?w=940&h=650&dpr=2&auto=compress&cs=tinysrgb"};

    public ImageAdapter(Activity context, List<DishImage> dishImages) {
        this.context = context;
        this.dishImages = dishImages;
    }

    @Override
    public int getCount() {
        return dishImages.size() == 0 ? 1 : dishImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.single_image, null);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.homeprogress);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        if (dishImages.size() != 0)
            LoadImage.loadImage(context, imageView, progressBar, UrlHelpers.FOOD_BASE_URL + dishImages.get(position).imagePath, R.drawable.placeholder_1440_960_4x, R.drawable.placeholder_1440_960_4x);
        else {
            progressBar.setVisibility(View.GONE);
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.placeholder_1440_960_4x));
        }
        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}