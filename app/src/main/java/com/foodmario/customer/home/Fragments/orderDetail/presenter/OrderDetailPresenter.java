package com.foodmario.customer.home.Fragments.orderDetail.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.orderDetail.view.IOrderDetailView;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.google.gson.JsonObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 2/1/18.
 */

public class OrderDetailPresenter {
    SharedPreference sharedPreference;
    IOrderDetailView view;

    public OrderDetailPresenter(SharedPreference sharedPreference, IOrderDetailView view) {
        this.view = view;
        this.sharedPreference = sharedPreference;
        this.view.init();
    }


    public void updateCart(JsonObject cartObject, Integer count, int position) {
        view.onShowProgressDialog("Udating cart. Please wait...");
        ApiClient.getClient().create(ApiInterface.class).updateCart(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), cartObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onUpdateSuccess(s.message, s.results);
                            } else {
                                view.onUpdateError(s.message, count, position);
                            }
                        },
                        e -> {
                            view.onHideProgressDialog();
                            view.onUpdateError(CommonMethods.getErrorTye(e), count, position);
//                           view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    public void deleteFood(Integer position, Integer foodId) {
        view.onShowProgressDialog("Deleting food. Please wait...");
        ApiClient.getClient().create(ApiInterface.class).deleteFromCart(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), sharedPreference.getIntValues(CommonDef.SharedPrefrences.CART_ID), foodId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onDeleteSuccess(s.message, position, s.results);
                            } else {
                                view.onDeleteError(s.message);
                            }
                        },
                        e -> {
                            view.onHideProgressDialog();
                            view.onDeleteError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    public void getMyOrders() {
        ApiClient.getClient().create(ApiInterface.class).getMyOrders(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), sharedPreference.getIntValues(CommonDef.SharedPrefrences.CART_ID))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onGetCart(s.results);

                            } else {
//                                view.onError(s.message);
                            }
                        },
                        e -> {
                            view.onHideProgressDialog();
                           view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

}
