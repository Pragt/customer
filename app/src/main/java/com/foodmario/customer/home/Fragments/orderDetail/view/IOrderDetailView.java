package com.foodmario.customer.home.Fragments.orderDetail.view;

import com.foodmario.customer.commonInterface.IcommonView;
import com.foodmario.customer.checkOut.model.OrderDetail;

/**
 * Created by prajit on 1/29/18.
 */

public interface IOrderDetailView extends IcommonView {
    void deleteFood(int position);

    void onDeleteSuccess(String message, Integer position, OrderDetail results);

    void onDeleteError(String message);

    void onUpdateSuccess(String message, OrderDetail results);

    void onGetCart(OrderDetail results);

    void doUpdate(int position, Integer count);

    void onUpdateError(String message, Integer oldCount, int position);
}
