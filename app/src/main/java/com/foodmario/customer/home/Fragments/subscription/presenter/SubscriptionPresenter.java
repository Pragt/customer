package com.foodmario.customer.home.Fragments.subscription.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.subscription.view.ISubscriptionView;
import com.foodmario.customer.home.Fragments.subscription.view.SubscriptionFragment;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 2/25/18.
 */

public class SubscriptionPresenter {
    ISubscriptionView view;
    SharedPreference sharedPreference;


    public SubscriptionPresenter(SubscriptionFragment subscriptionFragment, SharedPreference sharedPreference) {
        this.view = subscriptionFragment;
        this.sharedPreference = sharedPreference;
    }

    public void getSubscription() {
        ApiClient.getClient().create(ApiInterface.class).getSubscription(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onGetSubscription(s.results);

                            } else {
//                                view.onError(s.message);
                            }
                            view.onHideProgressDialog();
                        },
                        e -> {
                            view.onHideProgressDialog();
                           view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
