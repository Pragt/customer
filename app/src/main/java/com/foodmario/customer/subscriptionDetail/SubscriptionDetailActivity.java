package com.foodmario.customer.subscriptionDetail;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionPackage;
import com.foodmario.customer.setSubscription.SetSubscriptionDialog;
import com.foodmario.customer.subscriptionDetail.adapter.MySubscriptionAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SubscriptionDetailActivity extends AppCompatActivity implements MySubscriptionAdapter.SubscriptionEditListner {

    @BindView(R.id.rv_subscription_detail)
    RecyclerView rvSubscriptionDetail;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_sub_total)
    TextView tvSubTotal;


    @BindView(R.id.tv_discount)
    TextView tvDiscount;

    @BindView(R.id.tv_sub_total_title)
    TextView tvSubTotalTitle;

    @BindView(R.id.tv_grand_total)
    TextView tvGrandTotal;
    private MySubscriptionAdapter mAdapter;

    SubscriptionPackage subscriptions;
    private String subscriptionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_detail);
        ButterKnife.bind(this);
        subscriptions = (SubscriptionPackage) getIntent().getSerializableExtra("subscription");
        init();
    }

    private void init() {
        tvTitle.setText(subscriptions.name);
        rvSubscriptionDetail.setLayoutManager(new LinearLayoutManager(this));
        rvSubscriptionDetail.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(this, 5), false));
        mAdapter = new MySubscriptionAdapter(this, subscriptions.routine);
        mAdapter.setListner(this);
//        mAdapter.setListner(this);
        rvSubscriptionDetail.setAdapter(mAdapter);

        updatePrice();
        mAdapter.addAll(subscriptions.routine);

    }

    private void updatePrice() {
        tvDiscount.setText("Rs. " + subscriptions.totalDiscount);
        tvSubTotal.setText("Rs. " + subscriptions.subTotal);
        int grandTotal = subscriptions.subTotal - subscriptions.totalDiscount;
        tvGrandTotal.setText("Rs. " + grandTotal);

        if (subscriptions.type.equalsIgnoreCase("monthly"))
            subscriptionType = "28 ";
        else if (subscriptions.type.equalsIgnoreCase("biweekly"))
            subscriptionType = "14 ";
        else if (subscriptions.type.equalsIgnoreCase("weekly"))
            subscriptionType = "7 ";

        String first = "Item Subtotal (" + subscriptions.itemCount;
        String next = "<font color='#767676'> foods </font>";
        String third = " x " + subscriptionType  + "<font color='#767676'>" + "days)" +"</font>";
        tvSubTotalTitle.setText(Html.fromHtml(first + next + third));
    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

//    @Override
//    public void onItemClicked(int pos, int dayPos) {
//        SetSubscriptionDialog.getInstance(subscriptions.foods.get(pos), dayPos, subscriptions.id, subscriptions.foods.get(pos).routine.get(dayPos), new SetSubscriptionDialog.SubscriptionUpdateListner() {
//            @Override
//            public void onSubscriptionUpdate(SubscriptionPackage subscription) {
//                subscriptions = subscription;
////                init();
//                updatePrice();
//            }
//        }).show(getFragmentManager(), "");
//    }

    @OnClick(R.id.btn_proceed)
    void onProceedClicked()
    {
        Opener.openPlaceSubscriptionActivity(this, subscriptions);
    }

    @Override
    public void onEditSubscriptionListner(int dayPos, int foodPos) {
        SetSubscriptionDialog.getInstance(subscriptions.routine.get(dayPos), subscriptions.id, foodPos, new SetSubscriptionDialog.SubscriptionUpdateListner() {
            @Override
            public void onSubscriptionUpdate(SubscriptionPackage subscription) {
                subscriptions = subscription;
//                init();
                updatePrice();
                mAdapter.addAndNotify(subscriptions.routine, dayPos, foodPos);
            }
        }).show(getFragmentManager(), "");
    }

    @Override
    public void scrollToBottom(int position) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rvSubscriptionDetail.smoothScrollToPosition(position);
            }
        },100);
    }
}
