package com.foodmario.customer.subscriptionDetail.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.home.Fragments.subscription.model.Routine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class MySubscriptionAdapter extends RecyclerView.Adapter<MySubscriptionAdapter.mySubscriptionViewHolder> {

    private Context mContext;
    List<Routine> activeSubscriptions;
    SubscriptionEditListner mListner;
    int foodPos = -1;
    boolean isSingleChanged = false;
    ArrayList<MyActiveSubscriptionAdapter> listAdapter;


    public MySubscriptionAdapter(Context mContext, List<Routine> activeSubscriptions) {
        this.mContext = mContext;
        this.activeSubscriptions = activeSubscriptions;
        listAdapter = new ArrayList<>();
    }

    public void setListner(SubscriptionEditListner mListner) {
        this.mListner = mListner;
    }

    @Override
    public mySubscriptionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_subscription, null);
        return new mySubscriptionViewHolder(view);
    }


    @Override
    public void onBindViewHolder(mySubscriptionViewHolder holder, final int position) {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        if (activeSubscriptions.get(position).foods.size() == 0) {
            holder.rlNoSubscriptions.setVisibility(View.VISIBLE);
            holder.rvSubscription.setVisibility(View.GONE);
            MyActiveSubscriptionAdapter mAdapter = new MyActiveSubscriptionAdapter(mContext, activeSubscriptions.get(position).foods, true);
            listAdapter.add(mAdapter);
        } else {
            holder.rvSubscription.setVisibility(View.VISIBLE);
//            if (holder.rvSubscription.getAdapter() == null || foodPos == -1) {
            MyActiveSubscriptionAdapter mAdapter = new MyActiveSubscriptionAdapter(mContext, activeSubscriptions.get(position).foods, true);
            listAdapter.add(mAdapter);
            mAdapter.setListner(new MyActiveSubscriptionAdapter.SubscriptionListner() {
                @Override
                public void onFoodClicked(int pos) {
                    mListner.onEditSubscriptionListner(position, pos);
                }
            });
            holder.rvSubscription.setAdapter(mAdapter);
//            }
//            else {
//                ((MyActiveSubscriptionAdapter) holder.rvSubscription.getAdapter()).update(activeSubscriptions.get(position).foods.get(foodPos), foodPos);
//                 foodPos = -1;
//            }
            holder.rlNoSubscriptions.setVisibility(View.GONE);
            isSingleChanged = false;

        }

        if (activeSubscriptions.get(position).isShown) {
            holder.ll_subscription.setVisibility(View.VISIBLE);
//            holder.rlNoSubscriptions.setVisibility(View.GONE);
//            holder.tvDayName.setTextColor(ContextCompat.getColor(mContext, R.color.light_red1));
//            holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_arrow_up_red));
//            MyActiveSubscriptionAdapter mAdapter = new MyActiveSubscriptionAdapter(mContext, activeSubscriptions.get(position).foods, true);
//            mAdapter.setListner(new MyActiveSubscriptionAdapter.SubscriptionListner() {
//                @Override
//                public void onFoodClicked(int pos) {
//                    mListner.onEditSubscriptionListner(position, pos);
//                }
//            });
//            holder.rvSubscription.setAdapter(mAdapter);

//            activeSubscriptions.get(position).isShown = true;
            holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_up_arrow_black));
        }

        holder.rlDayBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activeSubscriptions.get(position).isShown) {
                    activeSubscriptions.get(position).isShown = false;
                    holder.ll_subscription.setVisibility(View.GONE);
//                    if (day == activeSubscriptions.get(position).weekId)
//                        holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_red_down_arrow));
//                    else
                    holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_down_arrow_black));
                } else {
                    activeSubscriptions.get(position).isShown = true;
                    holder.ll_subscription.setVisibility(View.VISIBLE);
                    mListner.scrollToBottom(position);
//                    if (day == activeSubscriptions.get(position).weekId)
//                        holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_arrow_up_red));
//                    else
                    holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_up_arrow_black));
                }
            }

        });

        switch (activeSubscriptions.get(position).weekId) {
            case 1:
                holder.tvDayName.setText("Sunday");
                break;
            case 2:
                holder.tvDayName.setText("Monday");
                break;
            case 3:
                holder.tvDayName.setText("Tuesday");
                break;
            case 4:
                holder.tvDayName.setText("Wednesday");
                break;
            case 5:
                holder.tvDayName.setText("Thursday");
                break;
            case 6:
                holder.tvDayName.setText("Friday");
                break;
            case 7:
                holder.tvDayName.setText("Saturday");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return activeSubscriptions.size();
    }

    public void addAll(List<Routine> activeSubscriptions) {
        isSingleChanged = false;
        listAdapter = new ArrayList<>();
        this.activeSubscriptions = activeSubscriptions;
        notifyDataSetChanged();
    }

    public void addAndNotify(List<Routine> routine, int dayPos, int foodPos) {
        isSingleChanged = true;
        this.activeSubscriptions = routine;
        this.foodPos = foodPos;
//        notifyItemChanged(dayPos);
        listAdapter.get(dayPos).update(routine.get(dayPos).foods.get(foodPos), foodPos);
//        listAdapter.get(dayPos).notifyItemChanged(foodPos);
    }

    public class mySubscriptionViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rv_subscription)
        RecyclerView rvSubscription;

        @BindView(R.id.tv_day_name)
        TextView tvDayName;

        @BindView(R.id.iv_arrow)
        ImageView ivArrow;

        @BindView(R.id.rl_no_subscription)
        RelativeLayout rlNoSubscriptions;

        @BindView(R.id.rl_day_bar)
        RelativeLayout rlDayBar;

        @BindView(R.id.ll_subscription)
        LinearLayout ll_subscription;

        public mySubscriptionViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            rvSubscription.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        }
    }

    // slide the view from below itself to the current position
    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public interface SubscriptionEditListner {
        void onEditSubscriptionListner(int dayPos, int foodId);

        void scrollToBottom(int position);
    }

}
