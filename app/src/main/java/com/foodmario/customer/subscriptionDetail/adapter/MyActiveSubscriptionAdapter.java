package com.foodmario.customer.subscriptionDetail.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.subscription.model.Food;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class MyActiveSubscriptionAdapter extends RecyclerView.Adapter<MyActiveSubscriptionAdapter.mySubscriptionViewHolder> {

    private final List<Food> foods;
    private Context mContext;
    boolean isCurrentDay;
    SubscriptionListner listner;


    public MyActiveSubscriptionAdapter(Context mContext, List<Food> foods, boolean isCurrentDay) {
        this.mContext = mContext;
        this.foods = foods;
        this.isCurrentDay = isCurrentDay;
    }

    public void setListner(SubscriptionListner mListner) {
        listner = mListner;
    }

    @Override
    public mySubscriptionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_subscription_food, null);
        return new mySubscriptionViewHolder(view);
    }


    @Override
    public void onBindViewHolder(mySubscriptionViewHolder holder, final int position) {
        holder.tvFoodName.setText(foods.get(position).details.foodName);
        holder.tvDeliveryTime.setText(CommonMethods.getTime(foods.get(position).deliveryTime));
        holder.tvQuantity.setText("Quantity: " + foods.get(position).qty);
        holder.tvFoodName.setText(foods.get(position).details.foodName);

        if (isCurrentDay) {
//            holder.tvDeliveryTime.setTextColor(ContextCompat.getColor(mContext, R.color.light_red1));
//            holder.ivFood.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_food_selected));
        }

        if (foods.get(position).details.dishImages != null && foods.get(position).details.dishImages.size() > 0) {
            LoadImage.loadCircular((Activity) mContext, holder.ivFood, UrlHelpers.FOOD_BASE_URL + foods.get(position).details.dishImages.get(0).imagePath, R.drawable.placeholder_circular_food, R.drawable.placeholder_circular_food);
        }

        if (position == foods.size() - 1)
            holder.ivLine.setVisibility(View.INVISIBLE);
        else
            holder.ivLine.setVisibility(View.VISIBLE);

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onFoodClicked(position);
            }
        });

        holder.btnEdit.setVisibility(View.VISIBLE);

        if (foods.get(position).isEnabled) {
            holder.btnEdit.setText("Edit");
            holder.btnEdit.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ripple_effect_red));
            holder.llSubscription.setAlpha(1);
            holder.llSubscription.setAlpha(1);
        }
        else {
            holder.btnEdit.setText("Add");
            holder.btnEdit.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_button_blue));
            holder.llSubscription.setAlpha((float) .4);
            holder.ivFood.setAlpha((float) .4);
        }
    }


    @Override
    public int getItemCount() {
        return foods.size();
    }

    public void update(Food food, int position) {
        foods.set(position, food);
        notifyItemChanged(position);
    }

    public class mySubscriptionViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_food)
        ImageView ivFood;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        @BindView(R.id.tv_delivery_time)
        TextView tvDeliveryTime;

        @BindView(R.id.tv_qty)
        TextView tvQuantity;

        @BindView(R.id.ll_subscription)
        LinearLayout llSubscription;

        @BindView(R.id.iv_line)
        ImageView ivLine;

        @BindView(R.id.btn_edit)
        Button btnEdit;

        public mySubscriptionViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface SubscriptionListner {
        void onFoodClicked(int pos);
    }


}
