package com.foodmario.customer.subscriptionDetail.model;

import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionPackage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateSubscriptionResponse {

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("results")
    @Expose
    public Results results;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;

    public class Results {

        @SerializedName("total_price")
        @Expose
        public Double totalPrice;
        @SerializedName("discount")
        @Expose
        public Double discount;
        @SerializedName("final_price")
        @Expose
        public Double finalPrice;

        @SerializedName("subscription")
        @Expose
        public SubscriptionPackage subscriptions;

    }

}