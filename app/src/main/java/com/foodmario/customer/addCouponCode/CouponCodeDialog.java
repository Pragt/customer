package com.foodmario.customer.addCouponCode;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.foodmario.customer.R;
import com.foodmario.customer.checkOut.model.OrderDetail;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/31/18.
 */

public class CouponCodeDialog extends DialogFragment {

    SharedPreference sharedPreference;
    @BindView(R.id.et_coupon_code)
    EditText etCouponCode;

    CustomProgressDialog pd;
    Alerts alerts;

    public static AddCouponCodeListner listner;

    public static CouponCodeDialog getInstance( AddCouponCodeListner list){
        listner = list;
        return new CouponCodeDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_coupon_code, container);
        ButterKnife.bind(this, view);
        pd = new CustomProgressDialog(getActivity());
        alerts = new Alerts(getActivity());
        sharedPreference = new SharedPreference(getActivity());

        return view;
    }

    @OnClick(R.id.iv_cross)
    void onCrossClicked()
    {
        dismiss();
    }

    @OnClick(R.id.btn_apply)
    void onAddCouponCode() {
        if (!etCouponCode.getText().toString().isEmpty()) {
            pd.showpd("Logging in.. Please wait");
            ApiClient.getClient().create(ApiInterface.class).useCouponCode(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), sharedPreference.getIntValues(CommonDef.SharedPrefrences.CART_ID), etCouponCode.getText().toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                                pd.hidepd();
                                if (s.statusCode == CommonDef.Response.SUCCESS) {
                                    listner.addCouponCodeListner(s.results);
                                } else {
                                    alerts.showErrorAlert(s.message);

                                }
                            },
                            e -> {
                                alerts.showErrorAlert(CommonMethods.getErrorTye(e));
                                pd.hidepd();
                                e.printStackTrace();
                            },
                            () -> System.out.println("supervisor list"));
            dismiss();
        }
    }

    public interface AddCouponCodeListner
    {
        void addCouponCodeListner(OrderDetail results);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
}
