package com.foodmario.customer.vendorProfile.view;

import com.foodmario.customer.commonInterface.IcommonView;
import com.foodmario.customer.vendorProfile.model.Results;

/**
 * Created by prajit on 1/27/18.
 */

public interface IVendorProfileView extends IcommonView {

    void showProgressBar();


    void onFoodReactError(String message, boolean isLiked, int oldLikeCount, int newPos);

    void onFoodReactSuccess(Boolean isLike, int likeCount, int newPos);

    void onGetProfile(Results results);
}
