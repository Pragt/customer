package com.foodmario.customer.vendorProfile.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Results {

@SerializedName("vendor")
@Expose
public Vendor vendor;
@SerializedName("categories")
@Expose
public List<Category> categories = null;

}