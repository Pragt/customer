package com.foodmario.customer.vendorProfile.model;

import com.foodmario.customer.home.Fragments.home.model.Discount;
import com.foodmario.customer.home.Fragments.home.model.DishImage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Food implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("food_name")
    @Expose
    public String foodName;
    @SerializedName("slug")
    @Expose
    public String slug;
    @SerializedName("price")
    @Expose
    public int price;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("max_unit_per_day")
    @Expose
    public Integer maxUnitPerDay;
    @SerializedName("max_pre_time")
    @Expose
    public String maxPreTime;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("discount_status")
    @Expose
    public String discountStatus;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("is_featured")
    @Expose
    public String isFeatured;
    @SerializedName("tnumbnail_path")
    @Expose
    public String tnumbnailPath;
    @SerializedName("like_count")
    @Expose
    public Integer likeCount;
    @SerializedName("comment_count")
    @Expose
    public Integer commentCount;
    @SerializedName("share_link")
    @Expose
    public String shareLink;
    @SerializedName("dish_images")
    @Expose
    public List<DishImage> dishImages = null;
    @SerializedName("discount_amount")
    @Expose
    public Float discountAmount;
    @SerializedName("discount")
    @Expose
    public Discount discount;
    @SerializedName("final_price")
    @Expose
    public int finalPrice;

    @SerializedName("is_liked")
    @Expose
    public boolean isLiked;
}