package com.foodmario.customer.vendorProfile.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.home.Fragments.home.adapter.FoodAdapter;
import com.foodmario.customer.vendorProfile.model.Category;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class FoodCategoryAdapterVendor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_CATEGORIES = 1;
    private static final int TYPE_FOOD = 2;
    private Context mContext;
    FoodListner listner;
    List<Category> categoryList;

    public FoodCategoryAdapterVendor(Context mContext, List<Category> categories) {
        this.mContext = mContext;
        this.categoryList = categories;

    }

    public void setListner(FoodListner listner) {
        this.listner = listner;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_category_item, null);
        return new myFoodViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myFoodViewHolder) {
            myFoodViewHolder holder1 = (myFoodViewHolder) holder;
            holder1.tvCategoryName.setText(categoryList.get(position).name);
            holder1.rvFood.setLayoutManager(new GridLayoutManager(mContext, 4));
            FoodGridAdapterVendor mAdapter = new FoodGridAdapterVendor(mContext, categoryList.get(position).foods, 1);
            mAdapter.setListner((FoodGridAdapterVendor.FoodImageClickListner) mContext);
            holder1.rvFood.setAdapter(mAdapter);
        }
    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_FOOD;
    }

    public void addAll(List<Category> categoryList) {
        this.categoryList = categoryList;
        notifyDataSetChanged();
    }

    public class myFoodViewHolder extends RecyclerView.ViewHolder {

       @BindView(R.id.rv_food)
       RecyclerView rvFood;

       @BindView(R.id.tv_Category_name)
       TextView tvCategoryName;

        public myFoodViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface FoodListner {

    }

}
