package com.foodmario.customer.vendorProfile.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("name")
@Expose
public String name;
@SerializedName("is_checked")
@Expose
public String isChecked;
@SerializedName("image")
@Expose
public String image;
@SerializedName("foods")
@Expose
public List<Food> foods = null;

}