package com.foodmario.customer.vendorProfile.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.utils.LoadImage;
import com.foodmario.customer.vendorProfile.model.Food;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pragt on 3/5/17.
 */

public class FoodGridAdapterVendor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_CATEGORIES = 1;
    private static final int TYPE_GRID = 2;
    private Context mContext;
    FoodImageClickListner listner;
    List<Food> foodList;
    int type;

    public FoodGridAdapterVendor(Context mContext, List<Food> categories, int type) {
        this.mContext = mContext;
        this.foodList = categories;
        this.type = type;

    }

    public void setListner(FoodImageClickListner listner) {
        this.listner = listner;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_GRID) {
            view = LayoutInflater.from(mContext).inflate(R.layout.layout_grid_image, null);
            return new myFoodViewHolder(view);
        }else
        {
            view = LayoutInflater.from(mContext).inflate(R.layout.layout_grid_item_small, null);
            return new myFoodViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myFoodViewHolder) {
            myFoodViewHolder holder1 = (myFoodViewHolder) holder;
            if (foodList.get(position).dishImages.size() > 0)
                LoadImage.loadImage((Activity) mContext, holder1.ivFood, holder1.progressBar, UrlHelpers.FOOD_BASE_URL + foodList.get(position).dishImages.get(0).imagePath, R.drawable.placeholder_480_480_4x, R.drawable.placeholder_480_480_4x);
            else {
                holder1.progressBar.setVisibility(View.GONE);
                holder1.ivFood.setImageDrawable(mContext.getResources().getDrawable(R.drawable.placeholder_480_480_4x));
            }
            holder1.itemView.setOnClickListener(v -> listner.onFoodImageClicked(foodList.get(position)));
        }
    }


    @Override
    public int getItemCount() {
        return foodList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (type == TYPE_GRID)
            return TYPE_GRID;
        else
            return TYPE_CATEGORIES;
    }

    public void addAll(List<Food> foods) {
        this.foodList = foods;
        notifyDataSetChanged();
    }

    public class myFoodViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_food)
        ImageView ivFood;

        @BindView(R.id.homeprogress)
        ProgressBar progressBar;

        public myFoodViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface FoodImageClickListner {

        void onFoodImageClicked(Food food);

    }

}
