package com.foodmario.customer.vendorProfile.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.addToCartDialog.MyBottomSheetDialogFragment;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.utils.LoadImage;
import com.foodmario.customer.vendorProfile.adapter.FoodCategoryAdapterVendor;
import com.foodmario.customer.vendorProfile.adapter.FoodGridAdapterVendor;
import com.foodmario.customer.vendorProfile.adapter.FoodListAdapterVendor;
import com.foodmario.customer.vendorProfile.model.Category;
import com.foodmario.customer.vendorProfile.model.Food;
import com.foodmario.customer.vendorProfile.model.Results;
import com.foodmario.customer.vendorProfile.model.Vendor;
import com.foodmario.customer.vendorProfile.presenter.VendorProfilePresenter;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class VendorProfile extends AppCompatActivity implements IVendorProfileView, FoodListAdapterVendor.FoodListner, FoodGridAdapterVendor.FoodImageClickListner {

    public static final int MODE_LIST = 0;
    public static final int MODE_GRID = 1;
    public static final int MODE_CATEGORY = 2;

    @BindView(R.id.rv_food)
    RecyclerView rvFood;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_vendor_name)
    TextView tvVendorName;

    @BindView(R.id.tv_vendor_address)
    TextView tvVendorAddress;

    @BindView(R.id.tv_like_count)
    TextView tvLikeCount;

    @BindView(R.id.tv_food_count)
    TextView tvFoodCount;

    @BindView(R.id.tv_order_count)
    TextView tvOrderCount;

    @BindView(R.id.tv_distance)
    TextView tvDistance;

    @BindView(R.id.tv_description)
    TextView tvDescription;

    @BindView(R.id.iv_profile_img)
    ImageView ivProfileImage;

    @BindView(R.id.iv_share)
    ImageView ivShare;

    @BindView(R.id.iv_grid)
    ImageView ivGrid;

    @BindView(R.id.iv_list)
    ImageView ivList;

    @BindView(R.id.iv_category)
    ImageView ivCategory;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.btnShowmore)
    TextView btnShowMore;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    boolean isProfileLoad = false;
    Vendor vendor;
    int mode = 0;

    Alerts alerts;
    CustomProgressDialog pd;
    SharedPreference sharedPreference;
    private ArrayList categoryList;
    private FoodListAdapterVendor mAdapter;
    private FoodGridAdapterVendor mGridAdapter;
    private FoodCategoryAdapterVendor mCategoryAdapter;
    VendorProfilePresenter presenter;
    int vendor_id = 0;
    private ArrayList<Food> foodList;
    ArrayList<Integer> foodIds;
    private List<Food> results;
    private Results categoryFoodList;
    private GridSpacingItemDecoration itemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_profile);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        presenter = new VendorProfilePresenter(sharedPreference, this);
        vendor_id = getIntent().getIntExtra(CommonDef.VENDOR_ID, 0);

        tvTitle.setText("Chef Detail");
        presenter.getVendorProfile(vendor_id);
    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onLikeClicked(int newPos) {
        boolean oldLiked = foodList.get(newPos).isLiked;
        int oldLikeCount = foodList.get(newPos).likeCount;

        if (foodList.get(newPos).isLiked) {
            foodList.get(newPos).isLiked = false;
            foodList.get(newPos).likeCount--;
        } else {
            foodList.get(newPos).isLiked = true;
            foodList.get(newPos).likeCount++;
        }
        mAdapter.setList(foodList);
        mAdapter.onLikeClicked(newPos);
        presenter.setLiked(foodList.get(newPos).id, newPos, oldLiked, oldLikeCount);
    }

    @Override
    public void onCommentClicked(int newPos) {
        Opener.openCommentActivity(this, foodList.get(newPos).id, newPos);
    }

    @Override
    public void onShareClicked(int newPos) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Foodmario");
        share.putExtra(Intent.EXTRA_TEXT, foodList.get(newPos).shareLink);

        startActivity(Intent.createChooser(share, "Foodmario"));
    }

    @Override
    public void addToCartClicked(int position) {
        String image = "";
        if (foodList.get(position).dishImages != null && foodList.get(position).dishImages.size() > 0)
            image = foodList.get(position).dishImages.get(0).imagePath;

        MyBottomSheetDialogFragment.newInstance(foodList.get(position).id, vendor.name, image, foodList.get(position).foodName, foodList.get(position).price, foodList.get(position).finalPrice, foodList.get(position).maxUnitPerDay).show(getSupportFragmentManager(), "");
    }

    @Override
    public void onImageClicked(Food food) {
        onFoodImageClicked(food);
    }

    @Override
    public void init() {
        alerts = new Alerts(this);
        pd = new CustomProgressDialog(this);

        rvFood.setLayoutManager(new LinearLayoutManager(this));
        rvFood.setNestedScrollingEnabled(false);
        itemDecoration = new GridSpacingItemDecoration(1, CommonMethods.dpToPx(this, 10), false);
        rvFood.setItemAnimator(null);
        foodList = new ArrayList<>();
        categoryList = new ArrayList<>();

//        swipeRefreshLayout.setOnRefreshmGridAdapterListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                presenter.getVendorProfile(lat, lng, value, type);
//            }
//        });

    }

    @Override
    public void onShowProgressDialog(String msg) {
        pd.showpd(msg);
//        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onHideProgressDialog() {
        pd.hidepd();
        progressBar.setVisibility(View.GONE);
//        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onSuccess(String msg) {
        alerts.showWarningAlert(msg);
    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }

    @Override
    public void showProgressBar() {
//        progressBar.setVisibility(View.VISIBLE);
    }


    @Override
    public void onFoodReactError(String message, boolean isLiked, int oldLikeCount, int newPos) {
        foodList.get(newPos).isLiked = isLiked;
        foodList.get(newPos).likeCount = oldLikeCount;
        mAdapter.addAll(foodList);
        alerts.showErrorAlert(message);
    }

    @Override
    public void onFoodReactSuccess(Boolean isLike, int likeCount, int newPos) {
        foodList.get(newPos).isLiked = isLike;
        foodList.get(newPos).likeCount = likeCount;
//        mAdapter.addAll(foodList);
        if (isLike)
            tvLikeCount.setText(convertCount(++vendor.likeCount));
        else
            tvLikeCount.setText(convertCount(--vendor.likeCount));
    }

    @Override
    public void onGetProfile(Results results) {
        scrollView.setVisibility(View.VISIBLE);
        this.results = getFoods(results);
        this.categoryFoodList = results;

        if (mode == MODE_LIST)
            loadList(getFoods(results), false);
        else if (mode == MODE_GRID)
            loadGrid(this.results, false);
        else
            loadCategory(this.categoryFoodList, false);
        loadProfile(results.vendor);
    }

    @OnClick(R.id.iv_grid)
    void onGridClicked() {
        if (mode != MODE_GRID) {
            mode = MODE_GRID;
            if (results != null)
                loadGrid(results, true);
            unselectAll();
            ivGrid.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_grid_selected));
        }
    }

    private void unselectAll() {
        ivCategory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_magic));
        ivGrid.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_grid));
        ivList.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_list_unselected));
    }

    @OnClick(R.id.iv_list)
    void onListClicked() {
        if (mode != MODE_LIST) {
            mode = MODE_LIST;
            if (results != null)
                loadList(results, true);
            unselectAll();
            ivList.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_list));
        }
    }

    @OnClick(R.id.iv_category)
    void onCategpryClicked() {
        if (mode != MODE_CATEGORY) {
            mode = MODE_CATEGORY;
            if (categoryList != null)
                loadCategory(categoryFoodList, true);
            unselectAll();
            ivCategory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_category_selected));
        }
    }

    private void loadCategory(Results results, boolean isLoadNew) {
        rvFood.setLayoutManager(new LinearLayoutManager(this));
        List<Category> categoryList = new ArrayList<>();
        for (int i = 0; i < results.categories.size(); i++) {
            if (results.categories.get(i).foods.size() > 0)
                categoryList.add(results.categories.get(i));
        }

        if (mCategoryAdapter == null || isLoadNew) {
            mCategoryAdapter = new FoodCategoryAdapterVendor(this, categoryList);
            rvFood.setAdapter(mCategoryAdapter);
        } else
            mCategoryAdapter.addAll(categoryList);
    }

    private void loadGrid(List<Food> foods, boolean loadNew) {
        rvFood.removeItemDecoration(itemDecoration);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        if (mGridAdapter == null || loadNew) {
            rvFood.setLayoutManager(layoutManager);
            mGridAdapter = new FoodGridAdapterVendor(this, foods, 2);
            mGridAdapter.setListner(this);
            rvFood.setAdapter(mGridAdapter);
        } else
            mGridAdapter.addAll(foods);
    }

    private void loadList(List<Food> foods, boolean loadNew) {
        rvFood.setLayoutManager(new LinearLayoutManager(this));
        rvFood.addItemDecoration(itemDecoration);
        if (mAdapter == null || loadNew) {
            mAdapter = new FoodListAdapterVendor(this, foods);
            mAdapter.setListner(this);

            rvFood.setAdapter(mAdapter);
        } else
            mAdapter.addAll(foods);
    }


    private String convertCount(int count) {
        if (count < 1000)
            return count + "";
        else if (count % 1000 == 0) {
            int countNum = count / 1000;
            return countNum + "K";
        } else {
            Float newCount = Float.valueOf(count) / 1000;
            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMinimumFractionDigits(1);
            formatter.setMaximumFractionDigits(1);
            String output = formatter.format(newCount);
            return output + "K";
        }

    }

    private void loadProfile(Vendor vendor) {
        this.vendor = vendor;
        tvLikeCount.setText(convertCount(vendor.likeCount));
        tvFoodCount.setText(convertCount(vendor.foodCount));
        if (!isProfileLoad) {
            isProfileLoad = true;
            tvVendorName.setText(vendor.name);
            tvVendorAddress.setText(vendor.userAddress);
            tvOrderCount.setText(convertCount(vendor.orderCount));
            tvDescription.setText(vendor.userDescription);


            ViewTreeObserver vto = tvDescription.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    ViewTreeObserver obs = tvDescription.getViewTreeObserver();
                    obs.removeGlobalOnLayoutListener(this);
                    if (tvDescription.getLineCount() > 3) {
                        Log.d("", "Line[" + tvDescription.getLineCount() + "]" + tvDescription.getText());
                        int lineEndIndex = tvDescription.getLayout().getLineEnd(3);
//                        String text = holder1.tvDescription.getText().subSequence(0, lineEndIndex-3)+"...";
//                        holder1.tvDescription.setText(text);
//                        Log.d("","NewText:"+text);
                        tvDescription.setMaxLines(3);
                        btnShowMore.setVisibility(View.VISIBLE);
                    } else
                        btnShowMore.setVisibility(View.GONE);

                }
            });

            btnShowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (btnShowMore.getText().toString().equalsIgnoreCase("Show More...")) {
                        tvDescription.setMaxLines(100);//your TextView
                        btnShowMore.setText("Show Less");
                    } else {
                        tvDescription.setMaxLines(3);//your TextView
                        btnShowMore.setText("Show More...");
                    }
                }
            });

//            tvDistance.setText(vendor.);
            LoadImage.loadCircularUserImage(this, ivProfileImage, UrlHelpers.USER_BASE_URL + vendor.userImage, R.drawable.ic_man_1, R.drawable.ic_man_1);

        }
    }

    private List<Food> getFoods(Results results) {
        foodList = new ArrayList<>();
        foodIds = new ArrayList<>();
        for (int i = 0; i < results.categories.size(); i++) {
            for (int j = 0; j < results.categories.get(i).foods.size(); j++) {
                if (!foodIds.contains(results.categories.get(i).foods.get(j).id)) {
                    foodList.add(results.categories.get(i).foods.get(j));
                    foodIds.add(results.categories.get(i).foods.get(j).id);
                }

            }
        }
        return foodList;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onFoodImageClicked(Food food) {
        Opener.openFoodDetail(this, food, vendor);
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CommonDef.GO_BACK) {
            finish();
        }
        if (requestCode == CommonDef.RESPONSE_COMMENT) {
            int pos = data.getIntExtra("pos", 0);
            int commentCount = data.getIntExtra("count", 0);
            foodList.get(pos).commentCount = commentCount;
            mAdapter.setList(foodList);
            mAdapter.onLikeClicked(pos);
        }

//        }
    }
}

