package com.foodmario.customer.vendorProfile.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.vendorProfile.view.IVendorProfileView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/27/18.
 */

public class VendorProfilePresenter implements IVendorProfilePresenter {
    SharedPreference sharedPreference;
    IVendorProfileView view;

    public VendorProfilePresenter(SharedPreference sharedPreference, IVendorProfileView view) {
        this.sharedPreference = sharedPreference;
        this.view = view;
        this.view.init();
    }

    public void getVendorProfile(int vendor_id) {
        view.showProgressBar();
        ApiClient.getClient().create(ApiInterface.class).getVendorProfile(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), vendor_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onGetProfile(s.results);

                            } else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                            view.onHideProgressDialog();
                           view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    @Override
    public void setLiked(Integer food_id, int newPos, Boolean isLiked, int oldLikeCount) {
        ApiClient.getClient().create(ApiInterface.class).toggleLike(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), food_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onFoodReactSuccess(s.results.isLike, s.results.likeCount, newPos);
                            } else {
//                                view.onError(s.message);
                                view.onFoodReactError(s.message, isLiked, oldLikeCount, newPos);
                            }
                        },
                        e -> {
//                           view.onError(CommonMethods.getErrorTye(e));
                            view.onFoodReactError(CommonMethods.getErrorTye(e), isLiked, oldLikeCount, newPos);
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}

