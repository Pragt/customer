package com.foodmario.customer.vendorProfile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorProfileResponse {

@SerializedName("status")
@Expose
public String status;
@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public Results results;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}