package com.foodmario.customer.vendorProfile.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.home.Fragments.home.adapter.ImageAdapter;
import com.foodmario.customer.vendorProfile.model.Food;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by pragt on 3/5/17.
 */

public class FoodListAdapterVendor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_CATEGORIES = 1;
    private static final int TYPE_FOOD = 2;
    private Context mContext;
    FoodListner listner;
    List<Food> foodList;
    private int MAX_LINES = 4;
    private boolean isLikeClicked = false;

    public FoodListAdapterVendor(Context mContext, List<Food> categories) {
        this.mContext = mContext;
        this.foodList = categories;
    }

    public void setListner(FoodListner listner) {
        this.listner = listner;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_food_item, null);
        return new myFoodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myFoodViewHolder) {
            myFoodViewHolder holder1 = (myFoodViewHolder) holder;
            final ImageAdapter mAdapter = new ImageAdapter((Activity) mContext, foodList.get(position).dishImages);
            holder1.viewPager.setAdapter(mAdapter);

            final int dotscount;
            final ImageView[] dots;

            holder1.viewPager.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onImageClicked(foodList.get(position));
                }
            });


            holder1.viewPager.setOnTouchListener(new View.OnTouchListener() {
                float oldX = 0, newX = 0, sens = 5;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            oldX = event.getX();
                            break;

                        case MotionEvent.ACTION_UP:
                            newX = event.getX();
                            if (Math.abs(oldX - newX) < sens) {
                                listner.onImageClicked(foodList.get(position));
                                return true;
                            }
                            oldX = 0;
                            newX = 0;
                            break;
                    }

                    return false;
                }
            });

            holder1.rlVendorInfo.setVisibility(View.GONE);
            dotscount = mAdapter.getCount();
            dots = new ImageView[dotscount];
            holder1.llDots.removeAllViews();

            holder1.tvDistance.setText(CommonMethods.calculateMinutesAndHour(Integer.parseInt(foodList.get(position).maxPreTime)));


            for (int i = 0; i < dotscount; i++) {

                dots[i] = new ImageView(mContext);
                dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_unselected_dot));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                params.setMargins(8, 0, 8, 0);

                holder1.llDots.addView(dots[i], params);

            }
            if (dots != null && dots.length > 0)
                dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_selected_dot));
            if (dots.length <= 1) {
                holder1.llDots.setVisibility(View.GONE);
            } else
                holder1.llDots.setVisibility(View.VISIBLE);

            holder1.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int pos) {

                    for (int i = 0; i < dotscount; i++) {
                        dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_unselected_dot));
                    }

                    if (dots.length > 1)
                        dots[pos].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_selected_dot));

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


            holder1.tvLikeCount.setText(foodList.get(position).likeCount + "");
            holder1.tvCommentCount.setText(foodList.get(position).commentCount + "");
            holder1.tvFoodName.setText(foodList.get(position).foodName);


            holder1.tvFinalPrice.setText("Rs. " + foodList.get(position).finalPrice);
            holder1.tvPrice.setText("Rs. " + foodList.get(position).price);
            holder1.tvPrice.setPaintFlags(holder1.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            if (String.valueOf(foodList.get(position).finalPrice).equalsIgnoreCase(String.valueOf(foodList.get(position).price))) {
                holder1.tvPrice.setVisibility(View.GONE);
            } else
                holder1.tvPrice.setVisibility(View.VISIBLE);

            holder1.llLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onLikeClicked(position);
                }
            });

            holder1.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onCommentClicked(position);
                }
            });

            holder1.llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onShareClicked(position);
                }
            });

            holder1.ivCart.setOnClickListener(v -> listner.addToCartClicked(position));

            if (foodList.get(position).isLiked)
                holder1.ivLikes.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_heart_red));
            else
                holder1.ivLikes.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_heart));


            if (!isLikeClicked) {
                holder1.tvDescription.setText(foodList.get(position).description);
                ViewTreeObserver vto = holder1.tvDescription.getViewTreeObserver();
                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        ViewTreeObserver obs = holder1.tvDescription.getViewTreeObserver();
                        obs.removeGlobalOnLayoutListener(this);
                        if (holder1.tvDescription.getLineCount() > 4) {
                            Log.d("", "Line[" + holder1.tvDescription.getLineCount() + "]" + holder1.tvDescription.getText());
                            int lineEndIndex = holder1.tvDescription.getLayout().getLineEnd(4);
//                        String text = holder1.tvDescription.getText().subSequence(0, lineEndIndex-3)+"...";
//                        holder1.tvDescription.setText(text);
//                        Log.d("","NewText:"+text);
                            holder1.tvDescription.setMaxLines(4);
                            holder1.btnShowMore.setVisibility(View.VISIBLE);
                        } else
                            holder1.btnShowMore.setVisibility(View.GONE);

                    }
                });
            }

            holder1.btnShowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder1.btnShowMore.getText().toString().equalsIgnoreCase("Show More...")) {
                        holder1.tvDescription.setMaxLines(100);//your TextView
                        holder1.btnShowMore.setText("Show Less");
                    } else {
                        holder1.tvDescription.setMaxLines(4);//your TextView
                        holder1.btnShowMore.setText("Show More...");
                    }
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return foodList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_FOOD;
    }

    public void addAll(List<Food> foods) {
        isLikeClicked = false;
        this.foodList = foods;
        notifyDataSetChanged();
    }

    public void setList(ArrayList<Food> foodList) {
        this.foodList = foodList;
    }

    public void onLikeClicked(int newPos) {
        isLikeClicked = true;
        notifyItemChanged(newPos);
    }

    public class myFoodViewHolder extends RecyclerView.ViewHolder {

        //       @BindView(R.id.tv_address)
        TextView tvAddress;

        @BindView(R.id.tv_final_price)
        TextView tvFinalPrice;

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.tv_time_distance)
        TextView tvDistance;

        @BindView(R.id.tv_like_count)
        TextView tvLikeCount;

        @BindView(R.id.tv_comment_count)
        TextView tvCommentCount;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        @BindView(R.id.tv_description)
        TextView tvDescription;


        @BindView(R.id.iv_profile_img)
        ImageView ivImage;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.viewPager)
        ViewPager viewPager;

        @BindView(R.id.slider_dots)
        LinearLayout llDots;

        @BindView(R.id.ll_like)
        LinearLayout llLike;

        @BindView(R.id.ll_comment)
        LinearLayout llComment;

        @BindView(R.id.ll_share)
        LinearLayout llShare;

        @BindView(R.id.iv_likes)
        ImageView ivLikes;

        @BindView(R.id.iv_cart)
        Button ivCart;

        @BindView(R.id.btnShowmore)
        TextView btnShowMore;

        @BindView(R.id.rl_vendor_info)
        RelativeLayout rlVendorInfo;

        public myFoodViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvDistance.setVisibility(View.VISIBLE);

        }
    }

    public interface FoodListner {

        void onLikeClicked(int position);

        void onCommentClicked(int position);

        void onShareClicked(int position);

        void addToCartClicked(int position);

        void onImageClicked(Food food);
    }

}
