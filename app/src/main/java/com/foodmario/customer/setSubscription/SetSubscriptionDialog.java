package com.foodmario.customer.setSubscription;

import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.foodmario.customer.R;
import com.foodmario.customer.checkOut.model.OrderDetail;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.subscription.model.Food;
import com.foodmario.customer.home.Fragments.subscription.model.Routine;
import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionPackage;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.google.gson.JsonObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/31/18.
 */

public class SetSubscriptionDialog extends DialogFragment {

    @BindView(R.id.tv_count)
    TextView tvCount;

    @BindView(R.id.iv_add)
    ImageView ivAdd;

    @BindView(R.id.iv_sub)
    ImageView ivSub;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.tv_price)
    TextView tvPrice;

    @BindView(R.id.tv_remove)
    TextView tvRemove;

    @BindView(R.id.tv_subscription_title)
    TextView tvSubscriptionTitle;

    private int count = 1;
    Food food;
    int subscriptionId;
    int position;
    Alerts alerts;
    SharedPreference sharedPreference;
    CustomProgressDialog pd;
    static SubscriptionUpdateListner listner;
    Routine routine;

    public static SetSubscriptionDialog getInstance(Routine routine, int id, Integer foodPos, SubscriptionUpdateListner subscriptionUpdateListner) {
        SetSubscriptionDialog dialog = new SetSubscriptionDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("foodPos", foodPos);
        bundle.putInt("subciption_id", id);
        bundle.putSerializable("routine", routine);
        listner = subscriptionUpdateListner;
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_set_subscription, container);
        ButterKnife.bind(this, view);
        alerts = new Alerts(getActivity());
        sharedPreference = new SharedPreference(getActivity());
        pd = new CustomProgressDialog(getActivity());
        subscriptionId = getArguments().getInt("subciption_id");
        position = getArguments().getInt("foodPos");
        routine = (Routine) getArguments().getSerializable("routine");
        food = routine.foods.get(position);
        pd = new CustomProgressDialog(getActivity());
        alerts = new Alerts(getActivity());
        sharedPreference = new SharedPreference(getActivity());
        tvPrice.setText("Rs. " + food.details.price * food.qty);
        count = routine.foods.get(position).qty;
        tvCount.setText(count + "");
        etTime.setText(CommonMethods.getTime(food.deliveryTime));
        tvPrice.setText("Rs. " + food.details.price * count);

        String dayName = "";
        switch (routine.weekId) {
            case 1:
                dayName = "Sunday";

                break;
            case 2:
                dayName = "Monday";

                break;
            case 3:
                dayName = "Tuesday";
                break;

            case 4:
                dayName = "Wednesday";
                break;
            case 5:
                dayName = "Thursday";
                break;
            case 6:
                dayName = "Friday";
                break;
            case 7:
                dayName = "Saturday";
                break;

        }

        tvSubscriptionTitle.setText("Set Subscription Time for " + dayName);

        if (!routine.foods.get(position).isEnabled)
            tvRemove.setText("Add subscription for " + dayName);
        else
            tvRemove.setText("Remove subscription for " + dayName);

        return view;
    }

    //
//
    public interface AddCouponCodeListner {
        void addCouponCodeListner(OrderDetail results);
    }

    @Override
    public void onStart() {
        super.onStart();
//        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @OnClick(R.id.iv_add)
    void onAddClicked() {
        count++;
        tvCount.setText(count + "");
        tvPrice.setText("Rs. " + food.details.price * count);
    }

    @OnClick(R.id.iv_sub)
    void onSubClicked() {
        if (count > 1) {
            count--;
            tvCount.setText(count + "");
            tvPrice.setText("Rs. " + food.details.price * count);
        }
    }

    @OnClick(R.id.btn_apply)
    void onApplyClicked() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("subscription_id", subscriptionId);
        JsonObject jsonFood = new JsonObject();
        jsonFood.addProperty("food_id", food.foodId);
        JsonObject rountin = new JsonObject();
        rountin.addProperty("week_id", routine.weekId);
        rountin.addProperty("delivery_time", CommonMethods.getTime1(etTime.getText().toString()));
        rountin.addProperty("is_enabled", true);
        rountin.addProperty("qty", count);
        jsonFood.add("routine", rountin);
        jsonObject.add("food", jsonFood);

        updateFood(jsonObject);
    }

    @OnClick(R.id.tv_remove)
    void onRemove() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("subscription_id", subscriptionId);
        JsonObject jsonFood = new JsonObject();
        jsonFood.addProperty("food_id", food.foodId);
        JsonObject rountin = new JsonObject();
        rountin.addProperty("week_id", routine.weekId);
        rountin.addProperty("delivery_time", CommonMethods.getTime1(etTime.getText().toString()));
        rountin.addProperty("is_enabled", !routine.foods.get(position).isEnabled);
        rountin.addProperty("qty", count);
        jsonFood.add("routine", rountin);
        jsonObject.add("food", jsonFood);

        updateFood(jsonObject);


    }

    private void updateFood(JsonObject jsonObject) {
        pd.showpd("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).updateSubscription(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            pd.hidepd();
                            dismiss();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                alerts.showSuccessAlerWithoutFinish(s.message);
                                listner.onSubscriptionUpdate(s.results.subscriptions);
                                dismiss();
                            } else {
                                alerts.showErrorAlert(s.message);

                            }
                        },
                        e -> {
                            alerts.showErrorAlert(CommonMethods.getErrorTye(e));
                            pd.hidepd();
                            dismiss();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    @OnClick(R.id.et_time)
    void doSetTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String am_pm = "";

                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);

                Calendar minCalendar = Calendar.getInstance();
                minCalendar.set(Calendar.HOUR_OF_DAY, 7);
                minCalendar.set(Calendar.MINUTE, 0);

                Calendar maxCalendar = Calendar.getInstance();
                maxCalendar.set(Calendar.HOUR_OF_DAY, 19);
                maxCalendar.set(Calendar.MINUTE, 30);

                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;

//                if (datetime.getTimeInMillis() >= minCalendar.getTimeInMillis() && datetime.getTimeInMillis() <= maxCalendar.getTimeInMillis()) {
                etTime.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                        selectedHour < 12 ? "AM" : "PM"));

//                } else {
//                    alerts.showWarningDialog("Your order can only deliver between the time frame 7:00am to 7:30pm. Please choose your appropriate time.", new Alerts.OnConfirmationClickListener() {
//                        @Override
//                        public void onYesClicked() {
//                            doSetTime();
//                        }
//
//                        @Override
//                        public void onNoClicked() {
//
//                        }
//                    }, 100);
//                }
            }
        }, hour, minute, false);
//        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public interface SubscriptionUpdateListner {
        void onSubscriptionUpdate(SubscriptionPackage subscription);
    }

}
