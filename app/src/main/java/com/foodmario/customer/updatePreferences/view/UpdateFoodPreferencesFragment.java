package com.foodmario.customer.updatePreferences.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.foodPreferences.adapter.ItemsAdapter;
import com.foodmario.customer.foodPreferences.model.FoodPreferencesResponse;
import com.foodmario.customer.foodPreferences.presenter.ChoosePreferencesPresenter;
import com.foodmario.customer.foodPreferences.view.IChoosePreferencesView;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.SharedPreference;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateFoodPreferencesFragment extends Fragment implements IChoosePreferencesView {

    @BindView(R.id.rv_food_preferenes)
    RecyclerView rvFoodPreferences;


    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    CustomProgressDialog pd;
    Alerts alerts;

    ChoosePreferencesPresenter presenter;
    SharedPreference sharedPreference;
    private List<FoodPreferencesResponse.Result> results;
    private ItemsAdapter mAdapter;
    private MainActivity mainActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.update_preferences_activity, container, false);
        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());
        presenter = new ChoosePreferencesPresenter(this, sharedPreference);
        presenter.getPreferences();
        return view;
    }

    @OnClick(R.id.btn_update)
    public void onProceedClicked() {
        presenter.setPreferences(getObject());
//        for (int i = 0; i < mAdapter.list.size(); i++) {
//            if (mAdapter.list.get(i).isChecked) {
//                presenter.setPreferences(getObject());
//                break;
//            }
//        }
    }

    private JsonObject getObject() {

        String typeIds = "";
        String categoryIds = "";
        String foodIds = "";
        for (int i = 0; i < mAdapter.list.size(); i++) {
            if (mAdapter.list.get(i).isChecked) {
                if (mAdapter.list.get(i).type == 2) {
                    if (typeIds.equalsIgnoreCase("")) {
                        typeIds = typeIds + mAdapter.list.get(i).id;
                    } else {
                        typeIds = typeIds + ","  + mAdapter.list.get(i).id;
                    }
                } else if (mAdapter.list.get(i).type == 1) {
                    if (categoryIds.equalsIgnoreCase("")) {
                        categoryIds = categoryIds + mAdapter.list.get(i).id;
                    } else {
                        categoryIds = categoryIds + ","  + mAdapter.list.get(i).id;
                    }
                } else {
                    if (foodIds.equalsIgnoreCase("")) {
                        foodIds = foodIds + mAdapter.list.get(i).id;
                    } else {
                        foodIds = foodIds + ","  + mAdapter.list.get(i).id;
                    }
                }


            }
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("category_ids", categoryIds);
        jsonObject.addProperty("type_ids", typeIds);
        jsonObject.addProperty("food_ids", foodIds);

        return jsonObject;
    }

    @Override
    public void init() {
        pd = new CustomProgressDialog(getActivity());
        alerts = new Alerts(getActivity());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        rvFoodPreferences.setLayoutManager(gridLayoutManager);
        results = new ArrayList<>();
        mAdapter = new ItemsAdapter(getActivity(), results);
        rvFoodPreferences.setAdapter(mAdapter);

    }

    @Override
    public void onShowProgressDialog(String msg) {
        pd.showpd(msg);
    }

    @Override
    public void onHideProgressDialog() {
        pd.hidepd();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String msg) {
        alerts.showToastMsg(msg);
        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.HAS_CHOOSED_FOOD_PREFERENCES, true);
//        MainFragment.reloadHome();
//        setResult(RESULT_OK);
        mainActivity.loadHomePage();

    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }


    @Override
    public void onGetFoodPreferences(List<FoodPreferencesResponse.Result> results) {
        mAdapter.addAll(results);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity)context;
    }

    @OnClick(R.id.ic_menu)
    void openMenu() {
        MainActivity.drawerLayout.openDrawer(GravityCompat.START);
    }
}

