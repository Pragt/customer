package com.foodmario.customer.forget_password;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ForgetPasswordActivity extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.btn_submit)
    Button btnSubmit;

    Alerts alerts;
    SharedPreference sharedPreference;
    CustomProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        tvTitle.setText("Forgot password");
        alerts = new Alerts(this);
        pd = new CustomProgressDialog(this);
    }

    @OnClick(R.id.iv_back)
    void onBackclicked() {
        onBackPressed();
    }

    @OnClick(R.id.btn_submit)
    void onSubmitClicked() {
        if (!etEmail.getText().toString().isEmpty()) {
            if (CommonMethods.isValidEmail(etEmail.getText().toString())) {
                doResetPassword();
            } else
                alerts.showWarningAlert("Please provide your email");
        }
    }

    private void doResetPassword() {
        pd.showpd("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).forgetPassword(etEmail.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            pd.hidepd();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                alerts.showSuccessAlert(s.message);

                            } else {
                                alerts.showWarningAlert(s.message);
                            }
                        },
                        e -> {
                            alerts.showWarningAlert(CommonDef.NETWORK_ERROR);
                            pd.hidepd();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
