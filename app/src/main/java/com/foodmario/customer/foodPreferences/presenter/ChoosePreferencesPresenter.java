package com.foodmario.customer.foodPreferences.presenter;

import com.foodmario.customer.foodPreferences.model.FoodPreferencesResponse;
import com.foodmario.customer.foodPreferences.view.ChooseFoodPreferencesActivity;
import com.foodmario.customer.foodPreferences.view.IChoosePreferencesView;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.google.gson.JsonObject;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/27/18.
 */

public class ChoosePreferencesPresenter {
    private SharedPreference sharedPreference;
    IChoosePreferencesView view;

    public ChoosePreferencesPresenter(IChoosePreferencesView view, SharedPreference sharedPreference) {
        this.sharedPreference = sharedPreference;
        this.view = view;
        this.view.init();
    }

    public void getPreferences() {
        view.showProgressBar();
        ApiClient.getClient().create(ApiInterface.class).getFoodPreferences(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onGetFoodPreferences(s.results);

                            } else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                           view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    public void setPreferences(JsonObject object) {
        view.onShowProgressDialog("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).setFoodPreferences(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), object)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onSuccess(s.message);

                            } else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                           view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
