package com.foodmario.customer.foodPreferences.view;

import com.foodmario.customer.commonInterface.IcommonView;
import com.foodmario.customer.foodPreferences.model.FoodPreferencesResponse;

import java.util.List;

/**
 * Created by prajit on 1/27/18.
 */

public interface IChoosePreferencesView extends IcommonView {
    void onGetFoodPreferences(List<FoodPreferencesResponse.Result> results);

    void showProgressBar();
}
