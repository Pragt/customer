package com.foodmario.customer.foodPreferences.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.foodPreferences.model.FoodPreferencesResponse;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class ItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    public List<FoodPreferencesResponse.Result> list;

    public ItemsAdapter(Context mContext, List<FoodPreferencesResponse.Result> results) {
        this.mContext = mContext;
        this.list = results;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_food_preferences, null);
        return new myCategoriesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            holder1.tvFoodName.setText(list.get(position).name);

            if (list.get(position).isChecked) {
                holder1.ivTick.setVisibility(View.VISIBLE);
                holder1.ivFood.setAlpha((float) 0.3);
            } else {
                holder1.ivTick.setVisibility(View.GONE);
                holder1.ivFood.setAlpha((float) 1);
            }

            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder1.ivTick.isShown()) {
                        holder1.ivTick.setVisibility(View.GONE);
//                        holder1.ivFood.setAlpha((float) 1);
                        list.get(position).isChecked = false;

                    } else {
                        holder1.ivTick.setVisibility(View.VISIBLE);
//                        holder1.ivFood.setAlpha((float) 0.3);
                        list.get(position).isChecked = true;
                    }
                }
            });

            if (list.get(position).type == 1)
                LoadImage.loadCircular((Activity) mContext, holder1.ivFood, UrlHelpers.CATEGORY_FOOD_URL + list.get(position).image, R.drawable.food, R.drawable.food);
            else if (list.get(position).type == 3)
                LoadImage.loadCircular((Activity) mContext, holder1.ivFood, UrlHelpers.FOOD_BASE_URL + list.get(position).image, R.drawable.food, R.drawable.food);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addAll(List<FoodPreferencesResponse.Result> results) {
        this.list = results;
        notifyDataSetChanged();
    }

    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fl_food)
        FrameLayout flFood;

        @BindView(R.id.iv_tick)
        ImageView ivTick;

        @BindView(R.id.iv_food)
        ImageView ivFood;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
