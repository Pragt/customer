package com.foodmario.customer.foodPreferences.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FoodPreferencesResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("results")
    @Expose
    public List<Result> results = null;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;

    public class Result {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("is_checked")
        @Expose
        public Boolean isChecked;
        @SerializedName("type")
        @Expose
        public Integer type;
        @SerializedName("image")
        @Expose
        public String image;

    }

}