package com.foodmario.customer.foodPreferences.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewStructure;
import android.widget.Button;
import android.widget.ProgressBar;

import com.foodmario.customer.R;
import com.foodmario.customer.foodPreferences.adapter.ItemsAdapter;
import com.foodmario.customer.foodPreferences.model.FoodPreferencesResponse;
import com.foodmario.customer.foodPreferences.presenter.ChoosePreferencesPresenter;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChooseFoodPreferencesActivity extends AppCompatActivity implements IChoosePreferencesView {

    @BindView(R.id.rv_food_preferenes)
    RecyclerView rvFoodPreferences;

    @BindView(R.id.btn_proceed)
    Button btnProceed;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    CustomProgressDialog pd;
    Alerts alerts;

    ChoosePreferencesPresenter presenter;
    SharedPreference sharedPreference;
    private List<FoodPreferencesResponse.Result> results;
    private ItemsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_food_preferences);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        presenter = new ChoosePreferencesPresenter(this, sharedPreference);
        presenter.getPreferences();
    }

    @OnClick(R.id.btn_proceed)
    public void onProceedClicked() {
        presenter.setPreferences(getObject());
//        for (int i = 0; i < mAdapter.list.size(); i++) {
//            if (mAdapter.list.get(i).isChecked) {
//                presenter.setPreferences(getObject());
//                break;
//            }
//        }
    }

    @OnClick(R.id.btn_later)
    void onLaterClicked()
    {
        Opener.openChooseDeliveryLocation(this);
        finish();
    }

    private JsonObject getObject() {

        String typeIds = "";
        String categoryIds = "";
        String foodIds = "";
        for (int i = 0; i < mAdapter.list.size(); i++) {
            if (mAdapter.list.get(i).isChecked) {
                if (mAdapter.list.get(i).type == 2) {
                    if (typeIds.equalsIgnoreCase("")) {
                        typeIds = typeIds + mAdapter.list.get(i).id;
                    } else {
                        typeIds = typeIds + ","  + mAdapter.list.get(i).id;
                    }
                } else if (mAdapter.list.get(i).type == 1) {
                    if (categoryIds.equalsIgnoreCase("")) {
                        categoryIds = categoryIds + mAdapter.list.get(i).id;
                    } else {
                        categoryIds = categoryIds + ","  + mAdapter.list.get(i).id;
                    }
                } else {
                    if (foodIds.equalsIgnoreCase("")) {
                        foodIds = foodIds + mAdapter.list.get(i).id;
                    } else {
                        foodIds = foodIds + ","  + mAdapter.list.get(i).id;
                    }
                }


            }
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("category_ids", categoryIds);
        jsonObject.addProperty("type_ids", typeIds);
        jsonObject.addProperty("food_ids", foodIds);

        return jsonObject;
    }

    @Override
    public void init() {
        pd = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        rvFoodPreferences.setLayoutManager(gridLayoutManager);
        results = new ArrayList<>();
        mAdapter = new ItemsAdapter(this, results);
        rvFoodPreferences.setAdapter(mAdapter);
    }

    @Override
    public void onShowProgressDialog(String msg) {
        pd.showpd(msg);
    }

    @Override
    public void onHideProgressDialog() {
        pd.hidepd();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String msg) {
        alerts.showToastMsg(msg);
        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.HAS_CHOOSED_FOOD_PREFERENCES, true);
        Opener.openChooseDeliveryLocation(this);
        finish();
    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onGetFoodPreferences(List<FoodPreferencesResponse.Result> results) {
        mAdapter.addAll(results);

    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }


}

