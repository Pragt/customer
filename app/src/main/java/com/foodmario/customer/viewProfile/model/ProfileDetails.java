package com.foodmario.customer.viewProfile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileDetails {

    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("profile_pic")
    @Expose
    public String profilePic;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("user_type")
    @Expose
    public String userType;
    @SerializedName("vat_number")
    @Expose
    public String vatNumber;
    @SerializedName("sell_status")
    @Expose
    public String sellStatus;
    @SerializedName("user_description")
    @Expose
    public String userDescription;
    @SerializedName("has_set_serving_hours")
    @Expose
    public String hasSetServingHours;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("is_active")
    @Expose
    public Integer isActive;

}