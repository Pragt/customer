package com.foodmario.customer.viewProfile.view;

import com.foodmario.customer.comment.view.ICommentView;
import com.foodmario.customer.commonInterface.IcommonView;

/**
 * Created by prajit on 2/5/18.
 */

public interface IViewProfileView extends IcommonView {
    void onProfileLoadSuccess();

}
