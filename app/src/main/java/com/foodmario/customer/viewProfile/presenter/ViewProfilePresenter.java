package com.foodmario.customer.viewProfile.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.register.model.RegisterObject;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.viewProfile.view.IViewProfileView;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 2/5/18.
 */

public class ViewProfilePresenter {
    SharedPreference sharedPreference;
    IViewProfileView view;

    public ViewProfilePresenter(SharedPreference sharedPreference, IViewProfileView view) {
        this.sharedPreference = sharedPreference;
        this.view = view;
        this.view.init();
    }

    public void updateProfile(RegisterObject object) {
        if (isAllValid(object)) {
            view.onShowProgressDialog("Please wait while updating your profile...");

            RequestBody fullName = RequestBody.create(MediaType.parse("multipart/form-data"), object.fullName);
            RequestBody location = RequestBody.create(MediaType.parse("multipart/form-data"), object.location);
            RequestBody mobile_number = RequestBody.create(MediaType.parse("multipart/form-data"), object.phoneNo);
            RequestBody latitude = RequestBody.create(MediaType.parse("multipart/form-data"), object.latitude);
            RequestBody longitude = RequestBody.create(MediaType.parse("multipart/form-data"), object.longitude);
            RequestBody aboutMe = RequestBody.create(MediaType.parse("multipart/form-data"), object.aboutMe);
            RequestBody vatNumber = RequestBody.create(MediaType.parse("multipart/form-data"), object.vatNumber);


            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            MultipartBody.Part bodyProfilePic = null;
            if (object.image != null && !object.image.equalsIgnoreCase("")) {
                File profile_pic = new File(object.image);

                RequestBody requestProfileFile = RequestBody.create(MediaType.parse("image/jpeg"), profile_pic);

                bodyProfilePic = MultipartBody.Part.createFormData("profile_pic", profile_pic.getName(), requestProfileFile);
            }

            ApiClient.getClient().create(ApiInterface.class).doUpdateYourProfile(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), fullName, mobile_number, location, bodyProfilePic, latitude, longitude, aboutMe, vatNumber)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                                view.onHideProgressDialog();
                                if (s.code == CommonDef.Response.SUCCESS) {
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.data.name);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.EMAIL, s.data.email);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.MOBILE, s.data.mobileNumber);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.ADDRESS, s.data.location);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LATITUDE, s.data.latitude);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LONGITUDE, s.data.longitude);
                                    if (s.data.vatNumber != null) {
                                        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.VAT_NUMBER, s.data.vatNumber);
                                    }
                                    if (s.data.userDescription != null)
                                        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.User_desc, s.data.userDescription);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.data.name);
                                    if (s.data.profilePic != null)
                                        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.PROFILE_PIC, s.data.profilePic);
                                    view.onSuccess(s.message);

                                } else {
                                    view.onError(s.message);

                                }
                            },
                            e -> {
                                view.onError(CommonMethods.getErrorTye(e));
                                view.onHideProgressDialog();
                                e.printStackTrace();
                            },
                            () -> System.out.println("supervisor list"));
        }
    }

    private boolean isAllValid(RegisterObject object) {
        if (object.fullName.isEmpty()) {
            view.onValidationError("Please Enter Full Name");
            return false;
        } else if (object.phoneNo.isEmpty()) {
            view.onValidationError("Please Enter Your Mobile Number");
            return false;
        } else if (!CommonMethods.isValidPhoneNumber(object.phoneNo)) {
            view.onValidationError("Please Enter Valid Mobile Number");
            return false;
        }
        return true;
    }

    public void loadProfile() {
        ApiClient.getClient().create(ApiInterface.class).loadProfile(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.profileDetails.name);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.MOBILE, s.profileDetails.mobileNumber);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.ADDRESS, s.profileDetails.location);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LATITUDE, s.profileDetails.latitude);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LONGITUDE, s.profileDetails.longitude);
                                if (s.profileDetails.vatNumber != null) {
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.VAT_NUMBER, s.profileDetails.vatNumber);
                                }
                                if (s.profileDetails.userDescription != null)
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.User_desc, s.profileDetails.userDescription);
                                if (s.profileDetails.profilePic != null)
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.PROFILE_PIC, s.profileDetails.profilePic);
                                view.onProfileLoadSuccess();

                            } else {
                                view.onError(s.message);

                            }
                        },
                        e -> {
                            view.onError(CommonMethods.getErrorTye(e));
                            view.onHideProgressDialog();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
