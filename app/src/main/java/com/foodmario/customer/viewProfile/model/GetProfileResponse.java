package com.foodmario.customer.viewProfile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProfileResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public ProfileDetails profileDetails;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}