package com.foodmario.customer.viewProfile.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.changePassword.changePasswordDialog;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.ImagePicker;
import com.foodmario.customer.helpers.LoginType;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.register.model.AddressToLatLngResponse;
import com.foodmario.customer.register.model.RegisterObject;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.utils.GooglePlaceAutoCompleteTextView;
import com.foodmario.customer.utils.LoadImage;
import com.foodmario.customer.viewProfile.presenter.ViewProfilePresenter;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ViewProfileActivity extends AppCompatActivity implements IViewProfileView {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.et_name)
    EditText etName;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_phone)
    EditText etMobile;

    @BindView(R.id.et_address)
    GooglePlaceAutoCompleteTextView etAddress;

    @BindView(R.id.et_about_yourself)
    EditText etAboutYourself;

    @BindView(R.id.iv_profile_pic)
    ImageView ivProfilePic;

    @BindView(R.id.ic_camera)
    ImageView icCamera;

    @BindView(R.id.et_vat_number)
    EditText etVatNumber;

    SharedPreference sharedPreference;
    private String imagePath;
    private CustomProgressDialog pd;
    private Uri imageUri;
    @BindView(R.id.ll_crop_image)
    LinearLayout llCropImage;

    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @BindView(R.id.btn_change_pass)
    Button btnChangePass;

    @BindView(R.id.CropImageView)
    CropImageView mCropImageView;

    private Float latitude;
    private Float longitude;

    Alerts alerts;
    ViewProfilePresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        presenter = new ViewProfilePresenter(sharedPreference, this);

        tvTitle.setText("My Profile");
        pd = new CustomProgressDialog(this);
        loadProfile();

        if (sharedPreference.getBoolValues(CommonDef.IS_FB_LOGIN)) {
            btnChangePass.setVisibility(View.GONE);
        }

        presenter.loadProfile();
        if (sharedPreference.getIntValues(CommonDef.LOGIN_TYPE) == LoginType.FACEBOOK) {
            btnChangePass.setVisibility(View.GONE);
        }
    }

    private void loadProfile() {
        if (!sharedPreference.getStringValues(CommonDef.SharedPrefrences.PROFILE_PIC).equalsIgnoreCase(""))
            icCamera.setVisibility(View.GONE);
        LoadImage.loadCircularUserImage(this, ivProfilePic, UrlHelpers.USER_BASE_URL + sharedPreference.getStringValues(CommonDef.SharedPrefrences.PROFILE_PIC), R.drawable.img_profile_placeholder, R.drawable.img_profile_placeholder);
        etName.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.FULL_NAME));
        etEmail.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.EMAIL));
        etMobile.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.MOBILE));
        etAddress.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.ADDRESS));
        etAboutYourself.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.User_desc));
        etName.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.FULL_NAME));
        etVatNumber.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.VAT_NUMBER));

        ivProfilePic.setBackground(ContextCompat.getDrawable(this, R.drawable.blue_circular_image_border));
//        etAddress.removeICon();
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.btn_change_pass)
    public void changePass() {
        changePasswordDialog dialog = changePasswordDialog.getInstance();
        dialog.show(getFragmentManager(), "changepass");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void init() {
        pd = new CustomProgressDialog(this);
        alerts = new Alerts(this);
        etAddress.init();
        etAddress.setHideMapIconMode(true);
        etAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CommonMethods.hideSoftKeyboard(ViewProfileActivity.this);
                performSearch();
            }
        });
    }

    private void performSearch() {
        if (!etAddress.getText().toString().isEmpty()) {
            if (CommonMethods.isConnectingToInternet(this)) {
                try {
                    getGeocodeFromAddress(etAddress.getText().toString().trim());

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                alerts.showErrorAlert("No internet connection available.");
                etAddress.setText("");
            }
        }
    }

    @Override
    public void onShowProgressDialog(String msg) {
        pd.showpd(msg);
    }

    @Override
    public void onHideProgressDialog() {
        pd.hidepd();
    }

    @Override
    public void onSuccess(String msg) {
        alerts.showSuccessAlert(msg);
    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }

    @Override
    public void onProfileLoadSuccess() {
        loadProfile();
    }

    class BitmapWorkerTask extends AsyncTask<Bitmap, Void, String> {
        private Bitmap data = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.showpd("saving...");
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            data = params[0];
            return saveBitmap(data);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(String fileLoc) {
            imagePath = fileLoc;
            if (imagePath != null) {
                LoadImage.loadCircular(ViewProfileActivity.this, ivProfilePic, imagePath, R.drawable.choose_pic, R.drawable.choose_pic);
                icCamera.setVisibility(View.GONE);
            }
            pd.hidepd();
        }
    }

    public String saveBitmap(Bitmap bmp) {
        FileOutputStream out = null;
        File file = getExternalCacheDir();
        file.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file1 = new File(file, fname);
        if (file1.exists()) file1.delete();

        try {
            out = new FileOutputStream(file1);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file1.getPath();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissions.length > 0)
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                startActivityForResult(ImagePicker.getPickImageChooserIntent(this), 200);
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivityForResult(ImagePicker.getGallaryImageChooserIntent(this), 200);
            } else {
                alerts.showToastMsg("Permission denied");
            }
    }

    @OnClick(R.id.iv_profile_pic)
    void onChooseImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
                return;
            } else
                startActivityForResult(ImagePicker.getPickImageChooserIntent(this), 200);
        } else
            startActivityForResult(ImagePicker.getPickImageChooserIntent(this), 200);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(CommonDef.TAG + requestCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 200:
                    // From Gallery
                    if (data != null && data.getData() != null)
                        imageUri = data.getData();
                    else // From camera
                        imageUri = ImagePicker.getPickImageResultUri();
                    boolean requirePermissions = false;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                            checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                            ImagePicker.isUriRequiresPermissions(this, imageUri)) {

                        // request permissions and handle t fhe result in onRequestPermissionsResult()
                        requirePermissions = true;
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                    }

                    if (!requirePermissions) {
                        scrollView.setVisibility(View.GONE);
                        llCropImage.setVisibility(View.VISIBLE);
                        mCropImageView.setAspectRatio(1, 1);
                        mCropImageView.setImageUriAsync(imageUri);
                    }

                    break;
            }
        }
    }

    @OnClick(R.id.btn_crop)
    void onCropClicked() {
        Bitmap cropped = mCropImageView.getCroppedImage(500, 500);
        mCropImageView.setImageBitmap(null);
        scrollView.setVisibility(View.VISIBLE);
        llCropImage.setVisibility(View.GONE);
        new BitmapWorkerTask().execute(cropped);
    }

    @OnClick(R.id.btn_new_image)
    void onNewImageClicked() {
        onChooseImage();
    }

    @OnClick(R.id.btn_save)
    void onSave() {

        if (!sharedPreference.getStringValues(CommonDef.SharedPrefrences.LATITUDE).equalsIgnoreCase("")) {
            latitude = Float.valueOf(sharedPreference.getStringValues(CommonDef.SharedPrefrences.LATITUDE));
            longitude = Float.valueOf(sharedPreference.getStringValues(CommonDef.SharedPrefrences.LONGITUDE));
        }

        RegisterObject object = new RegisterObject();
        object.fullName = etName.getText().toString();
        object.email = etEmail.getText().toString();
        object.password = "";
        object.location = etAddress.getText().toString();
        object.phoneNo = etMobile.getText().toString();
        object.image = imagePath;
        object.latitude = String.valueOf(latitude);
        object.longitude = String.valueOf(longitude);
        object.userType = "0";
        object.aboutMe = etAboutYourself.getText().toString();
        object.vatNumber = etVatNumber.getText().toString();

        presenter.updateProfile(object);
    }

    private void getGeocodeFromAddress(String address) throws UnsupportedEncodingException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<AddressToLatLngResponse> addressToLatLng = apiService.googleAddressToLatlng(address);
        addressToLatLng.enqueue(new Callback<AddressToLatLngResponse>() {
            @Override
            public void onResponse(Call<AddressToLatLngResponse> call, Response<AddressToLatLngResponse> response) {
                System.out.println("this is response " + response.body());
                if (response.isSuccessful()) {
                    if (response.body().status.equalsIgnoreCase("OK")) {
                        latitude = response.body().results.get(0).geometry.location.lat;
                        longitude = response.body().results.get(0).geometry.location.lng;

                    } else {
                        alerts.showWarningDialog("Could not fetch data for the selected location. Please try different location.", new Alerts.OnConfirmationClickListener() {
                            @Override
                            public void onYesClicked() {
                                etAddress.setText("");
                            }

                            @Override
                            public void onNoClicked() {

                            }
                        }, 2);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressToLatLngResponse> call, Throwable t) {
//                alerts.showErrorAlert("Could not fetch data for the selected location. Please try different location.");
            }
        });
    }


}
