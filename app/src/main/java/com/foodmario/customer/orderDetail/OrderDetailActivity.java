package com.foodmario.customer.orderDetail;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.orderHistory.model.RecentOrder;
import com.foodmario.customer.utils.LoadImage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderDetailActivity extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_delivered_date)
    TextView tvDeliveryDate;

    @BindView(R.id.tv_delivered_time)
    TextView tvDeliveryTime;

    @BindView(R.id.tv_delivery_address)
    TextView tvDeliveryAddress;

    @BindView(R.id.tv_mobile_no)
    TextView tvMobileNo;

    @BindView(R.id.tv_sub_total)
    TextView tvSubtotal;

    @BindView(R.id.tv_discount)
    TextView tvDiscount;

    @BindView(R.id.tv_delivery_charge)
    TextView tvDeliveryCharge;

    @BindView(R.id.tv_grand_total)
    TextView tvGrandTotal;

    @BindView(R.id.tv_additional_note)
    TextView tvAdditionalNotes;

    @BindView(R.id.tv_deliver_title)
    TextView tvDeliveryTitle;

    @BindView(R.id.tv_payment_status)
    TextView tvPaymentStatus;

    @BindView(R.id.ll_foods)
    LinearLayout llFoods;

    RecentOrder delivered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail2);
        ButterKnife.bind(this);
        tvTitle.setText("Suborder details");
        delivered = (RecentOrder) getIntent().getSerializableExtra(CommonDef.ORDER_DETAIL);
        for (int i = 0; i < delivered.foods.size(); i++) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_item_ordered_food, null);
            ImageView ivFood = (ImageView) view.findViewById(R.id.iv_food);
            TextView tvFoodName = (TextView) view.findViewById(R.id.tv_food_name);
            TextView tvQty = (TextView) view.findViewById(R.id.tv_qty);
            TextView tvPrice = (TextView) view.findViewById(R.id.tv_price);

            tvFoodName.setText(delivered.foods.get(i).details.foodName);
            tvQty.setText("Qty: " + delivered.foods.get(i).quantity);
            tvPrice.setText("Rs. " + (int) Double.parseDouble(delivered.foods.get(i).lineTotal));
            if (delivered.foods.get(i).details.dishImages.size() > 0)
                LoadImage.loadCircular((Activity) this, ivFood, UrlHelpers.FOOD_BASE_URL + delivered.foods.get(i).details.dishImages.get(0).imagePath, R.drawable.ic_man_1, R.drawable.ic_man_1);
            llFoods.addView(view);
        }

        tvMobileNo.setText(delivered.deliveryPhone);
        tvSubtotal.setText("Rs. " + (int) Double.parseDouble(delivered.subTotal));
        tvAdditionalNotes.setText(delivered.orderNote);
        tvDeliveryCharge.setText("Rs. " + (int) Double.parseDouble(delivered.deliveryCharge));
        tvDiscount.setText("Rs. " + (int) Double.parseDouble(delivered.totalPromotionDiscount));
        tvAdditionalNotes.setText(delivered.orderNote);
        tvGrandTotal.setText("Rs. " + (int) (Double.parseDouble(delivered.subTotal) - Double.parseDouble(delivered.totalPromotionDiscount)));
        tvDeliveryAddress.setText(delivered.address1 + "\n" + delivered.deliveryAddress);
        tvDeliveryDate.setText(CommonMethods.getDate(delivered.deliveryDate));
        tvDeliveryTime.setText(CommonMethods.getTime(delivered.deliveryTime));
        if (!delivered.orderStatus.equalsIgnoreCase("delivered"))
            tvDeliveryTitle.setText("Delivery on: ");
        if (delivered.paymentStatus.equalsIgnoreCase("0"))
            tvPaymentStatus.setText("unpaid");
        else
            tvPaymentStatus.setText("paid");
    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
