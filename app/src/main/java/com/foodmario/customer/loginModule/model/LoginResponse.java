package com.foodmario.customer.loginModule.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("statusCode")
    @Expose
    public int code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("results")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("user_id")
        @Expose
        public Integer userId;
        @SerializedName("profile_pic")
        @Expose
        public String profilePic;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("latitude")
        @Expose
        public String latitude;
        @SerializedName("longitude")
        @Expose
        public String longitude;
        @SerializedName("vat_number")
        @Expose
        public String vatNumber;
        @SerializedName("location")
        @Expose
        public String location;
        @SerializedName("mobile_number")
        @Expose
        public String mobileNumber;
        @SerializedName("user_type")
        @Expose
        public String userType;
        @SerializedName("sell_status")
        @Expose
        public String sellStatus;
        @SerializedName("user_description")
        @Expose
        public String userDescription;
        @SerializedName("has_set_preferences")
        @Expose
        public boolean hasChoosedFoodPreferences = false;

        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("updated_at")
        @Expose
        public String updatedAt;
        @SerializedName("is_active")
        @Expose
        public Integer isActive;
        @SerializedName("authentication_key")
        @Expose
        public String authenticationKey;

//        public boolean hasChoosedFoodPreferences = false;

    }
}