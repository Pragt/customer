package com.foodmario.customer.loginModule.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.LoginType;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.loginModule.presenter.LoginPresenter;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements ILoginView {

    @BindView(R.id.iv_facebook)
    ImageView ivFacebook;

    @BindView(R.id.tv_sign_up)
    TextView tvSignUp;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_password)
    EditText et_password;

    @BindView(R.id.btn_login)
    Button btnLogin;

    Alerts alerts;
    SharedPreference sharedPreference;
    CustomProgressDialog progressDialog;
    LoginPresenter presenter;
    private CallbackManager callbackmanager;
    private String fbProfilePic = "";
    private String firebase_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookInitialize();
        setContentView(R.layout.activity_login);
        CommonMethods.setupUI(findViewById(R.id.ll_login_activity), this);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        presenter = new LoginPresenter(sharedPreference, this);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.iv_facebook)
    void loginViaFb() {
        sharedPreference.setKeyValues(CommonDef.LOGIN_TYPE, LoginType.FACEBOOK);
        LoginManager.getInstance().logOut();
        sharedPreference.setKeyValues(CommonDef.IS_FB_LOGIN, true);
//        onShowProgressDialog("Please wait while login...");
        callbackmanager = CallbackManager.Factory.create();
        // Set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"
                , "public_profile"));

        LoginManager.getInstance().registerCallback(callbackmanager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());
//                                        try {
//                                            String email = object.getString("email");
//                                            String name = object.getString("name");
//                                            System.out.print("Email==" + email);
//                                            Opener.openCompleteProfileActivity(LoginActivity.this, name, email);
//                                            onFacebookResponse(loginResult.getAccessToken().getToken(), email);
                                        String profilePicUrl = null;
                                        try {
                                            profilePicUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                            fbProfilePic = profilePicUrl;

//                                            int  facebookId = object.getInt("id");
//                                            fbProfilePic = "https://graph.facebook.com/" + facebookId + "/picture?height=500";
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        firebase_token = FirebaseInstanceId.getInstance().getToken();
                                        if (firebase_token == null)
                                            firebase_token = "asdf";
                                        presenter.doFbLogin(loginResult.getAccessToken().getToken(), CommonMethods.getDeviceId(LoginActivity.this), firebase_token, 2);

//                                        } catch (JSONException e) {
//                                            onHideProgressDialog();
//                                            alerts.showToastMsg("Could not connect to facebook. Please try again later");
////                                            onFacebookResponse(loginResult.getAccessToken().getToken(), "");
////                                            Opener.openCompleteProfileActivity(LoginActivity.this, "Prajeet Naga", "pragt.me@gmail.com");
//
//                                            e.printStackTrace();
//                                        }
                                        System.out.print("token==" + loginResult.getAccessToken().getToken());
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,picture,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
//                        onHideProgressDialog();
//                        alerts.showToastMsg("Could not connect to facebook. Please try again later");
//                        Opener.openCompleteProfileActivity(LoginActivity.this, "Prajeet Naga", "pragt.me@gmail.com");
                    }

                    @Override
                    public void onError(FacebookException error) {
//                        if (error.getLocalizedMessage() instanceof NoConnectionPendingException)

                        onValidationError(CommonDef.No_Connection);
//                        alerts.showToastMsg("Could not connect to facebook. Please try again later");
//                        onHideProgressDialog();
//                        Opener.openCompleteProfileActivity(LoginActivity.this, "Prajeet Naga", "pragt.me@gmail.com");
                    }
                });
//        Opener.openCompleteProfileActivity(LoginActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackmanager.onActivityResult(requestCode, resultCode, data);
    }

    //Facebook Sdk initalize
    private void facebookInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(LoginActivity.this);
    }

    @OnClick(R.id.tv_sign_up)
    public void onSignUpClicked() {
        Opener.openSignUpActivity(this);
    }

    @OnClick(R.id.btn_login)
    public void doLogin() {
        sharedPreference.setKeyValues(CommonDef.LOGIN_TYPE, LoginType.NORMAL);
        firebase_token = FirebaseInstanceId.getInstance().getToken();
        sharedPreference.setKeyValues(CommonDef.IS_FB_LOGIN, false);
        if (firebase_token == null)
            firebase_token = "asdf";
        presenter.doLogin(etEmail.getText().toString(), et_password.getText().toString(), CommonMethods.getDeviceId(LoginActivity.this), firebase_token, 2);
    }

    @Override
    public void init() {
        alerts = new Alerts(this);
        progressDialog = new CustomProgressDialog(this);
        btnLogin.setEnabled(false);
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!etEmail.getText().toString().isEmpty() && !et_password.getText().toString().isEmpty()) {
                    btnLogin.setEnabled(true);
                } else
                    btnLogin.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!etEmail.getText().toString().isEmpty() && !et_password.getText().toString().isEmpty()) {
                    btnLogin.setEnabled(true);
                } else
                    btnLogin.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onShowProgressDialog(String msg) {
        progressDialog.showpd(msg);
    }

    @Override
    public void onHideProgressDialog() {
        progressDialog.hidepd();
    }

    @Override
    public void onSuccess(String msg) {
        alerts.showWarningAlert(msg);
    }

    @OnClick(R.id.tv_forget_password)
    void openForgetPass() {
        Opener.openForgetPass(this);
    }

    @Override
    public void onLoginSuccess(String msg, Boolean hasChoosedFoodPref) {
        if (!hasChoosedFoodPref)
            Opener.openChooseFoodPreference(this, true);
        else
            Opener.openChooseDeliveryLocation(LoginActivity.this);
        finish();
    }

    @Override
    public void completeYourProfile(String message, String name, String email, String firebase_token, String accessToken) {
        alerts.showWarningDialog(message, new Alerts.OnConfirmationClickListener() {
            @Override
            public void onYesClicked() {
                Opener.openCompleteProfileActivity(LoginActivity.this, name, email, fbProfilePic, firebase_token, accessToken);
            }

            @Override
            public void onNoClicked() {

            }
        }, 100);
    }

    @Override
    public void onInavtiveAccount() {
        alerts.showInactiveAccountDialog(new Alerts.OnConfirmationClickListener() {
            @Override
            public void onYesClicked() {
                presenter.resendVerificationCode(etEmail.getText().toString());
            }

            @Override
            public void onNoClicked() {

            }
        }, 100);
    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }
}

