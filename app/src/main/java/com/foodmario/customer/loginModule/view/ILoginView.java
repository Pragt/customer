package com.foodmario.customer.loginModule.view;

import com.foodmario.customer.commonInterface.IcommonView;

/**
 * Created by prajit on 12/17/17.
 */

public interface ILoginView extends IcommonView {
    void onValidationError(String msg);

    void onLoginSuccess(String message, Boolean hasChoosedFoodPref);

    void completeYourProfile(String message, String name, String email, String firebase_token, String accessToken);

    void onInavtiveAccount();
}
