package com.foodmario.customer.loginModule.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.loginModule.view.ILoginView;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 12/17/17.
 */

public class LoginPresenter {
    SharedPreference sharedPreference;
    ILoginView view;

    public LoginPresenter(SharedPreference sharedPreference, ILoginView view) {
        this.sharedPreference = sharedPreference;
        this.view = view;
        this.view.init();
    }

    public void doLogin(String email, String password, String deviceId, String firenbaseToken, int device_type) {
        if (isValid(email, password)) {
            view.onShowProgressDialog("Please wait while login...");
            ApiClient.getClient().create(ApiInterface.class).doLogin(email, password, "", deviceId, firenbaseToken, device_type, 1, 0)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                                view.onHideProgressDialog();
                                if (s.code == CommonDef.Response.SUCCESS) {
                                    view.onLoginSuccess(s.message, s.data.hasChoosedFoodPreferences);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.AUTH_KEY, "Bearer " + s.data.authenticationKey);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.IS_LOGGED_IN, true);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.data.name);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.EMAIL, s.data.email);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.MOBILE, s.data.mobileNumber);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.ADDRESS, s.data.location);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LATITUDE, s.data.latitude);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LONGITUDE, s.data.longitude);
                                    if (s.data.vatNumber != null) {
                                        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.VAT_NUMBER, s.data.vatNumber);
                                    }
                                    if (s.data.userDescription != null)
                                        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.User_desc, s.data.userDescription);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.data.name);
                                    if (s.data.profilePic != null)
                                        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.PROFILE_PIC, s.data.profilePic);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.HAS_CHOOSED_FOOD_PREFERENCES, s.data.hasChoosedFoodPreferences);
                                } else {
                                    if (s.code == 401)
                                        view.onError("Please check user credentials");
                                    else if (s.code == 604) {
                                        view.onInavtiveAccount();
                                    } else
                                        view.onError(s.message);
                                }
                            },
                            e -> {
                                view.onError(CommonMethods.getErrorTye(e));
                                view.onHideProgressDialog();
                                e.printStackTrace();
                            },
                            () -> System.out.println("supervisor list"));
        }
    }

    private boolean isValid(String email, String password) {
        if (email.isEmpty()) {
            view.onValidationError("Please provide email");
            return false;
        } else if (!CommonMethods.isValidEmail(email)) {
            view.onValidationError("Please provide valid email");
            return false;
        } else if (password.isEmpty()) {
            view.onValidationError("Please provide password");
            return false;
        }
        return true;
    }

    public void doFbLogin(String accessToken, String deviceId, String firebase_token, int device_type) {
        view.onShowProgressDialog("Logging in.. Please wait");
        ApiClient.getClient().create(ApiInterface.class).doLogin("", "", accessToken, deviceId, firebase_token, device_type, 2, 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.code == CommonDef.Response.SUCCESS) {
                                view.onLoginSuccess(s.message, s.data.hasChoosedFoodPreferences);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.AUTH_KEY, "Bearer " + s.data.authenticationKey);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.IS_LOGGED_IN, true);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.data.name);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.EMAIL, s.data.email);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.MOBILE, s.data.mobileNumber);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.ADDRESS, s.data.location);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LATITUDE, s.data.latitude);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LONGITUDE, s.data.longitude);
                                if (s.data.userDescription != null)
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.User_desc, s.data.userDescription);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.data.name);
                                if (s.data.profilePic != null)
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.PROFILE_PIC, s.data.profilePic);
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.HAS_CHOOSED_FOOD_PREFERENCES, s.data.hasChoosedFoodPreferences);
                            } else if (s.code == 602) {
                                view.completeYourProfile(s.message, s.data.name, s.data.email, firebase_token, accessToken);
                            } else {
                                view.onError(s.message);

                            }
                        },
                        e -> {
                            view.onError(CommonMethods.getErrorTye(e));
                            view.onHideProgressDialog();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    public void resendVerificationCode(String email) {
        view.onShowProgressDialog("Sending verification email. Please wait...");
        ApiClient.getClient().create(ApiInterface.class).resendVerificationCode(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onSuccess(s.message);
                            view.onHideProgressDialog();
                        },
                        e -> {
                            view.onError(CommonMethods.getErrorTye(e));
                            view.onHideProgressDialog();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
