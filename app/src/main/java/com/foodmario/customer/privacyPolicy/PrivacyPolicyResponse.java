package com.foodmario.customer.privacyPolicy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrivacyPolicyResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;

    public class Data {

        @SerializedName("privacy_policy")
        @Expose
        public String privacyPolicy;

    }
}