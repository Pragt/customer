package com.foodmario.customer.orderHistory.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.orderHistory.view.IorderHistoryView;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 2/2/18.
 */

public class OrderHistoryPresenter {
    SharedPreference sharedPreference;
    IorderHistoryView view;

    public OrderHistoryPresenter(SharedPreference sharedPreference, IorderHistoryView view) {
        this.sharedPreference = sharedPreference;
        this.view = view;
        this.view.init();
    }

    public void getMyOrders() {
        view.onShowProgressDialog("");
        ApiClient.getClient().create(ApiInterface.class).getMyOrders(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onGetOrders(s.results);

                            } else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                           view.onError(CommonMethods.getErrorTye(e));
                            view.onHideProgressDialog();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
