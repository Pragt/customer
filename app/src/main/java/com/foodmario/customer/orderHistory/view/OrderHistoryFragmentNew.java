package com.foodmario.customer.orderHistory.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.orderHistory.OrderHistoryPager;
import com.foodmario.customer.orderHistory.fragmentOrderHistory.RecentOrdersFragment;
import com.foodmario.customer.orderHistory.model.Results;
import com.foodmario.customer.orderHistory.presenter.OrderHistoryPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderHistoryFragmentNew extends Fragment implements IorderHistoryView {


    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.iv_back)
    ImageView ivBack;


    public static OrderHistoryPresenter presenter;
    SharedPreference sharedPreference;
    Alerts alerts;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.btn_order_history)
    Button btnOrderHistory;

    @BindView(R.id.btn_recent_order)
    Button btnRecentOrder;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private OrderHistoryPager adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_order_history, null);
        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());
        presenter = new OrderHistoryPresenter(sharedPreference, this);
        presenter.getMyOrders();
        ivBack.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_menu));

        adapter = new OrderHistoryPager(getFragmentManager(), 2);

        //Adding adapter to pager
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position) {
                    case 0:
                        btnRecentOrder.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.dark_button_left));
                        btnRecentOrder.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));

                        btnOrderHistory.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.transparent));
                        btnOrderHistory.setTextColor(ContextCompat.getColor(getActivity(), R.color.inactive));
                        break;
                    case 1:
                        btnOrderHistory.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.dark_button_right));
                        btnOrderHistory.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));

                        btnRecentOrder.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.transparent));
                        btnRecentOrder.setTextColor(ContextCompat.getColor(getActivity(), R.color.inactive));
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return view;
    }

    @Override
    public void init() {
        alerts = new Alerts(getActivity());

        tvTitle.setText("My Orders");
    }

    @OnClick(R.id.iv_back)
    void onMenuClicked()
    {
        MainActivity.drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void onShowProgressDialog(String msg) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideProgressDialog() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String msg) {

    }

    @Override
    public void onError(String msg) {
        onHideProgressDialog();
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {

    }


    @Override
    public void onGetOrders(Results results) {
        com.foodmario.customer.orderHistory.fragmentOrderHistory.OrderHistoryFragment.onGetOrders(results.orderHistory);
        RecentOrdersFragment.onGetOrders(results.recentOrder
        );
    }


    @OnClick(R.id.btn_recent_order)
    void onRecentOrderClicked() {
        viewPager.setCurrentItem(0);
    }

    @OnClick(R.id.btn_order_history)
    void onOrderHistoryClicked() {
        viewPager.setCurrentItem(1);
    }
}
