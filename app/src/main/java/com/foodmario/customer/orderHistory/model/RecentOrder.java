package com.foodmario.customer.orderHistory.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecentOrder implements Serializable {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("order_unique_name")
@Expose
public String orderUniqueName;
@SerializedName("user_id")
@Expose
public Integer userId;
@SerializedName("promotion_id")
@Expose
public Object promotionId;
@SerializedName("order_date")
@Expose
public String orderDate;
@SerializedName("order_status")
@Expose
public String orderStatus;
@SerializedName("order_type")
@Expose
public String orderType;
@SerializedName("delivery_address")
@Expose
public String deliveryAddress;
@SerializedName("address_1")
@Expose
public String address1;
@SerializedName("address_2")
@Expose
public String address2;
@SerializedName("delivery_lat")
@Expose
public String deliveryLat;
@SerializedName("delivery_long")
@Expose
public String deliveryLong;
@SerializedName("delivery_phone")
@Expose
public String deliveryPhone;
@SerializedName("delivery_charge")
@Expose
public String deliveryCharge;
@SerializedName("delivery_date")
@Expose
public String deliveryDate;
@SerializedName("delivery_time")
@Expose
public String deliveryTime;
@SerializedName("delivery_type")
@Expose
public String deliveryType;
@SerializedName("order_note")
@Expose
public String orderNote;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;
@SerializedName("payment_status")
@Expose
public String paymentStatus;
@SerializedName("total_discount")
@Expose
public String totalDiscount;
@SerializedName("total_promotion_discount")
@Expose
public String totalPromotionDiscount;
@SerializedName("sub_total")
@Expose
public String subTotal;
@SerializedName("foods")
@Expose
public List<Food> foods = null;
@SerializedName("grand_total")
@Expose
public String grandTotal;

}