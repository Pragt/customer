package com.foodmario.customer.orderHistory.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyOrderResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("results")
    @Expose
    public Results results;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;

}