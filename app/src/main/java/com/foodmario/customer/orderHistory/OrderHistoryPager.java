package com.foodmario.customer.orderHistory;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.foodmario.customer.orderHistory.fragmentOrderHistory.OrderHistoryFragment;
import com.foodmario.customer.orderHistory.fragmentOrderHistory.RecentOrdersFragment;

/**
 * Created by Belal on 2/3/2016.
 */
//Extending FragmentStatePagerAdapter
public class OrderHistoryPager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public OrderHistoryPager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs

        switch (position) {
            case 0:
                RecentOrdersFragment recentOrdersFragment = new RecentOrdersFragment();
                return recentOrdersFragment;
            case 1:
                OrderHistoryFragment orderHistoryFragment = new OrderHistoryFragment();
                return orderHistoryFragment;
////            case 2:
////                MySubscriptionFragment subscriptionFragment = new MySubscriptionFragment();
////                return subscriptionFragment;
//            case 3:
//                MyCartFragment cartFragment = new MyCartFragment();
//                return cartFragment;
//
            default:
                RecentOrdersFragment recentOrdersFragment1 = new RecentOrdersFragment();
                return recentOrdersFragment1;
        }
    }

    //Overriden method getCount to get the number of tabs 
    @Override
    public int getCount() {
        return tabCount;
    }
}