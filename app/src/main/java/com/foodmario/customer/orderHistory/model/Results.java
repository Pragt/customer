package com.foodmario.customer.orderHistory.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Results {

@SerializedName("recent_order")
@Expose
public List<RecentOrder> recentOrder = null;
@SerializedName("order_history")
@Expose
public List<RecentOrder> orderHistory = null;

}