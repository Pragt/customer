package com.foodmario.customer.orderHistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Food implements Serializable {

@SerializedName("food_id")
@Expose
public Integer foodId;
@SerializedName("quantity")
@Expose
public Integer quantity;
@SerializedName("status")
@Expose
public String status;
@SerializedName("order_id")
@Expose
public Integer orderId;
@SerializedName("discount_total")
@Expose
public String discountTotal;
@SerializedName("line_total")
@Expose
public String lineTotal;
@SerializedName("details")
@Expose
public Details details;

}