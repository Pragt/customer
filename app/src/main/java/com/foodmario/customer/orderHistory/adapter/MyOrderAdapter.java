package com.foodmario.customer.orderHistory.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.orderHistory.model.RecentOrder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class MyOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    ItemClickListner mListner;

    List<RecentOrder> orderList;

    public MyOrderAdapter(Context mContext, List<RecentOrder> results) {
        this.mContext = mContext;
        this.orderList = results;

    }

    public void setListner(ItemClickListner listner) {
        this.mListner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_order, null);
        return new myCategoriesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            String orderStatus = orderList.get(position).orderStatus;
            holder1.tvOrderId.setText(orderList.get(position).orderUniqueName);
            holder1.tvPlacedOn.setText(orderList.get(position).orderDate);
            holder1.btnStatus.setText(orderStatus.substring(0, 1).toUpperCase() + orderStatus.substring(1).toLowerCase());
            holder1.btnStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.dark_border_button));
            holder1.btnStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

            switch (orderList.get(position).orderStatus)
            {
                case "pending":
                    holder1.btnStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_pending));
                    holder1.btnStatus.setTextColor(ContextCompat.getColor(mContext, R.color.pending));
                    break;
                case "Cooking":
                    holder1.btnStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_ordered_placed));
                    holder1.btnStatus.setTextColor(ContextCompat.getColor(mContext, R.color.order_placed));
                    break;
                case "On the way":
                    holder1.btnStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_picked_up));
                    holder1.btnStatus.setTextColor(ContextCompat.getColor(mContext, R.color.picked_up));
                    break;
                case "delivered":
                    holder1.btnStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_delivered));
                    holder1.btnStatus.setTextColor(ContextCompat.getColor(mContext, R.color.delivered));
                    break;
                case "Waiting for cook":
                    holder1.btnStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_verified));
                    holder1.btnStatus.setTextColor(ContextCompat.getColor(mContext, R.color.verified));
                    break;
                case "Cook rejected":
                    holder1.btnStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_canceled));
                    holder1.btnStatus.setTextColor(ContextCompat.getColor(mContext, R.color.canceled));
                    break;
            }

//            LoadImage.loadCircular(mContext, holder1.ivFood, orderList.get(position).foods.get(0).details.ima);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Opener.openOrderDetailActivity((Activity) mContext, orderList.get(position));
            }
        });
    }


    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public void addAll(List<RecentOrder> results) {
        this.orderList = results;
        notifyDataSetChanged();
    }

    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.btn_status)
        Button btnStatus;


        @BindView(R.id.tv_order_id)
        TextView tvOrderId;

        @BindView(R.id.tv_placed_on)
        TextView tvPlacedOn;


        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface ItemClickListner {

    }


}
