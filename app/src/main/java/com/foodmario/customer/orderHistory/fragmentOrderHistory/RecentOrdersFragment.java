package com.foodmario.customer.orderHistory.fragmentOrderHistory;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.orderHistory.adapter.MyOrderAdapter;
import com.foodmario.customer.orderHistory.model.RecentOrder;
import com.foodmario.customer.orderHistory.view.OrderHistoryFragmentNew;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentOrdersFragment extends Fragment {

    @BindView(R.id.rv_my_orders)
    RecyclerView rvMyOrders;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private static MyOrderAdapter mAdapter;
    private static List<RecentOrder> orderList;

    SharedPreference sharedPreference;
    Alerts alerts;
    static TextView tvEmpty;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);
        ButterKnife.bind(this, view);
        ButterKnife.bind(this, view);
        init();
        orderList = new ArrayList<>();
        mAdapter = new MyOrderAdapter(getActivity(), orderList);
        tvEmpty = (TextView) view.findViewById(R.id.tv_empty);
        rvMyOrders.setAdapter(mAdapter);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                OrderHistoryFragmentNew.presenter.getMyOrders();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        return view;
    }


    public void init() {
        alerts = new Alerts(getActivity());

        rvMyOrders.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMyOrders.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(getActivity(), 5), false));
        rvMyOrders.setItemAnimator(new DefaultItemAnimator());
    }


    public static void onGetOrders(List<RecentOrder> results) {
        orderList = results;
        mAdapter.addAll(results);
        if (results.size() > 0) {
            tvEmpty.setVisibility(View.GONE);
        }else
        {
            tvEmpty.setText("No Active Orders");
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }
}
