package com.foodmario.customer.orderHistory.view;

import com.foodmario.customer.commonInterface.IcommonView;
import com.foodmario.customer.orderHistory.model.Results;

/**
 * Created by prajit on 2/2/18.
 */

public interface IorderHistoryView extends IcommonView {

    void onGetOrders(Results results);
}
