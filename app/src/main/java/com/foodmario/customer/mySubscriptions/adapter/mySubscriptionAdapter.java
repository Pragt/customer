package com.foodmario.customer.mySubscriptions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.mySubscriptions.model.Subscription;
import com.foodmario.customer.utils.LoadImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by pragt on 3/5/17.
 */

public class mySubscriptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    List<Subscription> results;
    ItemClickListner listner;

    public mySubscriptionAdapter(Context mContext, List<Subscription> results) {
        this.mContext = mContext;
        this.results = results;

    }

    public void setListner(ItemClickListner listner)
    {
        this.listner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_my_subscription, null);
        return new myCategoriesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof myCategoriesViewHolder) {
            myCategoriesViewHolder holder1 = (myCategoriesViewHolder) holder;
            Subscription result = results.get(position);
            holder1.tvFoodName.setText(result.name);
            if (result.discountTotal != 0)
            {
                holder1.tvFinalPrice.setText("Rs. " + (result.subTotal - result.discountTotal));
                holder1.tvPrice.setText("Rs. " + result.subTotal);
                holder1.tvPrice.setPaintFlags(holder1.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }else
            {
                holder1.tvFinalPrice.setText("Rs. " + result.subTotal);
                holder1.tvPrice.setVisibility(View.GONE);
            }
            holder1.tvPrice.setText("Rs. " + result.subTotal);
            holder1.tvStartDate.setText(result.startDate);
            holder1.tvEndDate.setText(result.validTo);
            String duration ="";
            if (results.get(position).type.equalsIgnoreCase("monthly"))
                duration = "28 Days ";
            else if (results.get(position).type.equalsIgnoreCase("biweekly"))
                duration = "14 Days ";
            else if (results.get(position).type.equalsIgnoreCase("weekly"))
                duration = "7 Days ";
            else
                duration = results.get(position).type;
            holder1.tvDuration.setText(duration);

            holder1.tvRenewed.setText(results.get(position).renewed+ "");

            if (result.status.equalsIgnoreCase("1"))
            {
                holder1.btnStatus.setVisibility(View.VISIBLE);
                holder1.btnExpired.setVisibility(View.GONE);
                holder1.btnRenewed.setVisibility(View.GONE);

            }else
            {
                holder1.btnStatus.setVisibility(View.GONE);
                holder1.btnExpired.setVisibility(View.VISIBLE);
                holder1.btnRenewed.setVisibility(View.VISIBLE);
            }

            holder1.btnRenewed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.onResumeClicked(position);

                }
            });

            LoadImage.loadCircularUserImage((Activity) mContext, holder1.ivFood, UrlHelpers.SUBSCRIPTION_BASE_URL + result.imagePath, R.drawable.ic_man_1, R.drawable.ic_man_1);
            holder1.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Opener.openSubscriptionDetail((Activity) mContext, result);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return results.size();
    }


    public class myCategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_food)
        ImageView ivFood;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        @BindView(R.id.tv_final_price)
        TextView tvFinalPrice;

        @BindView(R.id.tv_price)
        TextView tvPrice;


        @BindView(R.id.tv_start_date)
        TextView tvStartDate;

        @BindView(R.id.tv_end_date)
        TextView tvEndDate;

        @BindView(R.id.tv_duration)
        TextView tvDuration;

        @BindView(R.id.tv_renewed)
        TextView tvRenewed;

        @BindView(R.id.btn_expired)
        Button btnExpired;

        @BindView(R.id.btn_renew)
        Button btnRenewed;

        @BindView(R.id.btn_status)
        Button btnStatus;


        public myCategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface ItemClickListner
    {
        void onResumeClicked(int pos);
    }

}
