package com.foodmario.customer.mySubscriptions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.mySubscriptions.adapter.mySubscriptionAdapter;
import com.foodmario.customer.mySubscriptions.model.Subscription;
import com.foodmario.customer.mySubscriptions.resumeSubscription.ResumeSubscriptionDialog;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 3/12/18.
 */

public class MySubscriptionFragment extends Fragment implements mySubscriptionAdapter.ItemClickListner {

    private SharedPreference sharedPreference;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.rv_subscription)
    RecyclerView rvMySubscriptions;
    private mySubscriptionAdapter mAdadpter;

    Alerts alerts;
    private List<Subscription> results;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_subscription, container, false);
        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());
        alerts = new Alerts(getActivity());

        getMySubscription();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMySubscription();
            }
        });


        rvMySubscriptions.setLayoutManager(new LinearLayoutManager(getActivity()));

        rvMySubscriptions.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(getActivity(), 5), false));

        return view;
    }

    private void getMySubscription() {
        ApiClient.getClient().create(ApiInterface.class).getMySubscription(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            progressBar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                results = s.results;
                                mAdadpter = new mySubscriptionAdapter(getActivity(), s.results);
                                mAdadpter.setListner(this);
                                rvMySubscriptions.setAdapter(mAdadpter);
                                checkEmpty();

                            } else {
                                alerts.showErrorAlert(s.message);
                                swipeRefreshLayout.setRefreshing(false);
                                checkEmpty();
                            }
                        },
                        e -> {
                            alerts.showErrorAlert(CommonMethods.getErrorTye(e));
                            checkEmpty();
                            progressBar.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    private void checkEmpty() {
        if (results== null || results.size() == 0)
            tvEmpty.setVisibility(View.VISIBLE);
        else
            tvEmpty.setVisibility(View.GONE);
    }

    @OnClick(R.id.ic_menu)
    void openMenu() {
        MainActivity.drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void onResumeClicked(int pos) {
        ResumeSubscriptionDialog.getInstance(new ResumeSubscriptionDialog.ResumeSubsListner() {
            @Override
            public void onResume() {
                Opener.openResumeSubscriptionActivity(getActivity(), results.get(pos).subscriptionId, results.get(pos).type);
            }
        }).show(getActivity().getFragmentManager(), "");
    }
}
