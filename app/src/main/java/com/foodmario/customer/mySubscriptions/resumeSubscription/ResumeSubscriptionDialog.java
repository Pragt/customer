package com.foodmario.customer.mySubscriptions.resumeSubscription;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodmario.customer.R;
import com.foodmario.customer.checkOut.model.OrderDetail;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by prajit on 1/31/18.
 */

public class ResumeSubscriptionDialog extends DialogFragment {

    static ResumeSubsListner listner;

    public static ResumeSubscriptionDialog getInstance(ResumeSubsListner list) {
        ResumeSubscriptionDialog dialog = new ResumeSubscriptionDialog();
        listner = list;
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_resume_subscription, container);
        ButterKnife.bind(this, view);
        return view;
    }


    public interface AddCouponCodeListner {
        void addCouponCodeListner(OrderDetail results);
    }

    @Override
    public void onStart() {
        super.onStart();
//        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    public interface ResumeSubsListner{
        void onResume();
    }

    @OnClick(R.id.btn_resume)
    void onResumeClicked()
    {
        listner.onResume();
        dismiss();
    }

    @OnClick(R.id.btn_cancel)
    void onCancelClicked()
    {
        dismiss();
    }
}
