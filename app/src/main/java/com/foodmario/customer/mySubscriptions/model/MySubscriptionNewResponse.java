package com.foodmario.customer.mySubscriptions.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MySubscriptionNewResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public List<Subscription> results = null;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}