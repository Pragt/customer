package com.foodmario.customer.mySubscriptions.mySubscriptionDetail;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.customViews.GridSpacingItemDecoration;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.mySubscriptions.model.Routine;
import com.foodmario.customer.mySubscriptions.model.Subscription;
import com.foodmario.customer.mySubscriptions.mySubscriptionDetail.adapter.MySubscriptionAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MySubscriptionDetailActivity extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.rv_subscription_detail)
    RecyclerView rvSubscriptionDetail;
    private MySubscriptionAdapter mAdapter;

    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscription_detail);
        ButterKnife.bind(this);
        tvTitle.setText("Routine");

        subscription = (Subscription) getIntent().getSerializableExtra("subscription");
        rvSubscriptionDetail.setLayoutManager(new LinearLayoutManager(this));
        rvSubscriptionDetail.addItemDecoration(new GridSpacingItemDecoration(1, CommonMethods.dpToPx(this, 5), false));
        rvSubscriptionDetail.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new MySubscriptionAdapter(this, subscription.routine);
        rvSubscriptionDetail.setAdapter(mAdapter);
    }

    @OnClick(R.id.iv_back)
    void onBackClicked()
    {
        onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
