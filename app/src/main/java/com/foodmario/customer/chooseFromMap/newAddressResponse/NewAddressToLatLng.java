package com.foodmario.customer.chooseFromMap.newAddressResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by prajit on 2/27/18.
 */

public class NewAddressToLatLng {
    @SerializedName("results")
    @Expose
    public List<Result> results = null;
    @SerializedName("status")
    @Expose
    public String status;

    public class Result {

        @SerializedName("address_components")
        @Expose
        public List<AddressComponent> addressComponents = null;
        @SerializedName("formatted_address")
        @Expose
        public String formattedAddress;
        @SerializedName("geometry")
        @Expose
        public Geometry geometry;
        @SerializedName("place_id")
        @Expose
        public String placeId;
        @SerializedName("types")
        @Expose
        public List<String> types = null;

    }


    public class AddressComponent {

        @SerializedName("long_name")
        @Expose
        public String longName;
        @SerializedName("short_name")
        @Expose
        public String shortName;
        @SerializedName("types")
        @Expose
        public List<String> types = null;

    }

    public class Geometry {

        @SerializedName("location")
        @Expose
        public Location location;
        @SerializedName("location_type")
        @Expose
        public String locationType;


    }
    public class Location {

        @SerializedName("lat")
        @Expose
        public Float lat;
        @SerializedName("lng")
        @Expose
        public Float lng;

    }

}
