package com.foodmario.customer.chooseFromMap;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;

import com.foodmario.customer.ChooseLocationActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.chooseFromMap.newAddressResponse.NewAddressToLatLng;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.utils.GooglePlaceAutoCompleteTextView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseFromMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerDragListener {

    @BindView(R.id.et_location)
    GooglePlaceAutoCompleteTextView etLocation;
    private MapFragment supportMapFragment;
    private LatLng searchLatLng;
    private double latitude = 27.7172;
    private double longitude = 85.3240;

    SharedPreference sharedPreference;
    private GoogleMap googleMap;
    private Geocoder geocoder;
    boolean isFromHome;

    Alerts alerts;
    CustomProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_from_map);
        ButterKnife.bind(this);
        etLocation.setHideMapIconMode(true);
        etLocation.isChooseFromMap(true);
        etLocation.init();
        alerts = new Alerts(this);
        pd = new CustomProgressDialog(this);

        sharedPreference = new SharedPreference(this);
        isFromHome = getIntent().getBooleanExtra(CommonDef.IS_FROM_HOME, false);

        if (ChooseLocationActivity.delivery_lat != null) {
            latitude = Double.parseDouble(ChooseLocationActivity.delivery_lat);
            longitude = Double.parseDouble(ChooseLocationActivity.delivery_lng);
        }

        supportMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);


        geocoder = new Geocoder(this, Locale.getDefault());


        etLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CommonMethods.hideSoftKeyboard(ChooseFromMapActivity.this);
                performSearch();
            }
        });
    }

    private void performSearch() {
        if (!etLocation.getText().toString().isEmpty()) {
            if (CommonMethods.isConnectingToInternet(this)) {
                try {
                    getGeocodeFromAddress(etLocation.getText().toString().trim());

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                alerts.showErrorAlert(CommonDef.No_Connection);
                etLocation.setText("");
            }
        }
    }

    private void getGeocodeFromAddress(String address) throws UnsupportedEncodingException {
        pd.showpd("Please wait...");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<NewAddressToLatLng> addressToLatLng = apiService.googleAddressToLatlngNew(address);
        addressToLatLng.enqueue(new Callback<NewAddressToLatLng>() {
            @Override
            public void onResponse(Call<NewAddressToLatLng> call, Response<NewAddressToLatLng> response) {
                System.out.println("this is response " + response.body());
                pd.hidepd();
                String res = response.body().results.toString();
                if (response.isSuccessful()) {
                    if (response.body().status.equalsIgnoreCase("OK") && response.body().results.get(0).geometry.location.lat != null) {
                        latitude = Double.valueOf(response.body().results.get(0).geometry.location.lat.toString());
                        longitude = Double.valueOf(response.body().results.get(0).geometry.location.lng.toString());
                        searchLatLng = new LatLng(latitude, longitude);
                        setMarker(false);

                    } else {
                        alerts.showWarningDialog("Could not fetch data for the selected location. Please try different location.", new Alerts.OnConfirmationClickListener() {
                            @Override
                            public void onYesClicked() {
                                etLocation.setText("");
                            }

                            @Override
                            public void onNoClicked() {

                            }
                        }, 2);
                    }
                }
            }

            @Override
            public void onFailure(Call<NewAddressToLatLng> call, Throwable t) {
                pd.hidepd();
//                alerts.showErrorAlert("Could not fetch data for the selected location. Please try different location.");
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerDragListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPisPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

//        setMapView();
        searchLatLng = new LatLng(latitude, longitude);

        // zoom in the camera to city
        setMarker(true);
    }

    private void setMarker(boolean isPopulateAddress) {
        googleMap.clear();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(searchLatLng, 18));
        Bitmap markerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_marker);
//        markerBitmap = scaleBitmap(markerBitmap, , 70);

        MarkerOptions markerOptions = new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(markerBitmap))
                .position(searchLatLng)
                .title("Delivery Location")
                .draggable(false);
        googleMap.addMarker(markerOptions);
        animateCamera();
        getUserAddress(searchLatLng, isPopulateAddress);
    }

    private void animateCamera() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(searchLatLng);
        LatLngBounds bounds = builder.build();

//        int width = getResources().getDisplayMetrics().widthPixels;
//        int height = getResources().getDisplayMetrics().heightPixels;
//        int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen
//
//        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//        googleMap.animateCamera(cu);
//
//        googleMap.animateCamera(cu);
//        LatLngBounds bounds = builder.build();

        float currentZoom = googleMap.getCameraPosition().zoom;
//        if (currentZoom < 5) {
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, (int) CommonMethods.pxFromDp(getActivity(), 20));
//            googleMap.animateCamera(cu);

        CameraPosition camPos = new CameraPosition.Builder()
                .target(bounds.getCenter())
                .zoom(currentZoom)
                .build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        googleMap.animateCamera(camUpd3);
//        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        searchLatLng = latLng;
        latitude = latLng.latitude;
        longitude = latLng.longitude;
        setMarker(true);
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    private void getUserAddress(LatLng point, boolean isPopulateAddress) {
        List<Address> addresses = new ArrayList<>();
        try {
            addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address address = null;
        try {

            address = addresses.get(0);

        } catch (Exception exx) {
        }

        if (address != null) {
            StringBuilder sb = new StringBuilder();
            try {
                sb.append(address.getAddressLine(0) + "\n");
            } catch (Exception exx) {

            }
//            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//            }
            sharedPreference.setKeyValues(CommonDef.SharedPrefrences.DELIVERY_LOCATION, sb.toString());
            etLocation.isPopulateAddress = false;
            if (isPopulateAddress)
                etLocation.setText(sb.toString());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    etLocation.isPopulateAddress = true;
                }
            }, 3000);
//
        }

        //remove previously placed Marker
    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.btn_set_location)
    void onSetLocationClicked() {
        if (!etLocation.getText().toString().isEmpty()) {
            if (!isFromHome) {
                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.DELIVERY_LOCATION, etLocation.getText().toString());
                ChooseLocationActivity.delivery_location = etLocation.getText().toString();
                ChooseLocationActivity.tvChooseFromMap.setText(etLocation.getText().toString());
                ChooseLocationActivity.delivery_lng = String.valueOf(longitude);
                ChooseLocationActivity.delivery_lat = String.valueOf(latitude);
                ChooseLocationActivity.setLocationFromMap(etLocation.getText().toString());
                finish();
            } else {
                sharedPreference.setKeyValues(CommonDef.DELIVERY_LOCATION, etLocation.getText().toString());
                sharedPreference.setKeyValues(CommonDef.DELIVERY_LAT, String.valueOf(latitude));
                sharedPreference.setKeyValues(CommonDef.DELIVERY_LON, String.valueOf(longitude));
                Opener.openHomeActivity(this);
                finishAffinity();
            }
        }
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();
        float pivotX = 0;
        float pivotY = 0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }
}
