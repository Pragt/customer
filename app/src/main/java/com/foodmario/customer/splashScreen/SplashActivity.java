package com.foodmario.customer.splashScreen;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.utils.AppLog;

import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SplashActivity extends AppCompatActivity {

    private int _showTime = 3000;
    private boolean _isActive = true;
    SharedPreference sharedPreference;

    @BindView(R.id.rl_no_commection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.btn_retry)
    Button btnRetry;
    private boolean isConnectionAvailable = true;
    private int type = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        printKeyHash();
        loginOperation();
        checkConnection();

    }

    private void loginOperation() {
        try {
            if (getIntent().getExtras() != null) {
                for (String key : getIntent().getExtras().keySet()) {
                    try {
                        String value = getIntent().getExtras().getString(key);
                        AppLog.d("prajit", "fcm_Key, " + key + " fcm_Value " + value);
                        if (key.equalsIgnoreCase("type")) {
                            type = Integer.parseInt(value);
                        }
                    } catch (Exception exx) {

                    }
                }
            }
        } catch (Exception exx) {
            exx.printStackTrace();
        }
        // If user tabs notification bar it will gets data
    }

    private void checkConnection() {
        if (CommonMethods.isConnectingToInternet(this))
            isConnectionAvailable();
        else
            rlNoConnection.setVisibility(View.VISIBLE);
    }

    private boolean isConnectionAvailable() {
        ApiClient.getClient().create(ApiInterface.class).getPrivacyPolicy(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            startLooping();
                        },
                        e -> {
                            if (e instanceof UnknownHostException) {
                                isConnectionAvailable = false;
                            }
                            startLooping();
//                            else if (e instanceof SocketTimeoutException) {
//                                view.showAlert("Time out");
//                            }
//                            else if (e instanceof HttpException) {
//                                handleHttpException(view, e);
//                            }
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
        return true;
    }

    public void startLooping() {

//        printKeyHash(this);

        Thread showThread = new Thread() {
            public Intent intent;

            @Override
            public void run() {
                Looper.prepare();
                try {
                    int waitTime = 0;
                    while (waitTime < _showTime) {
                        sleep(100);
                        waitTime += 100;

                        if (waitTime >= _showTime && _isActive) {
                            if (sharedPreference.getBoolValues(CommonDef.SharedPrefrences.IS_LOGGED_IN)) {
                                if (!sharedPreference.getBoolValues(CommonDef.SharedPrefrences.HAS_CHOOSED_FOOD_PREFERENCES))
                                    Opener.openChooseFoodPreference(SplashActivity.this, true);
                                else {
                                    if (type == 1) {
                                        intent = new Intent(SplashActivity.this, MainActivity.class);
                                        intent.putExtra(CommonDef.GO_TO_MY_ORDER, true);
                                        intent.putExtra(CommonDef.GO_TO_SUBSCRIPTION, false);
                                    } else if (type == 2) {
                                        intent = new Intent(SplashActivity.this, MainActivity.class);
                                        intent.putExtra(CommonDef.GO_TO_SUBSCRIPTION, true);
                                        intent.putExtra(CommonDef.GO_TO_MY_ORDER, false);
                                    } else if (type == 3)
                                        Opener.openHomeActivity(SplashActivity.this);
                                    else
                                        Opener.openChooseDeliveryLocation(SplashActivity.this);
                                }
                            } else
                                Opener.LoginActivity(SplashActivity.this);
                            finish();
                        }
                    }
                } catch (
                        InterruptedException e) {
                }
                Looper.loop();
            }
        };
        showThread.start();
    }

    @OnClick(R.id.btn_retry)
    void onRetryClicked() {
        rlNoConnection.setVisibility(View.GONE);
        checkConnection();
    }

    void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.foodmario.customer",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
