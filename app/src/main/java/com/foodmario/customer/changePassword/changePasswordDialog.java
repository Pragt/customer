package com.foodmario.customer.changePassword;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.checkOut.model.OrderDetail;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/31/18.
 */

public class changePasswordDialog extends DialogFragment {

    SharedPreference sharedPreference;

    @BindView(R.id.et_old_pass)
    EditText etOldPass;

    @BindView(R.id.et_new_pass)
    EditText etNewPass;

    CustomProgressDialog pd;
    Alerts alerts;

    public static AddCouponCodeListner listner;

    public static changePasswordDialog getInstance() {
        return new changePasswordDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_password_dialog, container);
        ButterKnife.bind(this, view);
        pd = new CustomProgressDialog(getActivity());
        alerts = new Alerts(getActivity());
        sharedPreference = new SharedPreference(getActivity());

        return view;
    }


    public interface AddCouponCodeListner {
        void addCouponCodeListner(OrderDetail results);
    }

    @Override
    public void onStart() {
        super.onStart();
//        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private boolean isAllValid() {
        if (etOldPass.getText().toString().isEmpty()) {
            alerts.showToastMsg("Please provide old password");
            return false;
        } else if (etOldPass.getText().toString().isEmpty()) {
            alerts.showToastMsg("Please provide new password");
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_apply)
    void changePass() {
        if (isAllValid()) {
            pd.showpd("Logging in.. Please wait");
            ApiClient.getClient().create(ApiInterface.class).changePassword(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), etOldPass.getText().toString(), etNewPass.getText().toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                                pd.hidepd();
                                if (s.statusCode == CommonDef.Response.SUCCESS) {
                                    alerts.showSuccessAlerWithoutFinish(s.message);
                                    dismiss();
                                } else {
                                    alerts.showWarningAlert(s.message);
                                }
                            },
                            e -> {
                                alerts.showWarningAlert(CommonDef.NETWORK_ERROR);
                                pd.hidepd();
                                e.printStackTrace();
                            },
                            () -> System.out.println("supervisor list"));
        }

    }
}
