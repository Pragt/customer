package com.foodmario.customer.thankyouscreen;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.home.view.MainFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ThankyouSubscriptionActivity extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.btn_order_detail)
    Button btnOrderDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou_subscription);
        ButterKnife.bind(this);

        tvTitle.setText("Subscription Confirmation");
        btnOrderDetail.setVisibility(View.GONE);

    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.btn_home)
    void onGotoHomeClicked() {
        MainFragment.viewPager.setCurrentItem(0);
        Opener.openHomeActivity(this);
        finishAffinity();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
