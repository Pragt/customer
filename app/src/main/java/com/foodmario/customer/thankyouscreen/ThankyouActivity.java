package com.foodmario.customer.thankyouscreen;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.home.view.MainFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ThankyouActivity extends AppCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);
        ButterKnife.bind(this);

        tvTitle.setText("Order Confirmation");
    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.btn_home)
    void onGotoHomeClicked() {
        MainFragment.viewPager.setCurrentItem(0);
        Opener.openHomeActivity(this);
        finishAffinity();
    }

    @OnClick(R.id.btn_order_detail)
    void onOrderDetailClicked() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(CommonDef.GO_TO_MY_ORDER, true);
        startActivity(intent);
        finishAffinity();
        finishAffinity();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
