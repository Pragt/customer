package com.foodmario.customer.commonInterface;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NormalResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("statusCode")
@Expose
public int statusCode;

}