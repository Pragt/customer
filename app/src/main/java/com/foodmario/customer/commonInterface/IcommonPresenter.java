package com.foodmario.customer.commonInterface;

/**
 * Created by pragt on 5/13/17.
 */

public interface IcommonPresenter {
    void onShowProgressDialog(String msg);
    void onHideProgressDialog();
    void onSuccess(String msg);
    void onError(String msg);


}
