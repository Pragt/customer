package com.foodmario.customer.commonInterface;

import com.foodmario.customer.foodPreferences.model.FoodPreferencesResponse;

import java.util.List;

/**
 * Created by pragt on 5/13/17.
 */

public interface IcommonView {
    void init();
    void onShowProgressDialog(String msg);
    void onHideProgressDialog();
    void onSuccess(String msg);
    void onError(String msg);
    void onValidationError(String msg);
}
