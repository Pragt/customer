package com.foodmario.customer.addToCartDialog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Results {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("user_id")
@Expose
public Integer userId;
@SerializedName("promo_id")
@Expose
public Object promoId;
@SerializedName("delivery_address")
@Expose
public String deliveryAddress;
@SerializedName("date")
@Expose
public String date;
@SerializedName("delivery_lat")
@Expose
public String deliveryLat;
@SerializedName("delivery_long")
@Expose
public String deliveryLong;
@SerializedName("delivery_phone")
@Expose
public String deliveryPhone;
@SerializedName("delivery_date")
@Expose
public String deliveryDate;
@SerializedName("delivery_time")
@Expose
public String deliveryTime;
@SerializedName("note")
@Expose
public Object note;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;
@SerializedName("foods")
@Expose
public List<Food> foods = null;

}