package com.foodmario.customer.addToCartDialog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddToCartResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public Results results;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}