package com.foodmario.customer.addToCartDialog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Food {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("cart_id")
@Expose
public Integer cartId;
@SerializedName("food_id")
@Expose
public Integer foodId;
@SerializedName("qty")
@Expose
public Integer qty;
@SerializedName("details")
@Expose
public Details details;

}