package com.foodmario.customer.addToCartDialog;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.view.MainFragment;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.utils.LoadImage;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by MG on 17-07-2016.
 */
public class MyBottomSheetDialogFragment extends BottomSheetDialogFragment {

    String mString;
    static CardChooseListner cardChooseListner;
    @BindView(R.id.iv_profile_img)
    ImageView ivProfileImage;

    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_vendor_name)
    TextView tvVendorName;

    @BindView(R.id.tv_price)
    TextView tvPrice;

    @BindView(R.id.tv_discounted_price)
    TextView tvDiscountedPrice;

    @BindView(R.id.tv_count)
    TextView tvCount;

    @BindView(R.id.iv_add)
    ImageView ivAdd;

    @BindView(R.id.iv_sub)
    ImageView ivSub;

    @BindView(R.id.btn_add_to_list)
    Button btnAddtoList;

    @BindView(R.id.btn_place_order)
    Button btnPlaceOrder;

    int count = 1;
    private int food_id;

    private int max_order = 0;
    CustomProgressDialog pd;
    Alerts alerts;
    SharedPreference sharedPreference;

    public static MyBottomSheetDialogFragment newInstance(Integer id, String name, String image, String foodName, Integer price, Integer finalPrice, Integer maxOrder) {
        MyBottomSheetDialogFragment dialog = new MyBottomSheetDialogFragment();
        Bundle args = new Bundle();
        args.putInt("food_id", id);
        args.putString("food_name", foodName);
        args.putString("user_image", image);
        args.putInt("price", price);
        args.putInt("final_price", finalPrice);
        args.putInt("max_order", maxOrder);
        args.putString("username", name);
        dialog.setArguments(args);
//        cardChooseListnerner = listner;
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mString = getArguments().getString("title");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,

                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_model, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ButterKnife.bind(this, v);
        LoadImage.loadCircular(getActivity(), ivProfileImage, UrlHelpers.FOOD_BASE_URL + getArguments().getString("user_image"), R.drawable.ic_man_1, R.drawable.ic_man_1);
        tvFoodName.setText(getArguments().getString("food_name"));
        tvVendorName.setText(getArguments().getString("username"));
        tvPrice.setText("Rs. " + getArguments().getInt("price") + "");
        max_order = getArguments().getInt("max_order");
        tvDiscountedPrice.setText("Rs. " + getArguments().getInt("final_price") + "");
        if (getArguments().getInt("price") == getArguments().getInt("final_price")) {
            tvPrice.setVisibility(View.GONE);
        } else {
            tvPrice.setPaintFlags(tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        food_id = getArguments().getInt("food_id");

        alerts = new Alerts(getActivity());
        sharedPreference = new SharedPreference(getActivity());
        pd = new CustomProgressDialog(getActivity());

        btnAddtoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart(false);
            }
        });

        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart(true);
            }
        });

        return v;
    }

    private void addToCart(boolean goToCheckOut) {

        pd.showpd("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).addToCart(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), getFoodObject())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {

//


                            pd.hidepd();
                            dismiss();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                if (goToCheckOut) {
                                    Opener.openCheckoutActivity(getActivity(), s.results);
                                    alerts.showToastMsg(s.message);

                                } else {
//                                    Opener.MyCartFragment(getActivity(), s.results);
//                                    Intent intent = new Intent(getActivity(), MyCartFragment.class);
//                                    intent.putExtra(CommonDef.ORDER_DETAIL, s.results);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                    getActivity().startActivity(intent);
//                                    MainFragment.viewPager.setCurrentItem(3);
                                    alerts.showSuccessAlerWithoutFinish(s.message);
                                }
                                sharedPreference.setKeyValues(CommonDef.SharedPrefrences.CART_ID, s.results.id);

                                MainFragment.reloadCart();

                            } else {
                                alerts.showErrorAlert(s.message);

                            }
                        },
                        e -> {
                            alerts.showErrorAlert(CommonMethods.getErrorTye(e));
                            pd.hidepd();
                            dismiss();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    private JsonObject getFoodObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("cart_id", sharedPreference.getIntValues(CommonDef.SharedPrefrences.CART_ID));
        JsonObject food = new JsonObject();
        food.addProperty("food_id", food_id);
        food.addProperty("quantity", count);
        jsonObject.add("food", food);

        return jsonObject;
    }

    private void onShowProgressDialog(String msg) {

    }


    public interface CardChooseListner {
        void onSelectPaypal();

        void onSelectCreditCard();
    }

    @OnClick(R.id.iv_add)
    void onAddClicked() {
        if (count < max_order) {
            count++;
            tvCount.setText(count + "");
            int totaPrice = getArguments().getInt("final_price") * count;
            tvDiscountedPrice.setText("Rs. " + totaPrice + "");
            tvPrice.setText("Rs. " + getArguments().getInt("price") * count + "");
        }
    }

    @OnClick(R.id.iv_sub)
    void onSubClicked() {
        if (count > 1) {
            count--;
            tvCount.setText(count + "");
            tvDiscountedPrice.setText("Rs. " + getArguments().getInt("final_price") * count + "");
            tvPrice.setText("Rs. " + getArguments().getInt("price") * count + "");
        }
    }
}