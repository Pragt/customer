package com.foodmario.customer.termsAndconditions;

import com.foodmario.customer.privacyPolicy.PrivacyPolicyResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TermsAndConditionsResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;

    public class Data {

        @SerializedName("Terms_Of_Use")
        @Expose
        public String terms;

    }
}