package com.foodmario.customer.termsAndconditions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/31/18.
 */

public class TermsAndConditionFragment extends Fragment {

    SharedPreference sharedPreference;
    @BindView(R.id.webview)
    WebView webview;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    Alerts alerts;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());
        init();
        alerts = new Alerts(getActivity());
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDefaultFontSize(37);
        webview.setBackgroundColor(getResources().getColor(R.color.light_grey_pressed));
        tvTitle.setText("Terms of use");
        return view;
    }

    private void init() {
        ApiClient.getClient().create(ApiInterface.class).getTermsAndConditions(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/Cabin-Regular.otf\")}body {font-family: MyFont;font-size: medium;text-align: justify; background-color: #F6F6F6;}</style></head><body>";
                                String pas = "</body></html>";
                                String myHtmlString = pish + s.data.terms + pas;
                                webview.loadDataWithBaseURL(null, myHtmlString, "text/html", "UTF-8", null);
//                                webview.loadData(myHtmlString, "text/html", "UTF-8");
                            } else {
                                onError(s.message);
                            }
                        },
                        e -> {
                            onError(CommonMethods.getErrorTye(e));
                            onHideProgressDialog();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    private void onError(String message) {
        alerts.showErrorAlert(message);
    }

    private void onHideProgressDialog() {
        progressBar.setVisibility(View.GONE);
    }

    @OnClick(R.id.ic_menu)
    void openMenu() {
        MainActivity.drawerLayout.openDrawer(GravityCompat.START);
    }
}
