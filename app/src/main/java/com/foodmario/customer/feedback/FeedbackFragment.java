package com.foodmario.customer.feedback;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.foodmario.customer.MainActivity;
import com.foodmario.customer.R;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/31/18.
 */

public class FeedbackFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    @BindView(R.id.rb_food_1)
    RadioButton rbFood1;

    @BindView(R.id.rb_food_2)
    RadioButton rbFood2;

    @BindView(R.id.rb_food_3)
    RadioButton rbFood3;

    @BindView(R.id.rb_packing_1)
    RadioButton rbPacking1;
    @BindView(R.id.rb_packing_2)
    RadioButton rbPacking2;

    @BindView(R.id.rb_packing_3)
    RadioButton rbPacking3;

    @BindView(R.id.rb_delivery_1)
    RadioButton rbDelivery1;

    @BindView(R.id.rb_delivery_2)
    RadioButton rbDelivery2;

    @BindView(R.id.rb_delivery_3)
    RadioButton rbDelivery3;

    @BindView(R.id.btn_send)
    Button btnSend;

    @BindView(R.id.et_suggestions)
    EditText edtSuggestions;

    String ans1 = "";
    String ans2 = "";
    String ans3 = "";
    String ans4 = "";

    Alerts alerts;
    CustomProgressDialog pd;
    SharedPreference sharedPreference;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        ButterKnife.bind(this, view);
        sharedPreference = new SharedPreference(getActivity());
        init();

        return view;
    }

    private void init() {

        alerts = new Alerts(getActivity());
        pd = new CustomProgressDialog(getActivity());
        sharedPreference = new SharedPreference(getActivity());

        rbFood1.setOnClickListener(this);
        rbFood2.setOnClickListener(this);
        rbFood3.setOnClickListener(this);
        rbDelivery1.setOnClickListener(this);
        rbDelivery2.setOnClickListener(this);
        rbDelivery3.setOnClickListener(this);
        rbPacking1.setOnClickListener(this);
        rbPacking2.setOnClickListener(this);
        rbPacking3.setOnClickListener(this);
    }

    @OnClick(R.id.ic_menu)
    void openMenu() {
        MainActivity.drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    @OnClick(R.id.btn_send)
    void onButtonSend() {
        if (isAllValid()) {
            postFeedback(getJsonObject());
        }
    }

    private void postFeedback(JsonObject jsonObject) {
        pd.showpd("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).addFeedback(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            pd.hidepd();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                alerts.showSuccessAlerWithoutFinish("Thank you for the valuable feedback");
                                clearAll();

                            } else {
                                alerts.showErrorAlert(s.message);
                            }
                        },
                        e -> {
                            pd.hidepd();
                            alerts.showErrorAlert(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    private void clearAll() {
        rbFood2.setChecked(false);
        rbFood1.setChecked(false);
        rbFood3.setChecked(false);
        rbPacking1.setChecked(false);
        rbPacking2.setChecked(false);
        rbPacking3.setChecked(false);
        rbDelivery1.setChecked(false);
        rbDelivery2.setChecked(false);
        rbDelivery3.setChecked(false);
        rbFood1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        rbFood2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        rbFood3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        rbDelivery1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        rbDelivery2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        rbDelivery3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        rbPacking1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        rbPacking2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        rbDelivery3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
        edtSuggestions.setText("");

        ans1 = "";
        ans2 = "";
        ans3 = "";
        ans4 = "";
    }

    private JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        JsonArray array = new JsonArray();

        JsonObject jsonObject1 = new JsonObject();

        jsonObject1.addProperty("feedback_question_id", 1);
        jsonObject1.addProperty("answers", ans1);
        array.add(jsonObject1);

        jsonObject1 = new JsonObject();
        jsonObject1.addProperty("feedback_question_id", 2);
        jsonObject1.addProperty("answers", ans2);
        array.add(jsonObject1);

        jsonObject1 = new JsonObject();
        jsonObject1.addProperty("feedback_question_id", 3);
        jsonObject1.addProperty("answers", ans3);
        array.add(jsonObject1);

        if (!edtSuggestions.getText().toString().isEmpty()) {
            jsonObject1 = new JsonObject();
            jsonObject1.addProperty("feedback_question_id", 5);
            jsonObject1.addProperty("answers", edtSuggestions.getText().toString());
            array.add(jsonObject1);
        }

        JsonObject finalJson = new JsonObject();
        finalJson.add("feedbacks", array);

        return finalJson;

    }

    private boolean isAllValid() {
        if (ans1.equalsIgnoreCase("") || ans2.equalsIgnoreCase("") || ans3.equalsIgnoreCase("")) {
            alerts.showWarningAlert("Please provide your feedback to all the questions");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rb_food_1:
                rbFood2.setChecked(false);
                rbFood3.setChecked(false);
//                rbFood1.setChecked(true);
                ans1 = rbFood1.getText().toString();
                rbFood1.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbFood2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbFood3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;

            case R.id.rb_food_2:
//                rbFood2.setChecked(true);
                rbFood1.setChecked(false);
                rbFood3.setChecked(false);
                ans1 = rbFood2.getText().toString();
                rbFood2.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbFood1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbFood3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;

            case R.id.rb_food_3:
//                rbFood3.setChecked(true);
                rbFood1.setChecked(false);
                rbFood2.setChecked(false);
                ans1 = rbFood3.getText().toString();
                rbFood3.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbFood2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbFood1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;

            case R.id.rb_delivery_1:
                rbDelivery2.setChecked(false);
                rbDelivery3.setChecked(false);
                ans2 = rbDelivery1.getText().toString();
                rbDelivery1.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbDelivery2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbDelivery3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;

            case R.id.rb_delivery_2:
                rbDelivery1.setChecked(false);
                rbDelivery3.setChecked(false);
                ans2 = rbDelivery2.getText().toString();
                rbDelivery2.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbDelivery1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbDelivery3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;

            case R.id.rb_delivery_3:
                rbDelivery2.setChecked(false);
                rbDelivery1.setChecked(false);
                ans2 = rbDelivery3.getText().toString();
                rbDelivery3.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbDelivery2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbDelivery1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;

            case R.id.rb_packing_1:
                rbPacking2.setChecked(false);
                rbPacking3.setChecked(false);
                ans3 = rbPacking1.getText().toString();

                rbPacking1.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbPacking2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbPacking3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;

            case R.id.rb_packing_2:
                rbPacking1.setChecked(false);
                rbPacking3.setChecked(false);
                ans3 = rbPacking2.getText().toString();

                rbPacking2.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbPacking1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbPacking3.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;

            case R.id.rb_packing_3:
                rbPacking2.setChecked(false);
                rbPacking1.setChecked(false);
                ans3 = rbPacking3.getText().toString();

                rbPacking3.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_1));
                rbPacking2.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                rbPacking1.setTextColor(ContextCompat.getColor(getActivity(), R.color.title_color));
                break;
        }
    }
}
