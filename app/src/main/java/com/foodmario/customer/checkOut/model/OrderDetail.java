package com.foodmario.customer.checkOut.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetail implements Serializable{

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("user_id")
@Expose
public Integer userId;
@SerializedName("promo_id")
@Expose
public String promoId;

@SerializedName("promo_code")
@Expose
public String promoCode;
@SerializedName("delivery_address")
@Expose
public String deliveryAddress;
@SerializedName("date")
@Expose
public String date;
@SerializedName("delivery_lat")
@Expose
public String deliveryLat;
@SerializedName("delivery_long")
@Expose
public String deliveryLong;
@SerializedName("delivery_phone")
@Expose
public String deliveryPhone;
@SerializedName("delivery_date")
@Expose
public String deliveryDate;
@SerializedName("delivery_time")
@Expose
public String deliveryTime;
@SerializedName("note")
@Expose
public String note;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;
@SerializedName("total_price")
@Expose
public Integer totalPrice;

@SerializedName("delivery_charge")
@Expose
public Integer deliveryCharge;
@SerializedName("total_discount")
@Expose
public Integer totalDiscount;
@SerializedName("promotion_discount")
@Expose
public Integer promotionDiscount;
@SerializedName("final_price")
@Expose
public Integer finalPrice;
@SerializedName("foods")
@Expose
public List<Food> foods = null;

}