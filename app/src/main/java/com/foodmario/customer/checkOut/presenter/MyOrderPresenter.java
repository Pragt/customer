package com.foodmario.customer.checkOut.presenter;

import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.checkOut.view.IMyOrderView;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.google.gson.JsonObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/30/18.
 */

public class MyOrderPresenter implements IMyOrderPresenter {
    SharedPreference sharedPrefrences;
    IMyOrderView view;


    public MyOrderPresenter(SharedPreference sharedPreference, IMyOrderView view) {
        this.sharedPrefrences = sharedPreference;
        this.view = view;
        this.view.init();
    }

    @Override
    public void confirmOrder(JsonObject jsonObject) {
        view.onShowProgressDialog("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).confirmOrder(sharedPrefrences.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onSuccess(s.message);
                            }else if (s.statusCode == 606){
                                view.onConfirmDeleteUnavailableFromCart(s);
                            }
                            else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                           view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    @Override
    public void deleteCouponCode() {
        view.onShowProgressDialog("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).deleteCouponCode(sharedPrefrences.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), sharedPrefrences.getIntValues(CommonDef.SharedPrefrences.CART_ID))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onDeleteCouponCodeSuccess(s.results);
                            } else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                            view.onHideProgressDialog();
                           view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    @Override
    public void deleteUnavaiable(JsonObject jsonObject) {
        view.onShowProgressDialog("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).deleteUnavailable(sharedPrefrences.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            view.onHideProgressDialog();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onDeleteDeleteUnavailableCodeSuccess(s);
                            } else {
                                view.onError(s.message);
                            }
                        },
                        e -> {
                            view.onHideProgressDialog();
                            view.onError(CommonMethods.getErrorTye(e));
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
