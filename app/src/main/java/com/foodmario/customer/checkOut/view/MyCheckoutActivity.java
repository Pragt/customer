package com.foodmario.customer.checkOut.view;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.foodmario.customer.R;
import com.foodmario.customer.addCouponCode.CouponCodeDialog;
import com.foodmario.customer.checkOut.model.GetCartResponse;
import com.foodmario.customer.checkOut.model.OrderDetail;
import com.foodmario.customer.checkOut.presenter.MyOrderPresenter;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.view.MainFragment;
import com.google.gson.JsonObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyCheckoutActivity extends AppCompatActivity implements IMyOrderView {

    String delivery_location;
    String delivery_lat;
    String delivery_lng;

    @BindView(R.id.tv_grand_total)
    TextView tvGrandTotal;

    @BindView(R.id.btn_add_coupon_code)
    TextView btnAddCouponCode;

    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    @BindView(R.id.edt_note)
    EditText etNotes;

    @BindView(R.id.et_delivery_location)
    EditText etDeliveryLocation;

    @BindView(R.id.et_mobile)
    EditText etMobile;

    @BindView(R.id.et_delivery_address)
    EditText etDeliveryAddress;

    @BindView(R.id.ll_my_order)
    LinearLayout llMyOrder;

    @BindView(R.id.ll_empty)
    LinearLayout llEmpty;

    @BindView(R.id.txtDeliveryCharge)
    TextView txtDeliveryCharge;

    @BindView(R.id.cb_schedule)
    CheckBox cbSchedule;

    @BindView(R.id.cb_asap)
    CheckBox cbAsap;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.iv_delete_coupon)
    ImageView ivDeleteCouponCode;

    @BindView(R.id.tv_title)
    TextView tvTitle;


    MyOrderPresenter presenter;
    SharedPreference sharedPreference;
    private OrderDetail orderResponse;
    private String scheduleTime = "";
    Alerts alerts;
    CustomProgressDialog pd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        presenter = new MyOrderPresenter(sharedPreference, this);
        orderResponse = (OrderDetail) getIntent().getSerializableExtra(CommonDef.ORDER_DETAIL);
        onGetCart(orderResponse);
        llEmpty.setVisibility(View.GONE);
//        onHideProgressDialog();
        tvTitle.setText("My Order");
        swipeRefreshLayout.setEnabled(false);
        delivery_location = sharedPreference.getStringValues(CommonDef.DELIVERY_LOCATION);
        delivery_lat = sharedPreference.getStringValues(CommonDef.DELIVERY_LAT);
        delivery_lng = sharedPreference.getStringValues(CommonDef.DELIVERY_LON);
    }

    @Override
    public void init() {
        alerts = new Alerts(this);
        onAsapClicked();
        pd = new CustomProgressDialog(this);
        etMobile.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.MOBILE));

        Bitmap mbitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.img_map)).getBitmap();
        Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
        Canvas canvas = new Canvas(imageRounded);
        Paint mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 30, 30, mpaint);// Round Image Corner 100 100 100 100

    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        finish();
    }

    @Override
    public void onShowProgressDialog(String msg) {
        pd.showpd(msg);
    }

    @Override
    public void onHideProgressDialog() {
        swipeRefreshLayout.setRefreshing(false);
        pd.hidepd();
    }

    @Override
    public void onSuccess(String msg) {
//        alerts.showSuccessAlert(msg);
        llMyOrder.setVisibility(View.GONE);
        btnConfirm.setVisibility(View.GONE);
        etNotes.setText("");
        onAsapClicked();
        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.CART_ID, 0);
        MainFragment.resetCart();
        Opener.openThankyouScreen(this);
        finish();
    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }

    @OnClick(R.id.rl_order_detail)
    void onOrderDetailClicked() {
//        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
////
//        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
//
//        if (taskList.get(0).numActivities == 1) {
////                                Log.i(TAG, "This is last activity in the stack");
//        } else if (taskList.get(0).numActivities == 3) {
//            finish();
//        } else {
//            finish();
//        }
        MainFragment.gotoMyCart();
        setResult(CommonDef.GO_BACK);
        finish();
    }

    @OnClick(R.id.cb_asap)
    void onAsapClicked() {
        cbSchedule.setChecked(false);
        cbAsap.setClickable(false);
        cbSchedule.setText("Schedule an order");
        cbSchedule.setClickable(true);
        scheduleTime = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cbAsap.setButtonTintList(getResources().getColorStateList(R.color.cb_selected));
            cbAsap.setTextColor(getResources().getColorStateList(R.color.cb_selected));
            cbSchedule.setButtonTintList(getResources().getColorStateList(R.color.cb_unselected));
            cbSchedule.setTextColor(getResources().getColorStateList(R.color.cb_unselected));

            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR);
            int min = calendar.get(Calendar.MINUTE);

            scheduleTime = "";

        }
    }

    @OnClick(R.id.btn_add_coupon_code)
    void onAddCouponCodeAdd() {
        new CouponCodeDialog().getInstance(new CouponCodeDialog.AddCouponCodeListner() {
            @Override
            public void addCouponCodeListner(OrderDetail results) {
                onGetCart(results);
                if (results.promotionDiscount > 0)
                    alerts.showSuccessAlerWithoutFinish("Congratulations, You have got Rs. " + results.promotionDiscount + " discount!");
                else
                    alerts.showSuccessAlerWithoutFinish("Sorry, You have got Rs. " + results.promotionDiscount + " discount. Please try again later.");
//                ivDeleteCouponCode.setVisibility(View.VISIBLE);
            }
        }).show(getFragmentManager(), "");
    }

    @OnClick(R.id.iv_delete_coupon)
    void deleteCouponCode() {
        alerts.showDeleteConfirmationDialog("Are you sure to delete this coupon code?", new Alerts.OnConfirmationClickListener() {
            @Override
            public void onYesClicked() {
                presenter.deleteCouponCode();
            }

            @Override
            public void onNoClicked() {

            }
        }, 100);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick(R.id.cb_schedule)
    void onScheduleClicked() {
//        if (scheduleTime.isEmpty()) {
//            cbSchedule.setChecked(false);
//            cbSchedule.setButtonTintList(getResources().getColorStateList(R.color.cb_unselected));
//            cbSchedule.setTextColor(getResources().getColorStateList(R.color.cb_unselected));
//        }
//        cbSchedule.setClickable(false);
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String am_pm = "";

                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);

                Calendar minCalendar = Calendar.getInstance();
                minCalendar.set(Calendar.HOUR_OF_DAY, 7);
                minCalendar.set(Calendar.MINUTE, 0);

                Calendar maxCalendar = Calendar.getInstance();
                maxCalendar.set(Calendar.HOUR_OF_DAY, 19);
                maxCalendar.set(Calendar.MINUTE, 30);

                int hour = selectedHour % 12;
                if (hour == 0)
                    hour = 12;

                if (datetime.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis()) {
                    cbSchedule.setText(String.format("%02d:%02d %s", hour, selectedMinute,
                            selectedHour < 12 ? "AM" : "PM") + " scheduled");
                    scheduleTime = String.format("%02d:%02d", selectedHour, selectedMinute);
                    cbAsap.setChecked(false);
                    cbAsap.setClickable(true);
                    cbSchedule.setChecked(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        cbSchedule.setChecked(true);
                        cbSchedule.setButtonTintList(getResources().getColorStateList(R.color.cb_selected));
                        cbSchedule.setTextColor(getResources().getColorStateList(R.color.cb_selected));
                        cbAsap.setButtonTintList(getResources().getColorStateList(R.color.cb_unselected));
                        cbAsap.setTextColor(getResources().getColorStateList(R.color.cb_unselected));

                    }
                } else {
                    alerts.showErrorAlert("You cannot order this food today at this time. Please choose your appropriate time.");
//                    if (scheduleTime.equalsIgnoreCase("")) {
                    cbAsap.callOnClick();
                    cbAsap.setChecked(true);
                    cbSchedule.setChecked(false);
//                    }
                }
//                cbSchedule.setText(String.format("%02d:%02d %s", hour, selectedMinute,
//                        selectedHour < 12 ? "AM" : "PM") + " scheduled");
//                scheduleTime = String.format("%02d:%02d", selectedHour, selectedMinute);
            }
        }, hour, minute, false);
//        mTimePicker.setTitle("Select Time");

        mTimePicker.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    // Do Stuff
                    if (scheduleTime.isEmpty()) {
                        cbAsap.setChecked(true);
                    } else
                        cbSchedule.setChecked(true);
                }
            }
        });


        mTimePicker.show();

    }

    @OnClick(R.id.btn_confirm)
    void onConfirmButtonClicked() {
        if (etMobile.getText().toString().isEmpty()) {
            alerts.showWarningAlert("Please provide your mobile number.");
        } else if (!CommonMethods.isValidPhoneNumber(etMobile.getText().toString()))
            alerts.showWarningAlert("Please provide valid mobile number.");
        else {
            getJsonObject();
            presenter.confirmOrder(getJsonObject());
        }
    }

    private JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("cart_id", sharedPreference.getIntValues(CommonDef.SharedPrefrences.CART_ID));
        jsonObject.addProperty("mobile_number", etMobile.getText().toString());
        jsonObject.addProperty("latitude", delivery_lat);
        jsonObject.addProperty("longitude", delivery_lng);
        jsonObject.addProperty("notes", etNotes.getText().toString());
        jsonObject.addProperty("landmark", etDeliveryLocation.getText().toString());
        jsonObject.addProperty("device_type", 2);
        if (!scheduleTime.equalsIgnoreCase(""))
            jsonObject.addProperty("delivery_time", scheduleTime);
        jsonObject.addProperty("address", delivery_location);
        return jsonObject;
    }

    @Override
    public void onGetCart(OrderDetail results) {
        if (results.foods.size() > 0) {
            llMyOrder.setVisibility(View.VISIBLE);
            btnConfirm.setVisibility(View.VISIBLE);
//            ivDeleteCouponCode.setVisibility(View.GONE);
            tvGrandTotal.setText("Rs. " + (results.totalPrice + ""));
            if (results.promoCode != null && !results.promoCode.equalsIgnoreCase("")) {
                btnAddCouponCode.setText("   " + results.promoCode + "   ");
                btnAddCouponCode.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_coupon_code));
                ivDeleteCouponCode.setVisibility(View.VISIBLE);
            } else {
                btnAddCouponCode.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_empty_coupon));
                ivDeleteCouponCode.setVisibility(View.GONE);
            }
            etDeliveryAddress.setText(sharedPreference.getStringValues(CommonDef.DELIVERY_LOCATION));
        } else {
            llMyOrder.setVisibility(View.GONE);
            btnConfirm.setVisibility(View.GONE);

        }

        if (orderResponse.deliveryCharge == 0) {
            txtDeliveryCharge.setVisibility(View.GONE);
        } else {
            txtDeliveryCharge.setVisibility(View.VISIBLE);
            txtDeliveryCharge.setText("Including Rs." + orderResponse.deliveryCharge + " delivery charges");
        }

        orderResponse = results;
    }

    @Override
    public void onDeleteCouponCodeSuccess(OrderDetail results) {
        alerts.showToastMsg("Coupon code deleted successfully.");
        btnAddCouponCode.setText("   Apply coupon code   ");
        btnAddCouponCode.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_empty_coupon));
        ivDeleteCouponCode.setVisibility(View.GONE);
        onGetCart(results);
    }

    @Override
    public void onConfirmDeleteUnavailableFromCart(GetCartResponse response) {
        Intent intent = new Intent();
        intent.putExtra("data", response.results);
        intent.putExtra("message", response.message);
        setResult(RESULT_OK, intent);
        finish();

//        alerts.showDeleteConfirmationDialog(message, new Alerts.OnConfirmationClickListener() {
//            @Override
//            public void onYesClicked() {
//                JsonObject jsonObject = new JsonObject();
//                jsonObject.addProperty("cart_id", sharedPreference.getIntValues(CommonDef.SharedPrefrences.CART_ID));
//                jsonObject.addProperty("delivery_time", scheduleTime);
//                presenter.deleteUnavaiable(jsonObject);
//            }
//
//            @Override
//            public void onNoClicked() {
//
//            }
//        }, 100);

    }

    @Override
    public void onDeleteDeleteUnavailableCodeSuccess(GetCartResponse response) {
        Intent intent = new Intent();
        intent.putExtra("data", response.results);
        intent.putExtra("message", response.message);
        setResult(RESULT_OK, intent);
        finish();

//        onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
