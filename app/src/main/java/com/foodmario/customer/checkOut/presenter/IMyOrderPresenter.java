package com.foodmario.customer.checkOut.presenter;

import com.google.gson.JsonObject;

/**
 * Created by prajit on 1/30/18.
 */

public interface IMyOrderPresenter {

    void confirmOrder(JsonObject jsonObject);

    void deleteCouponCode();

    void deleteUnavaiable(JsonObject jsonObject);
}
