package com.foodmario.customer.checkOut.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCartResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public OrderDetail results;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}