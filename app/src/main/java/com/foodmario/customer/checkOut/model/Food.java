package com.foodmario.customer.checkOut.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Food implements Serializable {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("cart_id")
@Expose
public Integer cartId;
@SerializedName("food_id")
@Expose
public Integer foodId;
@SerializedName("qty")
@Expose
public Integer qty;
@SerializedName("max_quantity")
@Expose
public Integer maxQuantity;
@SerializedName("details")  
@Expose
public Details details;
@SerializedName("discount_amount")
@Expose
public Integer discountAmount;

}