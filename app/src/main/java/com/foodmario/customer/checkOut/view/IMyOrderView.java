package com.foodmario.customer.checkOut.view;

import com.foodmario.customer.checkOut.model.GetCartResponse;
import com.foodmario.customer.commonInterface.IcommonView;
import com.foodmario.customer.checkOut.model.OrderDetail;

/**
 * Created by prajit on 1/29/18.
 */

public interface IMyOrderView extends IcommonView {
    void onGetCart(OrderDetail results);

    void onDeleteCouponCodeSuccess(OrderDetail results);

    void onConfirmDeleteUnavailableFromCart(GetCartResponse message);

    void onDeleteDeleteUnavailableCodeSuccess(GetCartResponse message);

}
