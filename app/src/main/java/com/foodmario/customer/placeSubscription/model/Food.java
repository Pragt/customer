package com.foodmario.customer.placeSubscription.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Food {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("week_id")
@Expose
public Integer weekId;
@SerializedName("subscription_user_id")
@Expose
public Integer subscriptionUserId;
@SerializedName("food_id")
@Expose
public Integer foodId;
@SerializedName("qty")
@Expose
public Integer qty;
@SerializedName("delivery_time")
@Expose
public String deliveryTime;
@SerializedName("line_total")
@Expose
public String lineTotal;
@SerializedName("details")
@Expose
public Details details;

}