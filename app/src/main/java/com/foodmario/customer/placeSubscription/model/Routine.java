package com.foodmario.customer.placeSubscription.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Routine {

@SerializedName("week_id")
@Expose
public Integer weekId;
@SerializedName("foods")
@Expose
public List<Food> foods = null;

}