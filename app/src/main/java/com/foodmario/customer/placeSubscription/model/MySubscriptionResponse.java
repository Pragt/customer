package com.foodmario.customer.placeSubscription.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MySubscriptionResponse {

@SerializedName("message")
@Expose
public String message;
@SerializedName("results")
@Expose
public List<Result> results = null;
@SerializedName("statusCode")
@Expose
public Integer statusCode;

}