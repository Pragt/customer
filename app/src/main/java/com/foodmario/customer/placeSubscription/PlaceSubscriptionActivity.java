package com.foodmario.customer.placeSubscription;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.chooseFromMap.newAddressResponse.NewAddressToLatLng;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.home.Fragments.subscription.model.SubscriptionPackage;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.utils.GooglePlaceAutoCompleteTextView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PlaceSubscriptionActivity extends AppCompatActivity {

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_subscription)
    TextView tvSubscription;

    @BindView(R.id.et_delivery_address)
    GooglePlaceAutoCompleteTextView etAddress;

    @BindView(R.id.et_delivery_location)
    EditText etLocation;

    @BindView(R.id.edt_note)
    EditText edtNote;

    SubscriptionPackage subscriptions;
    Alerts alerts;
    SharedPreference sharedPreference;
    CustomProgressDialog pd;
    private Double latitude;
    private Double longitude;
    boolean isResume;
    int subsId;
    String startDate;
    String type = "";
    String subscriptionType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_subscription);
        CommonMethods.setupUI(findViewById(R.id.rl_place_subscription), this);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        sharedPreference = new SharedPreference(this);
        tvTitle.setText("Subscription");
        etAddress.setText(sharedPreference.getStringValues(CommonDef.DELIVERY_LOCATION));
        latitude = Double.parseDouble(sharedPreference.getStringValues(CommonDef.DELIVERY_LAT));
        longitude = Double.parseDouble(sharedPreference.getStringValues(CommonDef.DELIVERY_LON));
        etAddress.setHideMapIconMode(true);
        etAddress.init();

        alerts = new Alerts(this);
        pd = new CustomProgressDialog(this);
        if (getIntent().getExtras() != null) {
            isResume = getIntent().getBooleanExtra("is_resume", false);
        }

        if (isResume) {
            subsId = getIntent().getIntExtra("id", 0);
            type = getIntent().getStringExtra("type");
        } else {
            subscriptions = (SubscriptionPackage) getIntent().getExtras().getSerializable("subscriptions");
           type = subscriptions.type;
        }

        if (type.equalsIgnoreCase("monthly"))
            subscriptionType = "28 Days";
        else if (type.equalsIgnoreCase("biweekly"))
            subscriptionType = "14 Days ";
        else if (type.equalsIgnoreCase("weekly"))
            subscriptionType = "7 Days";

        tvSubscription.setText("You are currently subscribing for " + subscriptionType);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
//        Date c = Calendar.getInstance().getTime();
//        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(calendar.getTime());
        etDate.setText(formattedDate);

        etAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CommonMethods.hideSoftKeyboard(PlaceSubscriptionActivity.this);
                performSearch();
            }
        });

    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.et_date)
    void doSetDate() {
        DialogFragment newFragment = new SelectDateFragment(etDate);
        newFragment.show(getFragmentManager(), "DatePicker");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void placeSubscription() {
        pd.showpd("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).subscribe(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), getJsonObject())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            pd.hidepd();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                Opener.openThankyouSubscriptionScreen(this);
                                finish();

                            } else {
                                alerts.showErrorAlert(s.message);

                            }
                        },
                        e -> {
                            alerts.showErrorAlert(CommonMethods.getErrorTye(e));
                            pd.hidepd();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    @OnClick(R.id.btn_subscribe)
    void onSubscribeClicked() {
        startDate = etDate.getText().toString();
        if (etAddress.getText().toString().isEmpty() || latitude == null)
            alerts.showWarningAlert("Please provide your delivery location");
//        else if (etLocation.getText().toString().isEmpty())
//            alerts.showWarningAlert("Please enter Road Name/ Building Name or Office Name.");
        else if (!isResume) {
            subscriptions.start_date = etDate.getText().toString();
            placeSubscription();
        } else {
            resumeSubscription();
        }
    }

    private void resumeSubscription() {
        pd.showpd("Please wait...");
        ApiClient.getClient().create(ApiInterface.class).resume(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), getResumeJsonObject())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            pd.hidepd();
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                Opener.openThankyouSubscriptionScreen(this);
                                finish();

                            } else {
                                alerts.showErrorAlert(s.message);

                            }
                        },
                        e -> {
                            alerts.showErrorAlert(CommonMethods.getErrorTye(e));
                            pd.hidepd();
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    private JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("subscription_id", subscriptions.id);
        jsonObject.addProperty("sub_total", subscriptions.subTotal);
        jsonObject.addProperty("discount_total", subscriptions.totalDiscount);
        jsonObject.addProperty("start_date", subscriptions.start_date);
        jsonObject.addProperty("delivery_address", etAddress.getText().toString());
        jsonObject.addProperty("landmark", etLocation.getText().toString());
        jsonObject.addProperty("lat", latitude);
        jsonObject.addProperty("long", longitude);
        jsonObject.addProperty("notes", edtNote.getText().toString());
        JsonArray dayArray = new JsonArray();
        for (int i = 0; i < subscriptions.routine.size(); i++) {
            JsonObject day = new JsonObject();
            day.addProperty("week_id", subscriptions.routine.get(i).weekId);
            JsonArray routineArray = new JsonArray();
            for (int j = 0; j < subscriptions.routine.get(i).foods.size(); j++) {
                JsonObject food = new JsonObject();
                food.addProperty("food_id", subscriptions.routine.get(i).foods.get(j).foodId);
                food.addProperty("delivery_time", subscriptions.routine.get(i).foods.get(j).deliveryTime);
                food.addProperty("is_enabled", subscriptions.routine.get(i).foods.get(j).isEnabled);
                food.addProperty("qty", subscriptions.routine.get(i).foods.get(j).qty);
                routineArray.add(food);

            }
            day.add("foods", routineArray);
            dayArray.add(day);
        }
        jsonObject.add("routine", dayArray);

        return jsonObject;
    }

    private JsonObject getResumeJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("subscription_id", subsId);
        jsonObject.addProperty("start_date", startDate);
        jsonObject.addProperty("delivery_address", etAddress.getText().toString());
        jsonObject.addProperty("landmark", etLocation.getText().toString());
        jsonObject.addProperty("latitude", latitude);
        jsonObject.addProperty("longitude", longitude);
        jsonObject.addProperty("notes", edtNote.getText().toString());

        return jsonObject;
    }

    private void getGeocodeFromAddress(String address) throws UnsupportedEncodingException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<NewAddressToLatLng> addressToLatLng = apiService.googleAddressToLatlngNew(address);
        addressToLatLng.enqueue(new Callback<NewAddressToLatLng>() {
            @Override
            public void onResponse(Call<NewAddressToLatLng> call, Response<NewAddressToLatLng> response) {
                System.out.println("this is response " + response.body());
                String res = response.body().results.toString();
                if (response.isSuccessful()) {
                    if (response.body().status.equalsIgnoreCase("OK") && response.body().results.get(0).geometry.location.lat != null) {
                        latitude = Double.valueOf(response.body().results.get(0).geometry.location.lat.toString());
                        longitude = Double.valueOf(response.body().results.get(0).geometry.location.lng.toString());

                    } else {
                        alerts.showWarningDialog("Could not fetch data for the selected location. Please try different location.", new Alerts.OnConfirmationClickListener() {
                            @Override
                            public void onYesClicked() {
                                etLocation.setText("");
                            }

                            @Override
                            public void onNoClicked() {

                            }
                        }, 2);
                    }
                }
            }

            @Override
            public void onFailure(Call<NewAddressToLatLng> call, Throwable t) {
//                alerts.showErrorAlert("Could not fetch data for the selected location. Please try different location.");
            }
        });
    }

    private void performSearch() {
        if (!etAddress.getText().toString().isEmpty()) {
            if (CommonMethods.isConnectingToInternet(this)) {
                try {
                    getGeocodeFromAddress(etAddress.getText().toString().trim());

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                alerts.showErrorAlert(CommonDef.No_Connection);
                etLocation.setText("");
            }
        }
    }


}
