package com.foodmario.customer.placeSubscription.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("user_id")
@Expose
public Integer userId;
@SerializedName("food_name")
@Expose
public String foodName;
@SerializedName("slug")
@Expose
public String slug;
@SerializedName("price")
@Expose
public Integer price;
@SerializedName("description")
@Expose
public String description;
@SerializedName("max_unit_per_day")
@Expose
public Integer maxUnitPerDay;
@SerializedName("max_pre_time")
@Expose
public Integer maxPreTime;
@SerializedName("status")
@Expose
public String status;
@SerializedName("is_preference")
@Expose
public Integer isPreference;
@SerializedName("order")
@Expose
public Integer order;
@SerializedName("discount_status")
@Expose
public String discountStatus;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;
@SerializedName("is_featured")
@Expose
public String isFeatured;
@SerializedName("share_link")
@Expose
public String shareLink;

}