package com.foodmario.customer.placeSubscription.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

@SerializedName("id")
@Expose
public Integer id;
@SerializedName("user_id")
@Expose
public Integer userId;
@SerializedName("subscription_id")
@Expose
public Integer subscriptionId;
@SerializedName("delivery_lat")
@Expose
public Object deliveryLat;
@SerializedName("delivery_long")
@Expose
public Object deliveryLong;
@SerializedName("delivery_address")
@Expose
public Object deliveryAddress;
@SerializedName("delivery_address_1")
@Expose
public Object deliveryAddress1;
@SerializedName("delivery_address_2")
@Expose
public String deliveryAddress2;
@SerializedName("status")
@Expose
public String status;
@SerializedName("start_date")
@Expose
public String startDate;
@SerializedName("valid_from")
@Expose
public String validFrom;
@SerializedName("valid_to")
@Expose
public String validTo;
@SerializedName("is_approved")
@Expose
public String isApproved;
@SerializedName("sub_total")
@Expose
public Integer subTotal;
@SerializedName("discount_total")
@Expose
public String discountTotal;
@SerializedName("extra_info")
@Expose
public String extraInfo;
@SerializedName("created_at")
@Expose
public String createdAt;
@SerializedName("updated_at")
@Expose
public String updatedAt;
@SerializedName("name")
@Expose
public String name;
@SerializedName("image_path")
@Expose
public String imagePath;
@SerializedName("type")
@Expose
public String type;
@SerializedName("discount")
@Expose
public Discount discount;
@SerializedName("discount_type")
@Expose
public String discountType;
@SerializedName("has_expired")
@Expose
public Boolean hasExpired;
@SerializedName("routine")
@Expose
public List<Routine> routine = null;

}