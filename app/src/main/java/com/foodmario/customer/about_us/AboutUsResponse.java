package com.foodmario.customer.about_us;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutUsResponse {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;


        public class Data {

            @SerializedName("About_us")
            @Expose
            public String aboutUs;

    }
}