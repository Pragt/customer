package com.foodmario.customer;

import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foodmario.customer.about_us.AboutUsFragment;
import com.foodmario.customer.faq.FaqFragment;
import com.foodmario.customer.feedback.FeedbackFragment;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.view.MainFragment;
import com.foodmario.customer.mySubscriptions.MySubscriptionFragment;
import com.foodmario.customer.orderHistory.view.OrderHistoryFragmentNew;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.termsAndconditions.TermsAndConditionFragment;
import com.foodmario.customer.updatePreferences.view.UpdateFoodPreferencesFragment;
import com.foodmario.customer.utils.LoadImage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private boolean doubleBackToExitPressedOnce = false;
    private FragmentTransaction fragmentTransaction;
    public static DrawerLayout drawerLayout;
    SharedPreference sharedPreference;
    Alerts alerts;

    @BindView(R.id.ll_log_out)
    LinearLayout llLogOut;
    private TextView tvName;
    private ImageView profileImage;
    private Geocoder geocoder;
    boolean isPopUpShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        sharedPreference = new SharedPreference(this);
        navigationView.setItemIconTintList(null);
        navigationView.getHeaderView(0).findViewById(R.id.tv_view_profile).setOnClickListener(v -> openProfileActivity());
        tvName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_name);
        profileImage = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.iv_profile_img);
        tvName.setOnClickListener(v -> openProfileActivity());
        profileImage.setOnClickListener(v -> openProfileActivity());
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        alerts = new Alerts(this);
        sharedPreference = new SharedPreference(this);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getBoolean(CommonDef.GO_TO_SUBSCRIPTION)) {
                MySubscriptionFragment privacy_policy = new MySubscriptionFragment();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, privacy_policy);
                fragmentTransaction.commit();
                navigationView.getMenu().getItem(2).setChecked(true);
            } else if (getIntent().getExtras().getBoolean(CommonDef.GO_TO_MY_ORDER)) {
                OrderHistoryFragmentNew privacy_policy = new OrderHistoryFragmentNew();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, privacy_policy);
                fragmentTransaction.commit();
                navigationView.getMenu().getItem(1).setChecked(true);
            } else {
                openHomePage();
            }
        } else {
            openHomePage();
        }
    }

    private void openHomePage() {
        navigationView.getMenu().getItem(0).setChecked(true);
        MainFragment homeFragment = new MainFragment();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, homeFragment);

        fragmentTransaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.faq:
                if (!item.isChecked()) {
                    FaqFragment faqFragment = new FaqFragment();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, faqFragment);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.my_home:
                if (!item.isChecked()) {
                    openHomePage();
                }
                break;

            case R.id.feedback:
                if (!item.isChecked()) {
                    FeedbackFragment feedbackFragment = new FeedbackFragment();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, feedbackFragment);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.my_preferences:
                if (!item.isChecked()) {
                    UpdateFoodPreferencesFragment foodPreferencesFragment = new UpdateFoodPreferencesFragment();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, foodPreferencesFragment);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.be_a_cook:
//                try {
//                    Intent appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.foodmario.vendor"));
//                    appStoreIntent.setPackage("com.android.vending");
//
//                    startActivity(appStoreIntent);
//                } catch (android.content.ActivityNotFoundException exception) {
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.foodmario.vendor")));
//                }

                CommonMethods.gotoVendorApp(this);

                break;

            case R.id.my_subscription:
                if (!item.isChecked()) {
                    MySubscriptionFragment privacy_policy = new MySubscriptionFragment();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, privacy_policy);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.my_order:
                if (!item.isChecked()) {
                    OrderHistoryFragmentNew privacy_policy = new OrderHistoryFragmentNew();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, privacy_policy);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.terms:
                if (!item.isChecked()) {
                    TermsAndConditionFragment termsFragment = new TermsAndConditionFragment();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, termsFragment);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.about_us:
                if (!item.isChecked()) {
                    AboutUsFragment aboutUsFragment = new AboutUsFragment();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, aboutUsFragment);
                    fragmentTransaction.commit();
                }
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (!navigationView.getMenu().getItem(0).isChecked()) {
            navigationView.getMenu().getItem(0).setChecked(true);
            openHomePage();

        } else {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please Press BACK again to exit !", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.ll_log_out)
    void onLogoutClicked() {
        alerts.showLogOutConfirmationDialog("Do you wish to Log out?", new Alerts.OnConfirmationClickListener() {
            @Override
            public void onYesClicked() {
                doLogOut();
                sharedPreference.clearData();
                Opener.LoginActivity(MainActivity.this);
                finishAffinity();
            }

            @Override
            public void onNoClicked() {

            }
        }, 100);
    }

    private void doLogOut() {
        ApiClient.getClient().create(ApiInterface.class).logOut(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), CommonMethods.getDeviceId(this))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(s -> {
                            if (s.statusCode == 200) {
                                //do nothing
                            } else {
                                //do nothing
                            }
                        },
                        e -> {

                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }


    void openProfileActivity() {
        Opener.openProfileActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvName.setText(sharedPreference.getStringValues(CommonDef.SharedPrefrences.FULL_NAME));
        LoadImage.loadCircularUserImage(this, profileImage, UrlHelpers.USER_BASE_URL + sharedPreference.getStringValues(CommonDef.SharedPrefrences.PROFILE_PIC), R.drawable.ic_man_1, R.drawable.ic_man_1);
        checkLoggedIn();
    }

    private void checkLoggedIn() {
        ApiClient.getClient().create(ApiInterface.class).checkUser(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (s.statusCode == 200) {
                                //do nothing
                            } else {
                                doLogOut();
                                sharedPreference.clearData();
                                Opener.LoginActivity(MainActivity.this);
                                finishAffinity();
                            }
                        },
                        e -> {

                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            onNavigationItemSelected(navigationView.getMenu().getItem(0));
        }
    }

    public void loadHomePage() {
        openHomePage();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
