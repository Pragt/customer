package com.foodmario.customer.completeProfile.presenter;

import com.foodmario.customer.completeProfile.view.ICompleteProfileView;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.register.model.RegisterObject;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/19/18.
 */

public class CompleteYourProfilePresenter implements ICompleteProfilePresenter {
    SharedPreference sharedPreference;
    ICompleteProfileView view;

    public CompleteYourProfilePresenter(SharedPreference sharedPreference, ICompleteProfileView view)
    {
        this.sharedPreference = sharedPreference;
        this.view = view;
    }


    @Override
    public void doCompleteProfile(RegisterObject object, String deviceId) {
        if (isAllValid(object))
        {
            view.onShowProgressDialog("Signing Up... Please wait");

            RequestBody fullName = RequestBody.create(MediaType.parse("multipart/form-data"), object.fullName);
            RequestBody email = RequestBody.create(MediaType.parse("multipart/form-data"), object.email);
            RequestBody location = RequestBody.create(MediaType.parse("multipart/form-data"), object.location);
            RequestBody mobile_number= RequestBody.create(MediaType.parse("multipart/form-data"), object.phoneNo);
            RequestBody fbToken = RequestBody.create(MediaType.parse("multipart/form-data"), object.accessToken);
            RequestBody latitude = RequestBody.create(MediaType.parse("multipart/form-data"), object.latitude);
            RequestBody longitude = RequestBody.create(MediaType.parse("multipart/form-data"), object.longitude);
            RequestBody user_type = RequestBody.create(MediaType.parse("multipart/form-data"), object.userType);
            RequestBody device_id = RequestBody.create(MediaType.parse("multipart/form-data"), deviceId);
            RequestBody device_type = RequestBody.create(MediaType.parse("multipart/form-data"), "2");
            RequestBody firebaseToken = RequestBody.create(MediaType.parse("multipart/form-data"), object.firebaseToken != null? object.firebaseToken: "");
            RequestBody fbImage = RequestBody.create(MediaType.parse("multipart/form-data"), object.fbImage);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            MultipartBody.Part bodyProfilePic = null;
            if(object.image  != null && !object.image.equalsIgnoreCase(""))
            {
                File profile_pic = new File(object.image);

                RequestBody requestProfileFile = RequestBody.create(MediaType.parse("image/jpeg"), profile_pic);

                bodyProfilePic = MultipartBody.Part.createFormData("profile_pic", profile_pic.getName(), requestProfileFile);
            }

            ApiClient.getClient().create(ApiInterface.class).doCompleteYourProfile(fullName, email, mobile_number, location, fbToken, bodyProfilePic, fbImage, latitude, longitude, user_type, device_id, device_type, firebaseToken)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                                view.onHideProgressDialog();
                                if (s.code == CommonDef.Response.SUCCESS) {
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.AUTH_KEY, "Bearer " + s.data.authenticationKey);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.IS_LOGGED_IN, true);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.data.name);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.EMAIL, s.data.email);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.MOBILE, s.data.mobileNumber);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.ADDRESS, s.data.location);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LATITUDE, s.data.latitude);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.LONGITUDE, s.data.longitude);
                                    if (s.data.userDescription != null)
                                        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.User_desc, s.data.userDescription);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.FULL_NAME, s.data.name);
                                    if (s.data.profilePic != null)
                                        sharedPreference.setKeyValues(CommonDef.SharedPrefrences.PROFILE_PIC, s.data.profilePic);
                                    sharedPreference.setKeyValues(CommonDef.SharedPrefrences.HAS_CHOOSED_FOOD_PREFERENCES, s.data.hasChoosedFoodPreferences);
                                    view.onFbLoginSuccess(s.message, s.data.hasChoosedFoodPreferences);


                                } else {
                                    view.onError(s.message);

                                }
                            },
                            e -> {
                               view.onError(CommonMethods.getErrorTye(e));
                                view.onHideProgressDialog();
                                e.printStackTrace();
                            },
                            () -> System.out.println("supervisor list"));
        }

    }

    private boolean isAllValid(RegisterObject object) {
        if (object.fullName.isEmpty()) {
            view.onValidationError("Please Enter Full Name");
            return false;
        }
        else if (object.email.isEmpty()) {
            view.onValidationError("Please Enter Your Email");
            return false;
        }
        else if (!CommonMethods.isValidEmail(object.email)) {
            view.onValidationError("Please Enter Valid Email");
            return false;
        }
        else if (object.phoneNo.isEmpty()) {
            view.onValidationError("Please Enter Your Mobile Number");
            return false;
        }
        else if (object.phoneNo.length() < 10) {
            view.onValidationError("Please Enter Valid Mobile Number");
            return false;
        }
        return true;
    }
}
