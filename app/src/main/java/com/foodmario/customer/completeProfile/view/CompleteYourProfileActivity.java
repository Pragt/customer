package com.foodmario.customer.completeProfile.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.foodmario.customer.R;
import com.foodmario.customer.chooseFromMap.newAddressResponse.NewAddressToLatLng;
import com.foodmario.customer.completeProfile.presenter.CompleteYourProfilePresenter;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.CustomProgressDialog;
import com.foodmario.customer.helpers.ImagePicker;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.register.model.RegisterObject;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.foodmario.customer.utils.GooglePlaceAutoCompleteTextView;
import com.foodmario.customer.utils.LoadImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CompleteYourProfileActivity extends AppCompatActivity implements ICompleteProfileView {

    @BindView(R.id.iv_profile_pic)
    ImageView ivProfilePic;

    @BindView(R.id.et_fullname)
    EditText etFullname;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.fl_profile_pic)
    FrameLayout flProfilePic;

    @BindView(R.id.et_mobile)
    EditText etMobile;

    @BindView(R.id.iv_camera)
    ImageView ivCamera;

    @BindView(R.id.ll_crop_image)
    LinearLayout llCropImage;

    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @BindView(R.id.CropImageView)
    CropImageView mCropImageView;
    private String accessToken;


    @BindView(R.id.et_location)
    GooglePlaceAutoCompleteTextView etLocation;
    private Alerts alerts;
    SharedPreference sharedPreference;
    private CustomProgressDialog pd;
    private Double latitude;
    private Double longitude;
    private Uri imageUri;
    private String imagePath;
    CompleteYourProfilePresenter presenter;
    private String fbImage;
    private String firebaseToken;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_your_profile);
        CommonMethods.setupUI(findViewById(R.id.ll_complete_profile), this);
        ButterKnife.bind(this);

        sharedPreference = new SharedPreference(this);
        presenter = new CompleteYourProfilePresenter(sharedPreference, this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            flProfilePic.setElevation(10);

        if (getIntent().getExtras() != null) {
            etFullname.setText(getIntent().getExtras().getString("name"));
            etEmail.setText(getIntent().getExtras().getString("email"));
            accessToken = getIntent().getExtras().getString("fb_token");

            fbImage = getIntent().getExtras().getString("fb_image");
            firebaseToken = getIntent().getExtras().getString("firebase_token");

            if (fbImage != null && !fbImage.equalsIgnoreCase("")) {
                LoadImage.loadCircular(CompleteYourProfileActivity.this, ivProfilePic, fbImage, R.drawable.choose_pic, R.drawable.choose_pic);
                ivCamera.setVisibility(View.GONE);
            }
        }
        init();
    }

    @Override
    public void init() {
        alerts = new Alerts(this);
        pd = new CustomProgressDialog(this);
        etLocation.init();

        etLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CommonMethods.hideSoftKeyboard(CompleteYourProfileActivity.this);
                performSearch();
            }
        });
    }

    @Override
    public void onShowProgressDialog(String msg) {
        pd.showpd(msg);
    }

    @Override
    public void onHideProgressDialog() {
        pd.hidepd();
    }

    @Override
    public void onSuccess(String msg) {
        alerts.showSuccessAlerWithoutFinish(msg);
    }

    @Override
    public void onError(String msg) {
        alerts.showErrorAlert(msg);
    }

    @Override
    public void onValidationError(String msg) {
        alerts.showWarningAlert(msg);
    }

    private void performSearch() {
        if (!etLocation.getText().toString().isEmpty()) {
            if (CommonMethods.isConnectingToInternet(this)) {
                try {
                    getGeocodeFromAddress(etLocation.getText().toString().trim());

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                alerts.showErrorAlert(CommonDef.No_Connection);
                etLocation.setText("");
            }
        }
    }

    private void getGeocodeFromAddress(String address) throws UnsupportedEncodingException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<NewAddressToLatLng> addressToLatLng = apiService.googleAddressToLatlngNew(address);
        addressToLatLng.enqueue(new Callback<NewAddressToLatLng>() {
            @Override
            public void onResponse(Call<NewAddressToLatLng> call, Response<NewAddressToLatLng> response) {
                System.out.println("this is response " + response.body());
                String res = response.body().results.toString();
                if (response.isSuccessful()) {
                    if (response.body().status.equalsIgnoreCase("OK") && response.body().results.get(0).geometry.location.lat != null) {
                        latitude = Double.valueOf(response.body().results.get(0).geometry.location.lat.toString());
                        longitude = Double.valueOf(response.body().results.get(0).geometry.location.lng.toString());

                    } else {
                        alerts.showWarningDialog("Could not fetch data for the selected location. Please try different location.", new Alerts.OnConfirmationClickListener() {
                            @Override
                            public void onYesClicked() {
                                etLocation.setText("");
                            }

                            @Override
                            public void onNoClicked() {

                            }
                        }, 2);
                    }
                }
            }

            @Override
            public void onFailure(Call<NewAddressToLatLng> call, Throwable t) {
//                alerts.showErrorAlert("Could not fetch data for the selected location. Please try different location.");
            }
        });
    }

    @OnClick(R.id.rl_proceed)
    public void onProceed() {
        RegisterObject object = new RegisterObject();
        object.fullName = etFullname.getText().toString();
        object.email = etEmail.getText().toString();
        object.password = "";
        object.accessToken = accessToken;
        object.location = etLocation.getText().toString();
        object.phoneNo = etMobile.getText().toString();
        object.image = imagePath;
        object.latitude = String.valueOf(latitude);
        object.longitude = String.valueOf(longitude);
        object.fbImage = fbImage;
        object.firebaseToken = firebaseToken;
        object.userType = "0";

        presenter.doCompleteProfile(object, CommonMethods.getDeviceId(CompleteYourProfileActivity.this));
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.iv_profile_pic)
    void onChooseImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CommonDef.REQUEST_STORAGE_CAMERA);
                return;
            }
        }
        startActivityForResult(ImagePicker.getPickImageChooserIntent(this), 200);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(CommonDef.TAG + requestCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 200:
                    // From Gallery
                    if (data != null && data.getData() != null)
                        imageUri = data.getData();
                    else // From camera
                        imageUri = ImagePicker.getPickImageResultUri();
                    boolean requirePermissions = false;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                            checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                            ImagePicker.isUriRequiresPermissions(this, imageUri)) {

                        // request permissions and handle t fhe result in onRequestPermissionsResult()
                        requirePermissions = true;
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                    }


                    if (!requirePermissions) {
                        scrollView.setVisibility(View.GONE);
                        llCropImage.setVisibility(View.VISIBLE);
                        mCropImageView.setAspectRatio(1, 1);
                        mCropImageView.setImageUriAsync(imageUri);
                    }

                    break;
            }
        }
    }

    @OnClick(R.id.btn_crop)
    void onCropClicked() {
        Bitmap cropped = mCropImageView.getCroppedImage(500, 500);
        mCropImageView.setImageBitmap(null);
        scrollView.setVisibility(View.VISIBLE);
        llCropImage.setVisibility(View.GONE);
        new BitmapWorkerTask().execute(cropped);
    }

    @OnClick(R.id.btn_new_image)
    void onNewImageClicked() {
        onChooseImage();
    }

    @Override
    public void onFbLoginSuccess(String message, boolean hasChoosedFoodPreferences) {
//        Opener.openSetupServingHoursActivity(this, );
        alerts.showToastMsg(message);
        if (!hasChoosedFoodPreferences)
            Opener.openChooseFoodPreference(this, true);
        else
            Opener.openHomeActivity(this);
        finish();
    }

    class BitmapWorkerTask extends AsyncTask<Bitmap, Void, String> {
        private Bitmap data = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.showpd("saving...");
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            data = params[0];
            return saveBitmap(data);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(String fileLoc) {
            imagePath = fileLoc;
            if (imagePath != null) {
                LoadImage.loadCircular(CompleteYourProfileActivity.this, ivProfilePic, imagePath, R.drawable.choose_pic, R.drawable.choose_pic);
                ivCamera.setVisibility(View.GONE);
            }
            pd.hidepd();
        }
    }

    public String saveBitmap(Bitmap bmp) {
        FileOutputStream out = null;
        File file = getExternalCacheDir();
        file.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file1 = new File(file, fname);
        if (file1.exists()) file1.delete();

        try {
            out = new FileOutputStream(file1);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file1.getPath();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissions.length > 0)
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                startActivityForResult(ImagePicker.getPickImageChooserIntent(this), 200);
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivityForResult(ImagePicker.getGallaryImageChooserIntent(this), 200);
            } else {
                alerts.showToastMsg("Permission denied");
            }
    }
}
