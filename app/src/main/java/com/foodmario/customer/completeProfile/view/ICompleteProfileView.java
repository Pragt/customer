package com.foodmario.customer.completeProfile.view;

import com.foodmario.customer.commonInterface.IcommonView;

/**
 * Created by prajit on 1/19/18.
 */

public interface ICompleteProfileView extends IcommonView{
    void onFbLoginSuccess(String message, boolean hasChoosedFoodPreferences);
}
