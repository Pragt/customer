package com.foodmario.customer.completeProfile.presenter;

import com.foodmario.customer.register.model.RegisterObject;

/**
 * Created by prajit on 1/19/18.
 */

public interface ICompleteProfilePresenter {

    void doCompleteProfile(RegisterObject object, String deviceId);
}
