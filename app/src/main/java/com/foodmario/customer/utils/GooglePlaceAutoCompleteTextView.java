package com.foodmario.customer.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.foodmario.customer.R;
import com.foodmario.customer.customViews.DrawableClickListener;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.register.model.GoogleAutocompleteResponse;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Prajeet on 3/6/2017.
 */

public class GooglePlaceAutoCompleteTextView extends AutoCompleteTextView {
    private ArrayAdapter<String> adapterLocations;
    String[] validWords = new String[]{};
    private AutoCompleteTextView tv;
    private Context context;
    private ArrayList<String> suggestionList = new ArrayList<>();
    private boolean isIconVisible = true;
    private boolean hideMap = false;
    boolean isChooseFromMap = false;
    public boolean isPopulateAddress = true;

    public GooglePlaceAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        tv = this;
    }

    public GooglePlaceAutoCompleteTextView(Context context) {
        super(context);
        this.context = context;
        tv = this;
        tv.setThreshold(1);
    }

    public void removeICon() {
        isIconVisible = false;
        tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location, 0, 0, 0);
    }


    public void showIcon() {
        isIconVisible = true;
    }

    public void setHideMapIconMode(boolean hideMap) {
        this.hideMap = hideMap;
    }

    public void isChooseFromMap(boolean isFromMap) {
        isChooseFromMap = isFromMap;
    }


    class Validator implements AutoCompleteTextView.Validator {

        @Override
        public boolean isValid(CharSequence text) {
            Log.v("Test", "Checking if valid: " + text);
//            Arrays.sort(validWords);
//            if (Arrays.binarySearch(validWords, text.toString()) > 0) {
//                return true;
//            }
            for (String s : suggestionList)
                if (s.trim().equalsIgnoreCase(text.toString())) {
                    return true;
                }

            return false;
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            Log.v("Test", "Returning fixed text");

            /* I'm just returning an empty string here, so the field will be blanked,
             * but you could put any kind of action here, like popping up a dialog?
             *
             * Whatever value you return here must be in the list of valid words.
             */
            return "";
        }
    }

    class FocusListener implements View.OnFocusChangeListener {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            Log.v("Test", "Focus changed");
            if (v.getId() == R.id.et_location && !hasFocus) {
                Log.v("Test", "Performing validation");
                ((AutoCompleteTextView) v).performValidation();
            }
        }
    }

    public void init() {
        isIconVisible = true;
        RxTextView.textChanges(tv)
                .filter(s -> s.length() > 0)
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.io())
                .switchMap(s -> ApiClient.getClient().create(ApiInterface.class).getplaces(getResources().getString(R.string.GOOGLE_API), s.toString(), CommonDef.ZONE))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                            System.out.println("googleAutocompleteResponse " + r.toString());
                            validWords = GoogleAutocompleteResponse.getStringList(r);
                            suggestionList.addAll(new ArrayList<>(Arrays.asList(validWords)));
                            if (isPopulateAddress) {
                                adapterLocations = new ArrayAdapter<String>(context, R.layout.multiline_dropdown_item, validWords);
                                tv.setAdapter(adapterLocations);
                                adapterLocations.notifyDataSetChanged();
                            }

                        },
                        e -> {
                            e.printStackTrace();
                        },
                        () -> {
                        });
//        tv.setAdapter(new GooglePlacesAutocompleteAdapter(context, R.layout.multiline_dropdown_item));

        RxTextView.textChanges(tv).subscribe(charSequence -> {
            if (charSequence.length() > 0) {
                if (hideMap)
                    if (isChooseFromMap)
                        tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cross_icon_white, 0);
                    else
                        tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.search_cross_icon, 0);
                else if (isChooseFromMap)
                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location, 0, R.drawable.cross_icon_white, 0);
                else
                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location, 0, R.drawable.search_cross_icon, 0);
                tv.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(this) {
                    @Override
                    public boolean onDrawableClick() {
                        // TODO : insert code to perform on clicking of the RIGHT drawable image...
                        if (isIconVisible)
                            tv.setText("");
                        return true;
                    }
                });
            } else {
                if (hideMap)
                    tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                else
                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location, 0, 0, 0);
            }
        });


        tv.setValidator(new Validator());
        tv.setOnFocusChangeListener(new FocusListener());

    }
}
