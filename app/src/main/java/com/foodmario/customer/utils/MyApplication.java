package com.foodmario.customer.utils;

import android.content.Context;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.foodmario.customer.BuildConfig;
import com.foodmario.customer.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by prajit on 9/9/16.
 */
public class MyApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Cabin-Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        MultiDex.install(this);

        if (BuildConfig.DEBUG && Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectNonSdkApiUsage()
                    .penaltyLog()
                    .build());
        }
    }

    public static Context getMyApplicationContext()
    {
        return getMyApplicationContext();
    }
    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
    }
}