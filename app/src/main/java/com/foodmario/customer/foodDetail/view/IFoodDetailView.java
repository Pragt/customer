package com.foodmario.customer.foodDetail.view;

import com.foodmario.customer.commonInterface.IcommonView;

/**
 * Created by prajit on 1/28/18.
 */

public interface IFoodDetailView extends IcommonView {
    void onFoodReactSuccess(Boolean isLike, int likeCount);

    void onFoodReactError(String message, Boolean isLiked, int oldLikeCount);
}
