package com.foodmario.customer.foodDetail.presenter;

import com.foodmario.customer.foodDetail.view.IFoodDetailView;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.retrofit.ApiClient;
import com.foodmario.customer.retrofit.ApiInterface;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by prajit on 1/28/18.
 */

public class FoodDetailPresenter {
    SharedPreference sharedPreference;
    IFoodDetailView view;

    public FoodDetailPresenter(SharedPreference sharedPreference, IFoodDetailView view){
        this.sharedPreference = sharedPreference;
        this.view = view;
    }


    public void setLiked(Integer food_id , Boolean isLiked, int oldLikeCount) {
        ApiClient.getClient().create(ApiInterface.class).toggleLike(sharedPreference.getStringValues(CommonDef.SharedPrefrences.AUTH_KEY), food_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            if (s.statusCode == CommonDef.Response.SUCCESS) {
                                view.onFoodReactSuccess(s.results.isLike, s.results.likeCount);
                            } else {
//                                view.onError(s.message);
                                view.onFoodReactError(s.message, isLiked, oldLikeCount);
                            }
                        },
                        e -> {
//                           view.onError(CommonMethods.getErrorTye(e));
                            view.onFoodReactError(CommonMethods.getErrorTye(e), isLiked,oldLikeCount);
                            e.printStackTrace();
                        },
                        () -> System.out.println("supervisor list"));
    }
}
