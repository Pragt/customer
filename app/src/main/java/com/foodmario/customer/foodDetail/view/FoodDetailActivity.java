package com.foodmario.customer.foodDetail.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.foodmario.customer.R;
import com.foodmario.customer.addToCartDialog.MyBottomSheetDialogFragment;
import com.foodmario.customer.foodDetail.presenter.FoodDetailPresenter;
import com.foodmario.customer.helpers.Alerts;
import com.foodmario.customer.helpers.CommonDef;
import com.foodmario.customer.helpers.CommonMethods;
import com.foodmario.customer.helpers.Opener;
import com.foodmario.customer.helpers.SharedPreference;
import com.foodmario.customer.helpers.UrlHelpers;
import com.foodmario.customer.home.Fragments.home.adapter.ImageAdapter;
import com.foodmario.customer.utils.LoadImage;
import com.foodmario.customer.vendorProfile.model.Food;
import com.foodmario.customer.vendorProfile.model.Vendor;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FoodDetailActivity extends AppCompatActivity implements IFoodDetailView {

    @BindView(R.id.tv_address)
    TextView tvAddress;

    @BindView(R.id.tv_distance)
    TextView tvDistance;

    @BindView(R.id.tv_like_count)
    TextView tvLikeCount;

    @BindView(R.id.tv_comment_count)
    TextView tvCommentCount;

    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_description)
    TextView tvDescription;


    @BindView(R.id.iv_profile_img)
    ImageView ivImage;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.slider_dots)
    LinearLayout llDots;

    @BindView(R.id.ll_like)
    LinearLayout llLike;

    @BindView(R.id.ll_comment)
    LinearLayout llComment;

    @BindView(R.id.ll_share)
    LinearLayout llShare;

    @BindView(R.id.iv_likes)
    ImageView ivLikes;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.iv_cart)
    Button ivCart;

    @BindView(R.id.tv_final_price)
    TextView tvFinalPrice;

    @BindView(R.id.tv_price)
    TextView tvPrice;

    Food food;
    Vendor vendor;

    Alerts alerts;
    SharedPreference sharedPreference;

    FoodDetailPresenter foodDetailPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreference(this);
        foodDetailPresenter = new FoodDetailPresenter(sharedPreference, this);
        tvTitle.setText("Food Detail");
        food = (Food) getIntent().getSerializableExtra(CommonDef.FOOD_DETAIL);
        vendor = (Vendor) getIntent().getSerializableExtra(CommonDef.VENDOR_PROFILE);
        loadFoodDetail();
    }

    private void loadFoodDetail() {
        final ImageAdapter mAdapter = new ImageAdapter((Activity) this, food.dishImages);
        viewPager.setAdapter(mAdapter);

        final int dotscount;
        final ImageView[] dots;

        dotscount = mAdapter.getCount();
        dots = new ImageView[dotscount];
        llDots.removeAllViews();

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_unselected_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            llDots.addView(dots[i], params);

        }

        if (dots.length > 0)
            dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_selected_dot));
        if (dots.length <= 1) {
            llDots.setVisibility(View.GONE);
        } else
            llDots.setVisibility(View.VISIBLE);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_unselected_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_selected_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        LoadImage.loadCircularUserImage((Activity) this, ivImage, UrlHelpers.USER_BASE_URL + vendor.userImage, R.drawable.ic_man_1, R.drawable.ic_man_1);
        tvName.setText(vendor.name);
        tvAddress.setText(vendor.userAddress);
        tvLikeCount.setText(food.likeCount + "");
        tvCommentCount.setText(food.commentCount + "");
        tvFoodName.setText(food.foodName);
        tvDistance.setText(CommonMethods.calculateMinutesAndHour(Integer.parseInt(food.maxPreTime)));
        tvDescription.setMaxLines(100);
        tvDescription.setText(food.description);

        tvFinalPrice.setText("Rs. " + food.finalPrice);
        tvPrice.setText("Rs. " + food.price);
        tvPrice.setPaintFlags(tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (String.valueOf(food.finalPrice).equalsIgnoreCase(String.valueOf(food.price))) {
            tvPrice.setVisibility(View.GONE);
        } else
            tvPrice.setVisibility(View.VISIBLE);

        llLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLikeClicked(food.id);
            }
        });

        llComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCommentClicked(food.id);
            }
        });

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShareClicked(food.shareLink);
            }
        });

        if (food.isLiked)
            ivLikes.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_heart_red));
        else
            ivLikes.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_heart));

    }

    private void onShareClicked(String shareLink) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Foodmario");
        share.putExtra(Intent.EXTRA_TEXT, shareLink);

        startActivity(Intent.createChooser(share, "Foodmario"));
    }

    private void onCommentClicked(Integer id) {
        Opener.openCommentActivity(this, food.id, 0);
    }

    private void onLikeClicked(Integer id) {
        int oldLikeCount = food.likeCount;
        if (food.isLiked) {
            food.isLiked = false;
            ivLikes.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_heart));
            tvLikeCount.setText(--food.likeCount + "");
        } else {
            food.isLiked = true;
            ivLikes.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_heart_red));
            tvLikeCount.setText(++food.likeCount + "");
        }
        foodDetailPresenter.setLiked(id, food.isLiked, oldLikeCount);
    }

    @OnClick(R.id.iv_back)
    void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void init() {

    }

    @Override
    public void onShowProgressDialog(String msg) {

    }

    @Override
    public void onHideProgressDialog() {

    }

    @Override
    public void onSuccess(String msg) {

    }

    @Override
    public void onError(String msg) {
        alerts.showToastMsg(msg);
    }

    @Override
    public void onValidationError(String msg) {

    }

    @OnClick(R.id.iv_cart)
    void onAddToCart() {
        String image = "";
        if (food.dishImages != null && food.dishImages.size() > 0)
            image = food.dishImages.get(0).imagePath;

        MyBottomSheetDialogFragment.newInstance(food.id, vendor.name, image, food.foodName, food.price, food.finalPrice, food.maxUnitPerDay).show(getSupportFragmentManager(), "");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onFoodReactSuccess(Boolean isLike, int likeCount) {
        tvLikeCount.setText(likeCount + "");
        if (isLike) {
            ivLikes.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_heart_red));
        } else {
            ivLikes.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_heart));
        }
    }

    @Override
    public void onFoodReactError(String message, Boolean isLiked, int oldLikeCount) {
        tvLikeCount.setText(oldLikeCount + "");
        alerts.showErrorAlert(message);
        if (isLiked) {
            ivLikes.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_heart_red));
        } else {
            ivLikes.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_heart));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
        if (resultCode == CommonDef.GO_BACK) {
            setResult(CommonDef.GO_BACK);
            finish();
        }

        if (requestCode == CommonDef.RESPONSE_COMMENT) {
            int commentCount = data.getIntExtra("count", 0);
            food.commentCount = commentCount;
            tvCommentCount.setText(commentCount + "");
//            }
        }
    }
}
